<?php

return [
    // The current version of this application, will be read from the version.inc file
    'version' => file_get_contents(__DIR__ . '/version.inc'),

    // The duration in seconds that the user be kept logged in
    'rememberMeDuration' => 3600 * 24 * 30, // 30 days

    'pagination.limit' => 20,

    'account.userValidateTime' => "-9 months",
    'account.userInvalidTime' => "-12 months",

    // See web.php for alias '@upload'
    'file.uploadRoot' => '@upload/',
    //'file.uploadRoot' => '@uploadMitt/',
    'file.uploadMaxFiles' => 10,
];
