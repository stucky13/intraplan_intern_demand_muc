<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=10.0.30.201;dbname=itpsolution_demand_muc_190731',
    'username' => 'web_user',
    'password' => 'web_password',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
