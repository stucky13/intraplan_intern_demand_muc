<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 25.05.2018
 * Time: 12:48
 */

use app\modules\core\models\User;
use yii\helpers\Html;

/**
 * @var User $user
 * @var $password
 */
?>
<main>
    <table cellspacing="0" cellpadding="0" style="font-family: Calibri; font-size: 12pt;">
        <tr>
            <td>Sehr geehrte(r) Herr / Frau <?= $user->getLastName() ?>,<br><br><br></td>
        </tr>
        <tr>
            <td>Ihr Passwort für das Portal GmbH wurde geändert. Mit dem Klick auf
                den nachstehenden Link werden Sie auf eine Seite geführt, auf der Sie Ihr Passwort eingeben können.<br><br>
            </td>
        </tr>
        <tr>
            <td>
                <a href= <?= Yii::$app->urlManager->createAbsoluteUrl(['site/login']); ?>>Link zum itpIdea</a><br><br>
            </td>
        </tr>
        <tr>
            <td><?= Yii::t('app', 'Username') . ': ' . $user->getUsername(); ?><br></td>
        </tr>
        <tr>
            <td><?= Yii::t('app', 'Password') . ': ' . $password ?><br><br></td>
        </tr>
        <tr>
            <td>
                Bei Rückfragen wenden Sie sich bitte an Vorname Nachname, E-Mail: vorname.nachname@bwtarif.de, Telefon:
               Tel. Nr.<br><br><br>
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                Unternehmen
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                Adresse
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                PLZ Ort
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                Homepage<br><br>
            </td>
        </tr>
        <tr>
            <td>
                <?php //echo Html::img(Yii::$app->urlManager->createAbsoluteUrl('images/logo_email.png')); ?>
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Geschäftsführer: Vorname Nachnamer<br>
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Aufsichtsratsvorsitzender: Titel Vorname Nachname<br>
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Amtsgericht Ort HRB Nr<br>
            </td>
        </tr>
    </table>
</main>