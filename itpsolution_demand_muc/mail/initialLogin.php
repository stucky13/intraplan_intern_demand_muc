<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 23.05.2018
 * Time: 09:33
 */

use yii\helpers\Html;

/* @var $user \app\modules\core\models\User
 * @var $password string
 */
?>
<main>
    <table cellspacing="0" cellpadding="0" style="font-family: Calibri; font-size: 12pt;">
        <tr>
            <td>Sehr geehrte(r) Herr / Frau <?= $user->getLastName() ?>,<br><br></td>
        </tr>
        <tr>
            <td>Sie wurden als Benutzer für das bwTarifportal der Baden-Württemberg-Tarif GmbH eingetragen.</td>
        </tr>
        <tr>
            <td>Damit stehen Ihnen verschiedene Möglichkeiten zur Abfrage, zum Hochladen oder zur Bearbeitung von
                Tarifdaten, Vertriebsdaten oder Erlösmeldungen entsprechend den Ihnen zugewiesenen Berechtigungen zur
                Verfügung. Zu diesen Zwecken sind bei der Baden-Württemberg-Tarif GmbH Ihre beruflichen Kontaktdaten
                hinterlegt. Diese werden ausschließlich zur Kommunikation zwischen der BW-Tarif GmbH und Ihnen genutzt.
                Diese Daten können nur durch Mitarbeiter der Baden-Württemberg-Tarif GmbH sowie durch den
                verantwortlichen Ansprechpartner Ihres Unternehmens eingesehen werden. Mit dem Klick auf den
                nachstehenden Link werden Sie auf eine Seite geführt, auf der Sie Ihr Passwort eingeben können und der
                Speicherung und Verwendung der Daten entsprechend der Datenschutzerklärung zustimmen können.
            </td>
        </tr>
        <tr>
            <td>
                <a href= <?= Yii::$app->urlManager->createAbsoluteUrl(['site/login']); ?>>Link zum bwTarifportal</a><br><br>
            </td>
        </tr>
        <tr>
            <td><?= Yii::t('app', 'Username') . ': ' . $user->getUsername(); ?><br></td>
        </tr>
        <tr>
            <td><?= Yii::t('app', 'Password') . ': ' . $password ?><br><br></td>
        </tr>
        <tr>
            <td>
                Bei Rückfragen wenden Sie sich bitte an Lydia Tolksdorf, E-Mail: lydia.tolksdorf@bwtarif.de, Telefon:
                0711 7811 7215<br><br>
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                Baden-Württemberg-Tarif GmbH
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                Stockholmer Platz 1
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                D-70173 Stuttgart
            </td>
        </tr>
        <tr style="font-size: 11pt;">
            <td>
                www.bwtarif.gmbh/bwTarifportal
            </td>
        </tr>
        <tr>
            <td>
                <?= Html::img(Yii::$app->urlManager->createAbsoluteUrl('images/logo_email.png')); ?>
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Geschäftsführer: Thomas Balser
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Aufsichtsratsvorsitzender: Dr. Andreas Moschinski
            </td>
        </tr>
        <tr style="font-family: Arial; font-size: 8pt">
            <td>
                Amtsgericht Stuttgart HRB 763512
            </td>
        </tr>
    </table>
</main>