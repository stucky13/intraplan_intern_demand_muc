<?php

use yii\db\Migration;

/**
 * Class m190131_120748_db_scheme_delete_type_column_from_category
 */
class m190131_120748_db_scheme_delete_type_column_from_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('category', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('category', 'type', 'VARCHAR(255)');
    }
}
