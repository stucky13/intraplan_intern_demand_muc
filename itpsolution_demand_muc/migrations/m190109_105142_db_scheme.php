<?php

use yii\db\Migration;

/**
 * Class m190109_105142_db_scheme
 */
class m190109_105142_db_scheme extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS user (
              `user_id` INT(11) NOT NULL AUTO_INCREMENT,
              `first_name` VARCHAR(255) NOT NULL,
              `last_name` VARCHAR(255) NOT NULL,
              `username` VARCHAR(255) NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              `phone1` VARCHAR(255) NULL DEFAULT NULL,
              `phone2` VARCHAR(255) NULL DEFAULT NULL,
              `phone3` VARCHAR(255) NULL DEFAULT NULL,
              `company` VARCHAR(255) NULL DEFAULT NULL,
              `address1` VARCHAR(255) NULL DEFAULT NULL,
              `address2` VARCHAR(255) NULL DEFAULT NULL,
              `address3` VARCHAR(255) NULL DEFAULT NULL,
              `address4` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `password` VARCHAR(255) NOT NULL,
              `password_reset_token` VARCHAR(255) NULL DEFAULT NULL,
              `access_token` VARCHAR(255) NOT NULL,
              `auth_key` VARCHAR(255) NOT NULL,
              `is_active` TINYINT(4) NOT NULL DEFAULT '0',
              `is_new` TINYINT(4) NOT NULL DEFAULT '1',
              `is_deleted` TINYINT(1) NOT NULL DEFAULT '0',
              `is_password_change_required` TINYINT(1) NOT NULL,
              `privacy_policy` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`user_id`),
              UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC))
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS stationsangabe (
              `ibnr` INT(11) NOT NULL,
              `name` VARCHAR(255) NOT NULL,
              `laenge` DECIMAL(30,15) NOT NULL,
              `breite` DECIMAL(30,15) NOT NULL,
              PRIMARY KEY (`ibnr`))
            ENGINE = InnoDB
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS streckenband (
              `strecke_id` INT(11) NOT NULL,
              `hst_sort` INT(11) NOT NULL,
              `strecke_name` VARCHAR(255) NOT NULL,
              `ibnr` INT(11) NOT NULL,
              `name` VARCHAR(255) NULL,
              PRIMARY KEY (`strecke_id`, `hst_sort`),
              INDEX `fk_routes_stations_ibnr_idx` (`ibnr` ASC),
              CONSTRAINT `fk_streckenband_stationsangabe_ibnr`
                FOREIGN KEY (`ibnr`)
                REFERENCES stationsangabe (`ibnr`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS category (
              `category_id` INT(11) NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255) NOT NULL,
              `type` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`category_id`))
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS fahrschein (
              `id` INT(11) NOT NULL,
              `kategorie1` INT(11) NOT NULL,
              `kategorie2` INT(11) NOT NULL,
              `kategorie3` INT(11) NOT NULL,
              `kategorie4` INT(11) NOT NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_fahrschein_kategorie1_category_category_id_idx` (`kategorie1` ASC),
              INDEX `fk_fahrschein_kategorie2_category_category_id_idx` (`kategorie2` ASC),
              INDEX `fk_fahrschein_kategorie3_category_category_id_idx` (`kategorie3` ASC),
              INDEX `fk_fahrschein_kategorie4_category_category_id_idx` (`kategorie4` ASC),
              CONSTRAINT `fk_fahrschein_kategorie1_category_category_id`
                FOREIGN KEY (`kategorie1`)
                REFERENCES category (`category_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_kategorie2_category_category_id`
                FOREIGN KEY (`kategorie2`)
                REFERENCES category (`category_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_kategorie3_category_category_id`
                FOREIGN KEY (`kategorie3`)
                REFERENCES category (`category_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_kategorie4_category_category_id`
                FOREIGN KEY (`kategorie4`)
                REFERENCES category (`category_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS permission (
              `permission_id` INT(11) NOT NULL AUTO_INCREMENT,
              `permission` VARCHAR(255) NOT NULL,
              `is_assignable` TINYINT(1) NOT NULL DEFAULT '1',
              `permission_group_id` INT(11) NULL DEFAULT NULL,
              PRIMARY KEY (`permission_id`))
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS language (
              `language_id` INT(11) NOT NULL AUTO_INCREMENT,
              `title` VARCHAR(255) NOT NULL,
              `locale` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`language_id`))
            ENGINE = InnoDB;
        ");

        /* Default Language */
        $this->insert('language', ["title" => "Deutsch", "locale" => "de-DE"]);

        $this->execute("
            CREATE TABLE IF NOT EXISTS permission_i18n (
              `permission_id` INT(11) NOT NULL,
              `language_id` INT(11) NOT NULL,
              `title` VARCHAR(255) NOT NULL,
              `description` VARCHAR(255) NOT NULL,
              INDEX `fk_permission_i18n_permission_idx` (`permission_id` ASC),
              INDEX `fk_permission_i18n_language_idx` (`language_id` ASC),
              PRIMARY KEY (`permission_id`, `language_id`),
              CONSTRAINT `fk_permission_i18n_language`
                FOREIGN KEY (`language_id`)
                REFERENCES language (`language_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_permission_i18n_permission`
                FOREIGN KEY (`permission_id`)
                REFERENCES permission (`permission_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
        ");

        /* add permissions dont forget to yii rbac/rbac/init to initialize permissions */
        $this->insert('permission', ["permission_id" => 1, "permission" => "admin", "is_assignable" => 0, "permission_group_id" => 1]);
        $this->insert('permission', ["permission_id" => 2, 'permission' => "edit_org_type", "is_assignable" => 1, "permission_group_id" => 1]);
        $this->insert('permission', ["permission_id" => 3, "permission" => "edit_org", "is_assignable" => 1, "permission_group_id" => 1]);
        $this->insert('permission', ["permission_id" => 4, "permission" => "edit_user", "is_assignable" => 1, "permission_group_id" => 2]);
        $this->insert('permission', ["permission_id" => 5, "permission" => "view_org_type", "is_assignable" => 1, "permission_group_id" => 3]);
        $this->insert('permission', ["permission_id" => 6, "permission" => "view_org", "is_assignable" => 1, "permission_group_id" => 3]);
        $this->insert('permission', ["permission_id" => 7, "permission" => "view_user", "is_assignable" => 1, "permission_group_id" => 3]);
        $this->insert('permission', ["permission_id" => 8, "permission" => "view_all_users", "is_assignable" => 1, "permission_group_id" => 3]);

        $this->insert('permission_i18n', ["permission_id" => 1, "language_id" => 1, "title" => "Admin", "description" => "Administrator"]);
        $this->insert('permission_i18n', ["permission_id" => 2, "language_id" => 1, "title" => "Orga.Typen bearbeiten", "description" => "Darf Organisationstypen anlegen und bearbeiten"]);
        $this->insert('permission_i18n', ["permission_id" => 3, "language_id" => 1, "title" => "Organisationen bearbeiten", "description" => "Darf Organisationen anlegen und bearbeiten"]);
        $this->insert('permission_i18n', ["permission_id" => 4, "language_id" => 1, "title" => "Alle Benutzer bearbeiten", "description" => "Darf Benutzer anlegen und bearbeiten"]);
        $this->insert('permission_i18n', ["permission_id" => 5, "language_id" => 1, "title" => "Orga.Typen sehen", "description" => "Darf Organisationstypen sehen"]);
        $this->insert('permission_i18n', ["permission_id" => 6, "language_id" => 1, "title" => "Organisationen sehen", "description" => "Darf Organisationen sehen"]);
        $this->insert('permission_i18n', ["permission_id" => 7, "language_id" => 1, "title" => "Benutzer sehen", "description" => "Darf Benutzer seiner Organisation sehen"]);
        $this->insert('permission_i18n', ["permission_id" => 8, "language_id" => 1, "title" => "alle Benutzer sehen", "description" => "Darf alle Benutzer sehen"]);

        $this->execute("
            CREATE TABLE IF NOT EXISTS organisation_type (
              `organisation_type_id` INT(11) NOT NULL AUTO_INCREMENT,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `title` VARCHAR(255) NOT NULL,
              `short_title` VARCHAR(255) NOT NULL,
              `is_deleted` TINYINT(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`organisation_type_id`),
              UNIQUE INDEX `organisation_type_id_UNIQUE` (`organisation_type_id` ASC),
              INDEX `fk_organisation_type_user_id_idx` (`user_created` ASC),
              INDEX `fk_organisation_type_user_id2_idx` (`user_updated` ASC),
              CONSTRAINT `fk_organisation_type_user_id2`
                FOREIGN KEY (`user_updated`)
                REFERENCES user (`user_id`)
                ON DELETE SET NULL
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS organisation (
              `organisation_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_type_id` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `title` VARCHAR(255) NOT NULL,
              `short_title` VARCHAR(255) NOT NULL,
              `street` VARCHAR(255) NOT NULL,
              `house_number` VARCHAR(255) NULL DEFAULT NULL,
              `zip` INT(11) NOT NULL,
              `city` VARCHAR(255) NOT NULL,
              `representative_id` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`organisation_id`),
              UNIQUE INDEX `organisation_id_UNIQUE` (`organisation_id` ASC),
              INDEX `fk_organisation_user_id_idx` (`user_created` ASC),
              INDEX `fk_organisation_organisation_type_id_idx` (`organisation_type_id` ASC),
              INDEX `fk_organisation_user_id2_idx` (`user_updated` ASC),
              INDEX `fk_organisation_user_id3_idx` (`representative_id` ASC),
              CONSTRAINT `fk_organisation_organisation_type_id`
                FOREIGN KEY (`organisation_type_id`)
                REFERENCES organisation_type (`organisation_type_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_organisation_user_id2`
                FOREIGN KEY (`user_updated`)
                REFERENCES user (`user_id`)
                ON DELETE SET NULL
                ON UPDATE CASCADE,
              CONSTRAINT `fk_organisation_user_id3`
                FOREIGN KEY (`representative_id`)
                REFERENCES user (`user_id`)
                ON DELETE SET NULL
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS user_organisation (
              `user_id` INT(11) NOT NULL,
              `organisation_id` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_created` INT(11) NOT NULL,
              PRIMARY KEY (`user_id`, `organisation_id`),
              INDEX `fk_user_organisation_organisation_id_idx` (`organisation_id` ASC),
              CONSTRAINT `fk_user_organisation_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES organisation (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_user_organisation_user_id`
                FOREIGN KEY (`user_id`)
                REFERENCES user (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS organisation_auth_assignment (
              `organisation_id` INT(11) NOT NULL,
              `item_name` VARCHAR(64) NOT NULL,
              `dt_created` DATETIME NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              PRIMARY KEY (`organisation_id`, `item_name`),
              INDEX `fk_organisation_auth_assignment_id_organisation_id_idx` (`organisation_id` ASC),
              CONSTRAINT `fk_organisation_auth_assignment_id_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES organisation (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS organisation_type_auth_assignment (
              `organisation_type_id` INT(11) NOT NULL,
              `item_name` VARCHAR(64) NOT NULL,
              `dt_created` DATETIME NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              PRIMARY KEY (`organisation_type_id`, `item_name`),
              INDEX `fk_organisation_type_auth_assignment_id_organisation_type_i_idx` (`organisation_type_id` ASC),
              CONSTRAINT `fk_organisation_type_auth_assignment_id_organisation_type_id`
                FOREIGN KEY (`organisation_type_id`)
                REFERENCES organisation_type (`organisation_type_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE)
            ENGINE = InnoDB;
        ");


        $controller = new \app\modules\rbac\controllers\RbacController('test', new yii\base\Module('rbac'));
        $controller->actionInit();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $controller = new \app\modules\rbac\controllers\RbacController('test', new yii\base\Module('rbac'));
        $controller->actionDrop();

        $this->dropTable("organisation_type_auth_assignment");
        $this->dropTable("organisation_auth_assignment");
        $this->dropTable("user_organisation");
        $this->dropTable("organisation");
        $this->dropTable("organisation_type");
        $this->dropTable("permission_i18n");
        $this->dropTable("language");
        $this->dropTable("permission");
        $this->dropTable("fahrschein");
        $this->dropTable("category");
        $this->dropTable("streckenband");
        $this->dropTable("stationsangabe");
        $this->dropTable("user");
    }
}
