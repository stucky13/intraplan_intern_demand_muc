<?php

use app\modules\core\models\Permission;
use app\modules\core\models\PermissionI18n;
use yii\db\Migration;

/**
 * Class m190806_075723_db_scheme_add_table_berechnung
 */
class m190806_075723_db_scheme_add_table_berechnung extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `berechnung` (
  `berechnung_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Key',
  `user_id` int(11) NOT NULL COMMENT 'Login-User',
  `bezeichnung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'für den Anwender',
  `status` int(5) NOT NULL DEFAULT 0 COMMENT '0 = in_erfassung, 1 = fuer_berechnung_aktiviert, 2 = berechnung_begonnen, 3 = berechnung_erfolgreich, 4 = berechnung_mitfehlern, 67 = berechnung_erfolgreich_mailfehler, 68 = berechnung_mitfehlern_mailfehler',
  `anmerkung` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `startzeitpunkt` datetime NOT NULL DEFAULT current_timestamp(),
  `details` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Rückmeldung aus Berechnung',
  `muc_fahrzeit_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_fahrzeit_hbf_ist` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_hbf_neu` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_no_ist` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_no_neu` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_so_ist` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_so_neu` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_w_ist` decimal(15,2) DEFAULT NULL,
  `muc_fahrzeit_w_neu` decimal(15,2) DEFAULT NULL,
  `muc_takt_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_takt_hbf_ist` decimal(15,2) DEFAULT NULL,
  `muc_takt_hbf_neu` decimal(15,2) DEFAULT NULL,
  `muc_takt_no_ist` decimal(15,2) DEFAULT NULL,
  `muc_takt_no_neu` decimal(15,2) DEFAULT NULL,
  `muc_takt_so_ist` decimal(15,2) DEFAULT NULL,
  `muc_takt_so_neu` decimal(15,2) DEFAULT NULL,
  `muc_takt_w_ist` decimal(15,2) DEFAULT NULL,
  `muc_takt_w_neu` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_direktanbindung_hbf_ist` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_hbf_neu` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_no_ist` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_no_neu` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_so_ist` decimal(15,2) DEFAULT 1.00,
  `muc_direktanbindung_so_neu` decimal(15,2) DEFAULT NULL,
  `muc_direktanbindung_w_ist` decimal(15,2) DEFAULT 1.00,
  `muc_direktanbindung_w_neu` decimal(15,2) DEFAULT NULL,
  `muc_hbf_produkt_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_hbf_produkt_janein` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_hbf_produkt_preisaufschlag` decimal(15,2) DEFAULT NULL,
  `muc_str_fahrzeit_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_str_fahrzeit_a_ist` decimal(15,2) DEFAULT NULL,
  `muc_str_fahrzeit_a_neu` decimal(15,2) DEFAULT NULL,
  `muc_fra_fahrzeit_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_fra_fahrzeit_achse_s_ist` decimal(15,2) DEFAULT NULL,
  `muc_fra_fahrzeit_achse_s_neu` decimal(15,2) DEFAULT NULL,
  `muc_fra_fahrzeit_achse_n_ist` decimal(15,2) DEFAULT NULL,
  `muc_fra_fahrzeit_achse_n_neu` decimal(15,2) DEFAULT NULL,
  `muc_vie_fahrzeit_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_vie_fahrzeit_sal_ist` decimal(15,2) DEFAULT NULL,
  `muc_vie_fahrzeit_sal_neu` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_funktion` bit(1) NOT NULL DEFAULT b'0' COMMENT 'ja nein',
  `muc_miv_fahrzeit_a9_ist` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_a9_neu` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ad_ist` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ad_neu` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ak_s_ist` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ak_s_neu` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ak_w_ist` decimal(15,2) DEFAULT NULL,
  `muc_miv_fahrzeit_ak_w_neu` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`berechnung_id`),
  KEY `fk_user_user_id` (`user_id`),
  CONSTRAINT `fk_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Jeder Berechnungsvorgang wird in dieser Tabelle protokolliert.\r\n';
");

        $this->execute("CREATE TABLE IF NOT EXISTS `grenzwerte` (
  `nr` int(11) NOT NULL,
  `bezeichnung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max` decimal(5,1) DEFAULT NULL,
  `min` decimal(5,1) DEFAULT NULL,
  `anmerkung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;");


        $this->insert("grenzwerte", ["nr" => 1, "bezeichnung" => "muc_fahrzeit_hbf_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 2, "bezeichnung" => "muc_fahrzeit_no_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 3, "bezeichnung" => "muc_fahrzeit_so_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 4, "bezeichnung" => "muc_fahrzeit_w_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 5, "bezeichnung" => "muc_takt_hbf_vr", "max" => 3.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 6, "bezeichnung" => "muc_takt_no_vr", "max" => 3.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 7, "bezeichnung" => "muc_takt_so_vr", "max" => 3.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 8, "bezeichnung" => "muc_takt_w_vr", "max" => 3.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 9, "bezeichnung" => "muc_direktverbindung_hbf_vr", "max" => 4.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 10, "bezeichnung" => "muc_direktverbindung_no_vr", "max" => 4.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 11, "bezeichnung" => "muc_direktverbindung_so_vr", "max" => 50.0, "min" => 1.0, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 12, "bezeichnung" => "muc_direktverbindung_w_vr", "max" => 50.0, "min" => 1.0, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 13, "bezeichnung" => "str_fahrzeit_a_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 14, "bezeichnung" => "fra_fahrzeit_achse_s_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 15, "bezeichnung" => "fra_fahrzeit_achse_n_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 16, "bezeichnung" => "vie_fahrzeit_sal_vr", "max" => 1.5, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 17, "bezeichnung" => "miv_fahrzeit_a9_vr", "max" => 2.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 18, "bezeichnung" => "miv_fahrzeit_ad_vr", "max" => 2.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 19, "bezeichnung" => "miv_fahrzeit_ak_s_vr", "max" => 2.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 20, "bezeichnung" => "miv_fahrzeit_ak_w_vr", "max" => 2.0, "min" => 0.5, "anmerkung" => null]);
        $this->insert("grenzwerte", ["nr" => 21, "bezeichnung" => "hbf_produkt_preisaufschlag", "max" => 30.0, "min" => 0.0, "anmerkung" => null]);



        $this->insert("permission", ["permission_id" => 9, "permission" => "view_change_analysis", "is_assignable" => 1, "permission_group_id" => 3]);
        $this->insert("permission", ["permission_id" => 10, "permission" => "edit_change_analysis", "is_assignable" => 1, "permission_group_id" => 1]);

        $this->insert("permission_i18n", ["permission_id" => 9, "language_id" => 1, "title" => "Veränderungsanalysen sehen", "description" => "Darf Veränderungsanalysen sehen"]);
        $this->insert("permission_i18n", ["permission_id" => 10, "language_id" => 1, "title" => "Veränderungsanalysen beauftragen", "description" => "Darf Veränderungsanalysen in Auftrag geben"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("berechnung");
        $this->dropTable("grenzwerte");

        Permission::findOne(9)->delete();
        Permission::findOne(10)->delete();
    }


}
