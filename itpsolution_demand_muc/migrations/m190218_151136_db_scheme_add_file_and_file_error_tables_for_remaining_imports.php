<?php

use yii\db\Migration;

/**
 * Class m190218_151136_db_scheme_add_file_and_file_error_tables_for_remaining_imports
 */
class m190218_151136_db_scheme_add_file_and_file_error_tables_for_remaining_imports extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `fragebogen_file` (
              `fragebogen_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`fragebogen_file_id`),
              INDEX `fk_fragebogen_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_fragebogen_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_fragebogen_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_fragebogen_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_fragebogen_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fragebogen_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `fragebogen_file_error` (
              `fragebogen_file_id` INT(11) NOT NULL,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_fragebogen_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_fragebogen_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              INDEX `fk_fragebogen_file_error_streckenband_file_id_idx` (`fragebogen_file_id` ASC),
              CONSTRAINT `fk_fragebogen_file_error_fragebogen_file_id`
                FOREIGN KEY (`fragebogen_file_id`)
                REFERENCES `fragebogen_file` (`fragebogen_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fragebogen_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fragebogen_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `antworten_file` (
              `antworten_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`antworten_file_id`),
              INDEX `fk_antworten_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_antworten_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_antworten_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_antworten_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_antworten_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_antworten_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `antworten_file_error` (
              `antworten_file_id` INT(11) NOT NULL,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_antworten_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_antworten_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              INDEX `fk_antworten_file_error_antworten_file_id_idx` (`antworten_file_id` ASC),
              CONSTRAINT `fk_antworten_file_error_antworten_file_id`
                FOREIGN KEY (`antworten_file_id`)
                REFERENCES `antworten_file` (`antworten_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_antworten_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_antworten_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        /** add foreign key columns */
        $this->addColumn('antworten', 'antworten_file_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_antworten_antworten_file_id', 'antworten', 'antworten_file_id', 'antworten_file', 'antworten_file_id');
        $this->addColumn('fragebogen', 'fragebogen_file_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_fragebogen_fragebogen_file_id', 'fragebogen', 'fragebogen_file_id', 'fragebogen_file', 'fragebogen_file_id');

        /** fragebogen_id & antworten_id with auto increment */
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->alterColumn('fragebogen', 'fragebogen_id', 'INT(11) NOT NULL AUTO_INCREMENT');
        $this->alterColumn('antworten', 'antwort_id', 'INT(11) NOT NULL AUTO_INCREMENT');
        $this->alterColumn('fragen', 'frage_id', 'INT(11) NOT NULL AUTO_INCREMENT');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->alterColumn('fragebogen', 'fragebogen_id', 'INT(11) NOT NULL');
        $this->alterColumn('antworten', 'antwort_id', 'INT(11) NOT NULL');
        $this->alterColumn('fragen', 'frage_id', 'INT(11) NOT NULL');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

        $this->dropForeignKey('fk_fragebogen_fragebogen_file_id', 'fragebogen');
        $this->dropColumn('fragebogen', 'fragebogen_file_id');
        $this->dropForeignKey('fk_antworten_antworten_file_id', 'antworten');
        $this->dropColumn('antworten', 'antworten_file_id');

        $this->dropTable('antworten_file_error');
        $this->dropTable('antworten_file');
        $this->dropTable('fragebogen_file_error');
        $this->dropTable('fragebogen_file');
    }
}
