<?php

use yii\db\Migration;

/**
 * Class m190220_124659_db_scheme_add_auto_increment
 */
class m190220_124659_db_scheme_add_auto_increment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->alterColumn('fragen', 'frage_id', 'int(11) NOT NULL AUTO_INCREMENT');
        $this->alterColumn('fragebogen', 'fragebogen_id', 'int(11) NOT NULL AUTO_INCREMENT');
        $this->alterColumn('antworten', 'antwort_id', 'int(11) NOT NULL AUTO_INCREMENT');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->alterColumn('fragen', 'frage_id', 'int(11) NOT NULL');
        $this->alterColumn('fragebogen', 'fragebogen_id', 'int(11) NOT NULL');
        $this->alterColumn('antworten', 'antwort_id', 'int(11) NOT NULL');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }
}
