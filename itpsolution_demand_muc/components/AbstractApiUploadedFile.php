<?php

namespace app\components;


/**
 * Class AbstractApiUploadedFile
 * @package app\models
 *
 * This class implements methods and variables similar to UploadedFile for easy
 * usage of existing upload code
 */
class AbstractApiUploadedFile
{

    public $baseName;
    public $extension;
    public $tempName;
    private $fileData;

    /**
     * Creates "UploadFile" instance
     * @param string $decodedData File data
     * @param string $filename Filename
     * @return AbstractApiUploadedFile instance
     */
    public static function getInstance($decodedData, $filename)
    {
        $uploadedFile = new AbstractApiUploadedFile();

        $uploadedFile->fileData = $decodedData;

        $dot = strrpos($filename, '.');

        $uploadedFile->baseName = substr($filename, 0, strlen($filename) - strrpos($filename, '.') + 1);
        if ($dot) {
            //Extension exists, set ext var
            $uploadedFile->extension = substr($filename, strrpos($filename, '.') + 1);
        }

        //create temp file for validation
        $uploadedFile->tempName = tempnam(sys_get_temp_dir(), $uploadedFile->baseName);
        file_put_contents($uploadedFile->tempName, $decodedData);

        return $uploadedFile;
    }

    /**
     * Save file on disk
     * @param string $file Path
     * @return bool success
     */
    public function saveAs($file)
    {
        if (!file_put_contents($file, $this->fileData)) {
            return false;
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getBaseName()
    {
        return $this->baseName;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }


}