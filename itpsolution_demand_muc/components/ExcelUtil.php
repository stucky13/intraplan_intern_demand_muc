<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 11.02.2019
 * Time: 14:59
 */

namespace app\components;

use Yii;

class ExcelUtil {

    public $filter =  [];
    public $tablehead = [];
    public $tablecontent = [];
    public $title = [];

    /**
     * @return array of created sheets
     */
    public function createSheets()
    {
        $sheets = [];
        for($i = 0; $i < sizeof($this->tablehead); $i++ ){
            $data = [];
            for($j = 0; $j < sizeof($this->tablecontent[$i]); $j++){
                array_push($data, $this->tablecontent[$i][$j]);
            }
            $sheets[$this->tablehead[$i]] = [
                'data' => $data,
                'titles' => $this->title[$i],

            ];
        }

        return $sheets;
    }

    public function exportExcel($filename)
    {
        $file = Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => $this->createSheets(),
        ]);
        $file->send($filename.'.xlsx', false, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    }
}
