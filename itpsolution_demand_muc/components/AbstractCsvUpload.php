<?php

namespace app\components;


use app\modules\upload\models\AntwortenFile;
use app\modules\upload\models\FahrscheinFile;
use app\modules\upload\models\FragebogenFile;
use app\modules\upload\models\KnotenFile;
use app\modules\upload\models\LinienFile;
use app\modules\upload\models\ZaehldatenFile;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;


/**
 * Class AbstractCsvUpload
 *
 * Extends Model with some csv specific methods and a upload functionality
 * @package app\models
 */
abstract class AbstractCsvUpload extends Model
{
    /**
     * @var UploadedFile
     */
    public $csvFile;
    public $subFolder = "";
    public $uploadedPath = "";


    public function attributeLabels()
    {
        return [
            "csvFile" => Yii::t('app', 'csv file'),
        ];
    }

    public function rules()
    {
        return [
            ['csvFile', 'file'],
            ['csvFile', 'required'],
            ['csvFile', 'validateBaseCsv'],
            ['subFolder', 'string'],
        ];
    }


    /**
     * Validate dimensions of csv file
     * Every row has to have the same number of columns
     * @param $attribute
     */
    public function validateBaseCsv($attribute)
    {
        $csvFile = new CsvValidator();
        $csvFile->loadCsvFromFile($this->csvFile->tempName);
        if (!$csvFile->checkUniform()) {
            $this->addError($attribute, Yii::t('app', 'CSV not uniform'));
        }
    }

    /**
     * Builds path out of root + custom path and copies file from temp path  to provided path
     * @param bool $unique If the saved filename should be unique
     * @return string path | null if unsuccessful
     */
    public function upload($unique = false)
    {
        $aliasDir = \Yii::$app->params['file.uploadRoot'] . $this->subFolder;
        $absoluteDir = \Yii::getAlias($aliasDir);

        if (!is_dir($absoluteDir)) {
            mkdir($absoluteDir, 0777, true);
        }

        $filename = $unique ? $this->generateUniqueName() : $this->getName();
        $path = $absoluteDir . $filename;

        if ($this->csvFile->saveAs($path)) {
            $this->uploadedPath = $aliasDir . $filename;
            return $path;
        }
    }

    /**
     * Returns filename with extension
     * @return string Name
     */
    public function getName()
    {
        return $this->csvFile->baseName . "." . $this->csvFile->extension;
    }

    /**
     *  Returns timestamped name  ->  name + timestamp + extension
     * @return string unique name
     */
    public function generateUniqueName()
    {
        $timestamp = new \DateTime();
        return $this->csvFile->baseName . "_" . $timestamp->format("Y-m-d_H-i-s-v") . "." . $this->csvFile->extension;
    }

    /**
     * @param string $filePath
     * @return array $csvData
     */
    public function getCsv($filePath)
    {
        $row = 1;
        $csvData = [];
        if (($handle = fopen(Yii::getAlias($filePath), "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
                if ($row == 1) {
                    $row++;
                    continue;
                }
                $csvData[] = $data;

                unset($data);
                gc_collect_cycles();
                $row++;
            }
            fclose($handle);
            return $csvData;
        }
    }

    /**
     * @param string $filePath
     * @return array $csvData
     */
    public function getCsvHeader($filePath)
    {
        if (($handle = fopen(Yii::getAlias($filePath), "r")) !== FALSE) {
            $csvHeader = fgetcsv($handle, 0, ";");
            fclose($handle);
            return $csvHeader;
        }
    }
}