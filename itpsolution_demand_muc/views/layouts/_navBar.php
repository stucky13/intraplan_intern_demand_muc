<?php

use app\modules\admin\models\OrganisationSessionForm;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

$isGuest = Yii::$app->getUser()->isGuest;

?>

<?php NavBar::begin([
    'brandLabel' => Html::img('@web/images/logo.png', ['class' => 'navbar-brand-logo']),
    'brandUrl' => Yii::$app->getHomeUrl(),
    'options' => [
        'id' => 'mainmenu',
        'class' => 'navbar-default navbar-fixed-top',
    ],
]); ?>

<?= Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [

    ]
]); ?>

<?= Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'encodeLabels' => false,
    'items' => [
        $isGuest ?
            ['label' => Yii::t('app', 'login'), 'url' => ['/site/login']] :
            [
                'label' => Yii::t('app', 'Logout ({0})', Yii::$app->getUser()->getIdentity()->username),
                'url' => ['/site/logout'],
                'linkOptions' => ['class'=>'logoutColor','data-method' => 'post']
            ],
    ],
]); ?>

<div id="organisation_dropdown">
    <?= $this->render('@app/modules/admin/views/user/_organisation_dropdown', [
        'model' => new OrganisationSessionForm()
    ]); ?>
</div>
<?php NavBar::end(); ?>
