<?php

/* @var $this \yii\web\View
 * @var $content string
 */

use app\assets\AppAsset;
use yii\helpers\Html;

$bundle = AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <META http-equiv="X-UA-Compatible" content="IE=11">
    <script type="text/javascript">
        var baseUrl = "<?= Yii::$app->getHomeUrl();?>";
    </script>
</head>
<body>
<?php $this->beginBody();
$isGuest = Yii::$app->getUser()->isGuest; ?>

<div id="page" data-login-state="<?= $isGuest ? "logged-out" : "logged-in"; ?>">

    <?php if (!$isGuest) {
        echo $this->render('@app/views/layouts/_navBar');
    } else {
        echo $this->render('@app/views/layouts/_guestheader');
    } ?>

    <h1 id="bw-title-bar" class="title-bar">
        <?php
        if ($this->title == Yii::t('app', 'ITPidea')) {
            echo '<div style="text-transform: none">' . Html::encode($this->title) . "</div>";
        } else {
            echo Html::encode($this->title);
        }
        ?>
    </h1>
    <div id="content" class="container">
        <?php $this->render('@app/views/layouts/_alerts'); ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <ul class="pull-left text-muted footer-links list-inline">
            <li>&copy; Organisation</li>
            <li>·</li>
            <li><?= Html::a('Link', '#', ['target' => '_blank']) ?></li>
            <li><?= Html::a('Impressum', ['/site/imprint']) ?></li>
        </ul>
        <p class="pull-right text-muted">Version <?= Yii::$app->params['version'] ?></p>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
