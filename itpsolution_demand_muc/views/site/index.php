<?php
use app\components\Constants;
use app\modules\rbac\components\CustomDbManager;
use \app\modules\core\widgets\DashboardPanel;
use app\modules\core\widgets\NavigationWidget;

/**
 * @var \yii\db\ActiveQuery $newUsers
 * @var \yii\db\ActiveQuery $oldUsers
 * @var \yii\db\ActiveQuery $newReports
 * @var \yii\db\ActiveQuery $eaPeriods
 */


$this->title = Yii::t('app', 'bw portal');

?>

<div class="row">
    <div class="col-lg-8">
        <?php DashboardPanel::begin() ?>
        <?= NavigationWidget::widget(); ?>
        <?php DashboardPanel::end(); ?>
    </div>
</div>

