<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'ITPidea');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="center-block login-container">
    <div class="panel panel-default panel-color">
        <div class="panel-body panel-color">
            <p> <?= Yii::t('app', 'login request') ?></p>

            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['class' => 'form-control form-control-color'],['placeholder' => Yii::t('app', 'username')]) ?>

            <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control form-control-color'],['placeholder' => Yii::t('app', 'password')]) ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-lg-12\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
            ]) ?>

            <?= Html::submitButton(Yii::t('app', 'login'), ['class' => 'btn mediumButton btn-large btn-block']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>