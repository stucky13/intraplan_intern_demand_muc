<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 16:44
 */

namespace app\modules\data\widgets;

use app\modules\data\models\Category;
use yii\base\Model;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/**
 * Class CategorySelectWidget
 *
 * @package app\modules\core\widgets
 */
class CategorySelectWidget extends Widget
{
    /**
     * @var ActiveForm the active form widget
     */
    public $form;

    /**
     * @var Model the associated model
     */
    public $model;

    /**
     * @var string the attribute name
     */
    public $attribute;

    /**
     * @var array the update route
     */
    public $updateRoute;

    /**
     * @var boolean indicates whether this widget is read only
     */
    public $readOnly = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_category_select', [
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'updateUrl' => Url::to($this->updateRoute),
            'initialText' => $this->getInitialText(),
            'readOnly' => $this->readOnly
        ]);
    }

    /**
     * Returns the initial text for the select
     */
    private function getInitialText()
    {
        $categoryId = $this->model->{$this->attribute};

        // Return an empty string if nothing is selected
        if (empty($categoryId) || !($category = Category::findOne($categoryId))) {
            return "";
        }

        return $category->getTitle();
    }
}