<?php

use app\components\FormModel;
use app\modules\data\assets\DataAsset;
use app\modules\data\models\VerkehrsmittelForm;
use app\modules\data\Module;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model VerkehrsmittelForm
 * @var int $page
 * */

$this->title = $model->getScenario() == FormModel::SCENARIO_DEFAULT ? Module::t('data', 'create verkehrsmittel') : Module::t('data', 'update verkehrsmittel');

DataAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'verkehrsmittel-form',
    'options' => ['class' => 'form-vertical']
]); ?>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default panel-color">
            <div class="panel-body panel-color">
                <?= $form->field($model, 'nr')->textInput() ?>

                <?= $form->field($model, 'name')->textInput() ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= Html::a(Yii::t('app', 'abort'), ['index', 'page' => $page], ['class' => 'btn mediumButton pull-left']) ?>

        <?= Html::submitButton(Yii::t('app', 'save'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
