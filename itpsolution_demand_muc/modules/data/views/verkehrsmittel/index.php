<?php

use app\modules\data\Module;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('data', 'verkehrsmittel');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'nr' => [
            'attribute' => 'nr',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'name' => [
            'attribute' => 'name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-eye-open', 'title' => Yii::t('app', 'view')]);
                },
                'update' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-pencil', 'title' => Yii::t('app', 'update')]);
                },
                'delete' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-trash', 'title' => Yii::t('app', 'delete'), 'onclick' => 'return confirm("' . Module::t('data', 'delete verkehrsmittel') . '");']);
                }
            ]
        ]
    ],
]); ?>

<div class="clearfix">
    <?= Html::a(Module::t('data', 'create verkehrsmittel'), ["create", 'page' => $page], [
        'class' => 'btn mediumButton pull-right'
    ]); ?>
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>