<?php

use app\modules\data\models\Verkehrsmittel;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model Verkehrsmittel
 * @var $page int
 */

$this->title = $model->getName();

?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
            'attributes' => [
                'nr' => [
                    'attribute' => 'nr',
                    'headerOptions' => ['class' => 'mediumButton gridview-header']
                ],
                'name' => [
                    'attribute' => 'name',
                    'headerOptions' => ['class' => 'mediumButton gridview-header']
                ]
            ]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= Html::a(Yii::t('app', 'back'), ['index', 'page' => $page], [
            'class' => 'btn mediumButton pull-left',
        ]); ?>
        <?= Html::a(Yii::t('app', 'update'), ["update", 'id' => $model['nr'], 'page' => $page], [
            'class' => 'btn mediumButton pull-right'
        ]); ?>
    </div>
</div>
