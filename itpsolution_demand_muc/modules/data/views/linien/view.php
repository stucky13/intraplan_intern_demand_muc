<?php

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\modules\data\assets\DataAsset;
use app\modules\data\models\LinienKnoten;
use app\modules\data\Module;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

DataAsset::register($this);
$this->title = Module::t('data', 'route');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'hst_sort' => [
            'attribute' => 'hst_sort',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'nr' => [
            'attribute' => 'nr',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (LinienKnoten $routeKnot) {
                return $routeKnot->getKnot()->one()->getName();
            }
        ]
    ],
]); ?>
<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>