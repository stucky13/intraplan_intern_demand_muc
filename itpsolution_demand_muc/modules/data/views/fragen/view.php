<?php

use app\modules\data\Module;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model app\modules\questionnaire\models\Fragen
 * @var $questionEnums app\modules\questionnaire\models\FragenEnum
 * @var $attributes
 * @var $page int
 */

$this->title = $model->getFragestellung();

?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
            'attributes' => $attributes
        ]) ?>
    </div>
</div>
<?php if ($model['antworttyp'] == 'user_defined') : ?>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <table style="background-color: #ddd" class="table table-striped">
                <thead>
                <tr>
                    <th style="text-align: center;"><?= Module::t('questionnaire', 'user defined title'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($questionEnums as $enum) { ?>
                    <tr>
                        <td><?= $enum['value'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= Html::a(Module::t('questionnaire', 'back'), ['index', 'page' => $page], [
            'class' => 'btn mediumButton pull-left',
        ]); ?>
        <?= Html::a(Module::t('questionnaire', 'update'), ["update", 'id' => $model['frage_id'], 'page' => $page], [
            'class' => 'btn mediumButton pull-right'
        ]); ?>
    </div>
</div>
