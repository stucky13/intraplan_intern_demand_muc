<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:11
 */

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\components\Constants;
use app\modules\data\assets\DataAsset;
use app\modules\data\Module;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

DataAsset::register($this);
$this->title = Module::t('data', 'raised data');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'erhebungsdaten_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'year' => [
            'attribute' => 'year',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'period' => [
            'attribute' => 'period',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'note' => [
            'attribute' => 'note',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_TICKETS)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-trash', 'title' => Module::t('data', 'delete'), 'onclick' => 'return confirm("' . Module::t('data', 'delete ticket') . '");']);
                    }
                    return "";
                }
            ]
        ]
    ],
]); ?>
<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>