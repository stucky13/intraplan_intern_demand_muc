<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:11
 */

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\modules\data\assets\DataAsset;
use app\modules\data\Module;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

DataAsset::register($this);
$this->title = Module::t('data', 'knot');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'nr' => [
            'attribute' => 'nr',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'name' => [
            'attribute' => 'name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'laenge' => [
            'attribute' => 'laenge',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'breite' => [
            'attribute' => 'breite',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ]
    ],
]); ?>
<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>

    <?= Html::a(Yii::t('app', 'knots import'), 'upload', ['class' => 'btn mediumButton pull-right']) ?>

</div>