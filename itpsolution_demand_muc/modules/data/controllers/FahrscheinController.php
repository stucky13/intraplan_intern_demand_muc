<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:08
 */

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\data\models\Fahrschein;
use app\modules\data\models\FahrscheinCsvUpload;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class FahrscheinController
 *
 * @package app\modules\data\controllers
 */
class FahrscheinController extends BaseController
{

    /**
     * Displays the overview.
     * @param int $page
     * @return string the content
     * @return string|Response
     * @throws \Exception
     * @throws Throwable
     */
    public function actionIndex($page = 1)
    {
        /*if (!Yii::$app->getUser()->can(Constants::VIEW_TICKETS)) {
            return $this->goHome();
        }*/

        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Fahrschein::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Deletes the specified ticket.
     *
     * @param int $id the ticket id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid ticket id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var Fahrschein $ticket */
        $ticket = Fahrschein::findOne($id);
        if ($ticket == null) {
            throw new BadRequestHttpException(Module::t('data', "Failed to find ticket with id ") . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $ticket->delete();
            $transaction->commit();
            Alert::addSuccess(Module::t('data', "ticket %s successfully deleted"), $ticket->getId());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t('data', "ticket %s couldn't be deleted"), $ticket->getId());
        }

        return $this->redirect(['index']);
    }

    public function actionUpload($page = 1)
    {
        // Permission check
//        if (!(Yii::$app->getUser()->can(Constants::IMPORT_ROUTES)
//            || Yii::$app->getUser()->can(Constants::EDIT_ROUTES))) {
//            return $this->goHome();
//        }

        $model = new FahrscheinCsvUpload();
        $organisationId = SessionUtil::getSessionOrganisationId();

        $model->organisation_id = $organisationId;

        $model->subFolder .= $organisationId . "/";

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');

            if ($model->validate()) {
                if ($model->upload(true)) {
                    if ($model->save()) {
                        Alert::addSuccess(Module::t('message', 'file uploaded'));
                        return $this->redirect(['index']);
                    } else {
                        Alert::addError(Module::t('message', 'could not save file'));
                    }
                } else {
                    Alert::addError(Module::t('message', 'could not save file'));
                }
            } else {
                Alert::addError(Module::t('message', 'could not upload file'));
            }
        }

        return $this->render('upload', [
            'model' => $model,
            'page' => $page
        ]);
    }
}