<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:08
 */

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\data\models\Knoten;
use app\modules\data\models\KnotenCsvUpload;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class KnotenController
 *
 * @package app\modules\data\controllers
 */
class KnotenController extends BaseController
{
    /**
     * Displays the overview.
     * @param int $page
     * @return string the content
     * @return string|Response
     * @throws \Exception
     * @throws Throwable
     */
    public function actionIndex($page = 1)
    {
        /*if (!Yii::$app->getUser()->can(Constants::VIEW_STATIONS)) {
            return $this->goHome();
        }*/

        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Knoten::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Deletes the specified knot
     *
     * @param int $id the nr of the knot
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid ibnr
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var Knoten $knot */
        $knot = Knoten::findOne($id);
        if ($knot == null) {
            throw new BadRequestHttpException(Module::t('data', "Failed to find knot with nr ") . $id);
        }
        $knotId = $knot->getNr();
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $knot->delete();
            $transaction->commit();
            Alert::addSuccess(Module::t('data', "knot %s successfully deleted"), $knotId);
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t('data', "knot %s couldn't be deleted"), $knotId);
        }

        return $this->redirect(['index']);
    }

    public function actionUpload($page = 1)
    {
        // Permission check
//        if (!(Yii::$app->getUser()->can(Constants::IMPORT_STATIONS)
//            || Yii::$app->getUser()->can(Constants::EDIT_STATIONS))) {
//            return $this->goHome();
//        }

        $model = new KnotenCsvUpload();
        $organisationId = SessionUtil::getSessionOrganisationId();

        $model->organisation_id = $organisationId;

        $model->subFolder .= $organisationId . "/";

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');

            if ($model->validate()) {
                if ($model->upload(true)) {
                    if ($model->save()) {
                        Alert::addSuccess(Module::t('message', 'file uploaded'));
                        return $this->redirect(['index']);
                    } else {
                        Alert::addError(Module::t('message', 'could not save file'));
                    }
                } else {
                    Alert::addError(Module::t('message', 'could not save file'));
                }
            } else {
                Alert::addError(Module::t('message', 'could not upload file'));
            }
        }

        return $this->render('upload', [
            'model' => $model,
            'page' => $page
        ]);
    }
}