<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 23.01.2019
 * Time: 10:56
 */

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\data\models\AntwortenCsvUpload;
use app\modules\data\models\Erhebungsdaten;
use app\modules\data\models\ErhebungsdatenForm;
use app\modules\data\models\Fahrschein;
use app\modules\data\models\FragebogenCsvUpload;
use app\modules\data\models\ZaehldatenCsvUpload;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ErhebungsdatenController extends BaseController
{

    /**
     * Displays the overview.
     * @param int $page
     * @return string the content
     * @return string|Response
     * @throws \Exception
     * @throws Throwable
     */
    public function actionIndex($page = 1)
    {
        /*if (!Yii::$app->getUser()->can(Constants::VIEW_TICKETS)) {
            return $this->goHome();
        }*/

        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Erhebungsdaten::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Deletes the specified ticket.
     *
     * @param int $id the ticket id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid ticket id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var Fahrschein $ticket */
        $ticket = Fahrschein::findOne($id);
        if ($ticket == null) {
            throw new BadRequestHttpException(Module::t('data', "Failed to find ticket with id ") . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $ticket->delete();
            $transaction->commit();
            Alert::addSuccess(Module::t('data', "ticket %s successfully deleted"), $ticket->getId());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t('data', "ticket %s couldn't be deleted"), $ticket->getId());
        }

        return $this->redirect(['index']);
    }

    public function actionUpload($page = 1)
    {
        // Permission check
//        if (!(Yii::$app->getUser()->can(Constants::IMPORT_ROUTES)
//            || Yii::$app->getUser()->can(Constants::EDIT_ROUTES))) {
//            return $this->goHome();
//        }

        $model = new ErhebungsdatenForm();
        $questionnaireModel = new FragebogenCsvUpload();
        $surveyModel = new AntwortenCsvUpload();
        $countDataModel = new ZaehldatenCsvUpload();

        $organisationId = SessionUtil::getSessionOrganisationId();

        $questionnaireModel->organisation_id = $organisationId;
        $questionnaireModel->subFolder .= $organisationId . "/";
        $surveyModel->organisation_id = $organisationId;
        $surveyModel->subFolder .= $organisationId . "/";
        $countDataModel->organisation_id = $organisationId;
        $countDataModel->subFolder .= $organisationId . "/";

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $questionnaireModel->csvFile = UploadedFile::getInstance($model, 'questionnaire_file');
            $surveyModel->csvFile = UploadedFile::getInstance($model, 'survey_file');
            $countDataModel->csvFile = UploadedFile::getInstance($model, 'count_data_file');
            
            if ($questionnaireModel->validate() && $surveyModel->validate() && $countDataModel->validate()) {
                $questionnaireModel->upload(true);
                $questionnaireModel->save();
                $model->questionnaire_id = $questionnaireModel->questionnaireFileId;

                $surveyModel->upload(true);
                $surveyModel->questionnaireId = $questionnaireModel->questionnaireId;
                $surveyModel->save();
                $model->survey_id = $surveyModel->surveyFileId;

                $countDataModel->upload(true);
                $countDataModel->save();
                $model->count_data_id = $countDataModel->countFileId;
            }

            if ($model->validate()) {
                if ($model->save()) {
                    Alert::addSuccess(Module::t('message', 'file uploaded'));
                    return $this->redirect(['index']);
                } else {
                    Alert::addError(Module::t('message', 'could not save file'));
                }
            } else {
                Alert::addError(Module::t('message', 'could not upload file'));
            }
        }

        return $this->render('upload', [
            'model' => $model,
            'page' => $page
        ]);
    }
}