<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:08
 */

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\data\models\Linien;
use app\modules\data\models\LinienCsvUpload;
use app\modules\data\models\LinienKnoten;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class LinienController
 *
 * @package app\modules\data\controllers
 */
class LinienController extends BaseController
{

    /**
     * Displays the overview.
     * @param int $page
     * @return string the content
     * @return string|Response
     * @throws \Exception
     * @throws Throwable
     */
    public function actionIndex($page = 1)
    {
        /*if (!Yii::$app->getUser()->can(Constants::VIEW_ROUTES)) {
            return $this->goHome();
        }*/

        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Linien::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Displays the knots in correct hst_sort for the specified route.
     *
     * @param int $id
     * @param int $page
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid linien_id
     * @throws Throwable
     */
    public function actionView($id, $page = 1)
    {
        $route = Linien::findOne($id);
        if ($route == null) {
            throw new BadRequestHttpException(Module::t('admin', "Invalid route id ") . $id);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $route->getRouteKnots()
        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'page' => $page
        ]);
    }

    /**
     * TODO: Refactor delete
     * Deletes the route.
     *
     * @param int $linien_id the route id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $strecke_id & $hst_sort is not a valid foreign key combination
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($linien_id)
    {
        /** @var Linien $route */
        $route = Linien::find()->where(['linien' => $linien_id])->one();
        if ($route == null) {
            throw new BadRequestHttpException(Module::t('data', "Failed to find route with Id ") . $linien_id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $route->delete();
            LinienKnoten::deleteAll(['linien_id' => $linien_id]);
            $transaction->commit();
            Alert::addSuccess(Module::t('data', "route %s successfully deleted"), $linien_id);
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t('data', "route %s couldn't be deleted"), $linien_id);
        }

        return $this->redirect(['index']);
    }

    public function actionUpload($page = 1)
    {
        // Permission check
//        if (!(Yii::$app->getUser()->can(Constants::IMPORT_ROUTES)
//            || Yii::$app->getUser()->can(Constants::EDIT_ROUTES))) {
//            return $this->goHome();
//        }

        $model = new LinienCsvUpload();
        $organisationId = SessionUtil::getSessionOrganisationId();

        $model->organisation_id = $organisationId;

        $model->subFolder .= $organisationId . "/";

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->csvFile = UploadedFile::getInstance($model, 'csvFile');

            if ($model->validate()) {
                if ($model->upload(true)) {
                    if ($model->save()) {
                        Alert::addSuccess(Module::t('message', 'file uploaded'));
                        return $this->redirect(['index']);
                    } else {
                        Alert::addError(Module::t('message', 'could not save file'));
                    }
                } else {
                    Alert::addError(Module::t('message', 'could not save file'));
                }
            } else {
                Alert::addError(Module::t('message', 'could not upload file'));
            }
        }

        return $this->render('upload', [
            'model' => $model,
            'page' => $page
        ]);
    }
}