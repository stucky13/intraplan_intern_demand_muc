<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:36
 */

namespace app\modules\data\models;

use app\components\AbstractActiveRecord;

/**
 * Class Fragebogen
 * @package app\modules\data\models
 * @property int $fragebogen_id
 * @property int $user_created
 * @property string $dt_created
 * @property int $user_updated
 * @property string $dt_updated
 */
class Fragebogen extends AbstractActiveRecord
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getFragebogenId()
    {
        return $this->fragebogen_id;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

}