<?php

namespace app\modules\data\models;


/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:29
 */

use app\components\FormModel;
use app\modules\data\Module;
use Yii;

/**
 * Class FragenForm
 *
 * @package app\modules\data\models
 */
class FragenForm extends FormModel
{

    public $frage_id;
    public $fragestellung;
    public $kurztext;
    public $antworttyp;
    public $wertebereich_min;
    public $wertebereich_max;
    public $nachkommastellen;
    public $questionEnums;
    public $reference;
    public $field;
    public $category_id;
    public $is_georef;
    public $is_filter;
    public $is_value;
    public $value_type;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fragestellung', 'kurztext', 'antworttyp'], 'required'],
            [['fragestellung', 'kurztext', 'antworttyp', 'nachkommastellen', 'reference', 'field'], 'string'],
            [['wertebereich_min', 'wertebereich_max', 'category_id', 'value_type'], 'integer'],
            [['is_georef', 'is_filter', 'is_value'], 'boolean'],
            [
                'field',
                'required',
                'when' => function () {
                    return $this->antworttyp == 'reference';
                },
                'enableClientValidation' => false
            ],
            [
                'value_type',
                'required',
                'when' => function () {
                    return $this->is_value;
                },
                'enableClientValidation' => false
            ],
            [
                'value_type',
                'exist',
                'targetClass' => ValueType::className(),
                'targetAttribute' => 'value_type_id',
                'when' => function () {
                    return $this->is_value;
                },
                'enableClientValidation' => false
            ],
            [
                'category_id',
                'exist',
                'targetClass' => Category::className(),
                'targetAttribute' => 'category_id',
                'when' => function () {
                    return $this->reference == 'fahrschein';
                }
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'frage_id' => Module::t('questionnaire', 'question id'),
            'fragestellung' => Module::t('questionnaire', 'question'),
            'kurztext' => Module::t('questionnaire', 'short text'),
            'antworttyp' => Module::t('questionnaire', 'type of answer'),
            'wertebereich_min' => Module::t('questionnaire', 'range min'),
            'wertebereich_max' => Module::t('questionnaire', 'range max'),
            'nachkommastellen' => Module::t('questionnaire', 'number of decimal places'),
            'reference' => Module::t('questionnaire', 'reference'),
            'category_id' => Module::t('questionnaire', 'category_id'),
            'field' => Module::t('questionnaire', 'field'),
            'is_filter' => Module::t('questionnaire', 'is filter'),
            'is_georef' => Module::t('questionnaire', 'is georef'),
            'is_value' => Module::t('questionnaire', 'is value'),
            'value_type' => Module::t('questionnaire', 'value type'),
        ];
    }

    /**
     * Creates a new question, or updates an existing one.
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $question = new Fragen();
            $question->user_created = Yii::$app->user->getId();
        } else {
            $question = Fragen::findOne($this->id);
            $question->user_updated = Yii::$app->user->getId();
        }

        $question->fragestellung = $this->fragestellung;
        $question->kurztext = $this->kurztext;
        $question->antworttyp = $this->antworttyp;
        $question->is_filter = $this->is_filter;
        $question->is_georef = $this->is_georef;
        $question->is_value = $this->is_value;
        $question->value_type = $this->is_value ? $this->value_type : null;

        $question->wertebereich_min = $question->wertebereich_max = $question->nachkommastellen = $question->reference = $question->field = $question->category_id = null;
        switch ($this->antworttyp) {
            case "integer":
                $question->wertebereich_min = $this->wertebereich_min;
                $question->wertebereich_max = $this->wertebereich_max;
                break;
            case "decimal":
                $question->nachkommastellen = $this->nachkommastellen;
                break;
            case "reference":
                $question->reference = $this->reference;
                $question->field = $this->field;
                $question->category_id = $this->reference == 'fahrschein' ? $this->category_id : null;
                break;
        }

        if (!$question->save()) {
            return false;
        }


        if ($this->antworttyp == 'user_defined' && sizeof($this->questionEnums) > 0) {
            /** @var FragenEnumForm $questionEnum */
            foreach ($this->questionEnums as $key => $questionEnum) {
                $questionEnum->frage_id = $question->getFrageId();
                $questionEnum->enum_id = $key + 1;
                $questionEnum->save();
            }
        } else {
            FragenEnum::deleteAll(['frage_id' => $this->id]);
        }

        return true;
    }
}