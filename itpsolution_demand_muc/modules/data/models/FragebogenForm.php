<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class FragebogenForm
 * @package app\modules\data\models
 */
class FragebogenForm extends FormModel
{
    public $fragebogen_id;
    public $name;
    public $fragebogen_file_id;

    public function rules()
    {
        return [
            [
                ['fragebogen_id', 'name', 'fragebogen_file_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $fragebogen = new Fragebogen();

        $fragebogen->fragebogen_id = $this->fragebogen_id;
        $fragebogen->fragebogen_file_id = $this->fragebogen_file_id;

        return $fragebogen->save();
    }
}