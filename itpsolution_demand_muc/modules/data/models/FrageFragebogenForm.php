<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class FrageFragebogenForm
 * @package app\modules\data\models
 */
class FrageFragebogenForm extends FormModel
{
    public $frage_id;
    public $fragebogen_id;

    public function rules()
    {
        return [
            [
                ['frage_id', 'fragebogen_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $frageFragebogen = new FrageFragebogen();

        $frageFragebogen->frage_id = $this->frage_id;
        $frageFragebogen->fragebogen_id = $this->fragebogen_id;

        $frageFragebogen->save();
    }
}