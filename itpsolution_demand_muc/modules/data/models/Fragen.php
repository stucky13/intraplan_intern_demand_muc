<?php

namespace app\modules\data\models;


/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:29
 */

use app\components\AbstractActiveRecord;
use app\modules\data\Module;
use yii\db\ActiveQuery;

/**
 * Class Fragen
 * @package app\modules\questionnaire\models
 * @property int $frage_id
 * @property string $fragestellung
 * @property string $kurztext
 * @property string $antworttyp
 * @property string $wertebereich_min
 * @property string $wertebereich_max
 * @property string $nachkommastellen
 * @property string $dt_created
 * @property int $user_created
 * @property string $dt_updated
 * @property int $user_updated
 * @property string $reference
 * @property string $field
 * @property int $category_id
 * @property int $is_georef
 * @property int $is_filter
 * @property int $is_value
 * @property int $value_type
 *
 * @property FragenEnum $fragenEnums
 */
class Fragen extends AbstractActiveRecord
{

    /**
     * @return int
     */
    public function getFrageId()
    {
        return $this->frage_id;
    }

    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @return array | null
     */
    public function getReferenceArray()
    {
        $referenceArray = null;

        switch ($this->reference) {
            case "linien":
                $referenceArray = Linien::find()->select(['linien_id AS id', 'linien_name as name'])->asArray()->all();
                break;
            case "knoten":
                $referenceArray = Knoten::find()->select(['nr AS id', 'name'])->asArray()->all();
                break;
            case "fahrschein":
                $referenceArray = Fahrschein::find()->select(['id', 'name'])->asArray()->all();
                break;
            default:
                $referenceArray = [];
        }

        return $referenceArray;
    }

    /**
     * @return string
     */
    public function getFragestellung()
    {
        return $this->fragestellung;
    }

    /**
     * @return string
     */
    public function getKurztext()
    {
        return $this->kurztext;
    }

    /**
     * @return string
     */
    public function getWertebereichMin()
    {
        return $this->wertebereich_min;
    }

    /**
     * @return string
     */
    public function getWertebereichMax()
    {
        return $this->wertebereich_max;
    }

    /**
     * @return string
     */
    public function getNachkommastellen()
    {
        return $this->nachkommastellen;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return int
     */
    public function isGeoref()
    {
        return $this->is_georef;
    }

    /**
     * @return int
     */
    public function isFilter()
    {
        return $this->is_filter;
    }

    /**
     * @return int
     */
    public function isValue()
    {
        return $this->is_value;
    }

    /**
     * retrun int
     */
    public function getValueTypeId()
    {
        return $this->value_type;
    }

    /**
     * @return ActiveQuery
     */
    public function getValueType()
    {
        return $this->hasOne(ValueType::className(), ['value_type_id' => 'value_type']);
    }

    /**
     * @return ActiveQuery
     */
    public function getFrageFragebogen()
    {
        return $this->hasMany(FrageFragebogen::className(), ['frage_id' => 'frage_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAntworten()
    {
        return $this->hasMany(Antworten::className(), ['frage_id' => 'frage_id']);
    }

    public function adjustQuestionType()
    {
        $questiontype = $this->getAntworttyp();
        $type = null;

        switch ($questiontype) {
            case "integer":
                $type = 'Ganzzahl';
                break;
            case "boolean":
                $type = 'Ja/Nein';
                break;
            case "decimal":
                $type = 'Zahl';
                break;
            case "date":
                $type = 'Datum';
                break;
            case "string":
                $type = 'Freitext';
                break;
            case "reference":
                $type = 'Referenzwert';
                break;
            case "user_defined":
                $type = 'Benutzerdefiniert';
                break;
            default:
                echo "";
        }

        return $type;
    }

    /**
     * @return string
     */
    public function getAntworttyp()
    {
        return $this->antworttyp;
    }

    public function adjustReference()
    {
        $reference = $this->getReference();
        $type = null;

        switch ($reference) {
            case "fahrschein":
                $type = 'Fahrscheintabelle';
                break;
            case "linien":
                $type = 'Linientabelle';
                break;
            case "knoten":
                $type = 'Knotentabelle';
                break;
            default:
                echo "";
        }

        return $type;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    public function adjustReferenceField()
    {
        $referenceField = $this->getField();
        $type = null;

        switch ($referenceField) {
            case "id":
                $type = 'Id';
                break;
            case "nr":
                $type = 'Nr';
                break;
            case "linien_id":
                $type = 'Id';
                break;
            case "name":
                $type = 'Name';
                break;
            case "linien_name":
                $type = 'Name';
                break;
            default:
                echo "";
        }

        return $type;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Gets the Category for the given type.
     *
     * @param int $type the category type
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return string
     */
    public function getFragenEnumsAntworten()
    {
        $questionEnums = $this->getFragenEnums()->select('value')->asArray()->all();
        $values = '';

        if ($questionEnums) {
            $values = implode(', ', array_column($questionEnums, 'value'));
        }

        return $values;
    }

    /**
     * @return ActiveQuery
     */
    public function getFragenEnums()
    {
        return $this->hasMany(FragenEnum::className(), ['frage_id' => 'frage_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'frage_id' => Module::t('questionnaire', 'question id'),
            'fragestellung' => Module::t('questionnaire', 'question'),
            'kurztext' => Module::t('questionnaire', 'short text'),
            'antworttyp' => Module::t('questionnaire', 'type of answer'),
            'wertebereich_min' => Module::t('questionnaire', 'range min'),
            'wertebereich_max' => Module::t('questionnaire', 'range max'),
            'nachkommastellen' => Module::t('questionnaire', 'number of decimal places'),
            'fragenEnums' => Module::t('questionnaire', 'user defined title'),
            'reference' => Module::t('questionnaire', 'reference'),
            'field' => Module::t('questionnaire', 'field'),
            'category_id' => Module::t('questionnaire', 'category_id'),
            'is_filter' => Module::t('questionnaire', 'is filter'),
            'is_georef' => Module::t('questionnaire', 'is georef'),
            'is_value' => Module::t('questionnaire', 'is value'),
            'value_type' => Module::t('questionnaire', 'value type'),
        ];
    }
}