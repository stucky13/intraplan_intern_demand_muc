<?php

namespace app\modules\data\models;


use app\components\AbstractCsvUpload;
use app\components\Constants;
use app\components\CsvValidator;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class LinienCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "linien/";

    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $routeCsvValidator */
    public $routeCsvValidator;

    function rules()
    {
        return array_merge(parent::rules(), [
            [
                'csvFile', 'validateRouteContent'
            ],
            [
                'csvFile',
                'required'
            ]
        ]);
    }

    /**
     * Validate income report
     * @param $attribute
     * @throws \Exception
     */
    public function validateRouteContent($attribute)
    {
        $this->routeCsvValidator = new CsvValidator($this, $this->linienCsvRules(), Constants::CSV_HEADER_LINIEN_FILE);
        $this->routeCsvValidator->loadCsvFromModel($this, $attribute);
        $this->routeCsvValidator->validateCsv();

        if (!empty($this->routeCsvValidator->getCsvCritical())) {
            foreach ($this->routeCsvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function linienCsvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_CRITICAL
            ]
        ];
    }

    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            // Look for previous LinienFile with same org
            /** @var LinienFile $previousRouteFile */
            $previousRouteFile = LinienFile::find()->where(['organisation_id' => $this->organisation_id])
                ->orderBy(['version' => SORT_DESC])->one();

            $previousVersion = 1;
            if ($previousRouteFile) {
                $previousVersion = $previousRouteFile->getVersion() + 1;
            }

            $routeFile = new LinienFile();
            $routeFile->organisation_id = $this->organisation_id;
            $routeFile->version = $previousVersion;
            $routeFile->path = $this->uploadedPath;
            $routeFile->name = $this->name;
            // Cut if last 2 digits of IP
            $routeFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $routeFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$routeFile->save()) {
                throw new Exception("Could not save LinienFile to DB");
            }

            $routeFileId = $routeFile->getPrimaryKey();

            if (!empty($this->routeCsvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->routeCsvValidator->getCsvErrors()));
                $routeFile->path_errors = $errorFile;
                $routeFile->has_errors = true;
            } else {
                if ($previousRouteFile) {
                    $previousRouteFile->is_valid = false;
                    $previousRouteFile->save();
                }
                $routeFile->is_valid = true;
            }
            $routeFile->save();

            foreach ($this->routeCsvValidator->getCsvErrors() as $message) {
                $routeFileError = new LinienFileError();
                $routeFileError->linien_file_id = $routeFileId;
                $routeFileError->error_type = CsvValidator::ERROR_NORMAL;
                $routeFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $routeFileError->detail = $message;
                if (!$routeFileError->save()) {
                    throw new Exception("Could not save LinienFileError to DB");
                }
            }

            foreach ($this->routeCsvValidator->getCsvWarnings() as $message) {
                $routeFileError = new LinienFileError();
                $routeFileError->linien_file_id = $routeFileId;
                $routeFileError->error_type = CsvValidator::ERROR_WARNING;
                $routeFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $routeFileError->detail = $message;
                if (!$routeFileError->save()) {
                    throw new Exception("Could not save LinienFileError to DB");
                }
            }

            if (!$this->import($routeFile)) {
                throw new Exception("Could not save Linien to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $routeFile LinienFile
     * @return bool
     * @throws Exception
     */
    private function import($routeFile)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($routeFile->getPath());
        $routeFileId = $routeFile->getLinienFileId();
        $routeFileUserCreated = $routeFile->getUserCreated();
        $prevRouteId = 0;

        try {
            foreach ($csvData as $rowIndex => $row) {
                $lineValues = array_combine(array_map('strtolower', Constants::CSV_HEADER_LINIEN_FILE), $row);

                if ($prevRouteId !== $lineValues['linien_id']) {
                    $routeData = new LinienForm();
                    $routeData->linien_id = $lineValues['linien_id'];
                    $routeData->linien_name = $lineValues['linien_name'];
                    $routeData->linien_file_id = $routeFileId;

                    if ($routeData->validate()) {
                        $routeData->save();
                    } else {
                        foreach ($routeData->errors as $field => $fieldErrors) {
                            foreach ($fieldErrors as $error) {
                                $fileError = new LinienFileError();
                                $fileError->linien_file_id = $routeFileId;
                                $fileError->user_created = $routeFileUserCreated;
                                $fileError->detail = "Zeile " . $row . ": Feld " . $field . ": " . $error;
                                $fileError->save();
                            }
                        }
                    }

                    $prevRouteId = $routeData->linien_id;
                    LinienKnoten::deleteAll(['linien_id' => $lineValues['linien_id']]);
                }

                $routeStationData = new LinienKnotenForm();
                $routeStationData->linien_id = $lineValues['linien_id'];
                $routeStationData->nr = $lineValues['nr'];
                $routeStationData->hst_sort = $lineValues['hst_sort'];

                if ($routeStationData->validate()) {
                    $routeStationData->save();
                } else {
                    foreach ($routeStationData->errors as $field => $fieldErrors) {
                        foreach ($fieldErrors as $error) {
                            $fileError = new LinienFileError();
                            $fileError->linien_file_id = $routeFileId;
                            $fileError->user_created = $routeFileUserCreated;
                            $fileError->detail = "Zeile " . $rowIndex . ": Feld " . $field . ": " . $error;
                            $fileError->save();
                        }
                    }
                }

                unset($lineValues);
                unset($fileError);
                unset($routeData);
                gc_collect_cycles();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return true;
    }
}