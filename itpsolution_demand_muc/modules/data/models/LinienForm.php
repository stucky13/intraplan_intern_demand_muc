<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class LinienForm
 * @package app\modules\data\models
 */
class LinienForm extends FormModel
{
    public $linien_id;
    public $linien_name;
    public $linien_file_id;

    public function rules()
    {
        return [
            [
                ['linien_id', 'linien_name', 'linien_file_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        /** @var Linien $linien */
        $linien = Linien::findOne($this->linien_id);
        if (!$linien) {
            $linien = new Linien();
        }

        $linien->linien_id = $this->linien_id;
        $linien->linien_name = $this->linien_name;
        $linien->linien_file_id = $this->linien_file_id;

        return $linien->save();
    }
}