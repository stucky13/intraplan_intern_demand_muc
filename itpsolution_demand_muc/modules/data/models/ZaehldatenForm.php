<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class ZaehldatenForm
 * @package app\modules\data\models
 */
class ZaehldatenForm extends FormModel
{
    public $zaehldaten_id;
    public $strecke;
    public $linie;
    public $zugnummer;
    public $zeitschicht;
    public $richtung;
    public $hst_sort;
    public $bhf_id;
    public $bhf_name;
    public $uhrzeit;
    public $zaehlung;
    public $ein;
    public $aus;
    public $bes;
    public $bes1;
    public $bes2;
    public $ein_f;
    public $aus_f;
    public $c_bes_f;
    public $abschnitt_km;
    public $f_tag;
    public $f_jahr;
    public $p_tag;
    public $r_tag;
    public $pkm_tag;
    public $rkm_tag;
    public $p_jahr;
    public $r_jahr;
    public $pkm_jahr;
    public $rkm_jahr;
    public $zaehldaten_file_id;

    public function rules()
    {
        return [
            [
                ["zaehldaten_id",
                    "strecke",
                    "linie",
                    "zugnummer",
                    "zeitschicht",
                    "richtung",
                    "hst_sort",
                    "bhf_id",
                    "bhf_name",
                    "uhrzeit",
                    "zaehlung",
                    "ein",
                    "aus",
                    "bes",
                    "bes1",
                    "bes2",
                    "ein_f",
                    "aus_f",
                    "c_bes_f",
                    "abschnitt_km",
                    "f_tag",
                    "f_jahr",
                    "p_tag",
                    "r_tag",
                    "pkm_tag",
                    "rkm_tag",
                    "p_jahr",
                    "r_jahr",
                    "pkm_jahr",
                    "rkm_jahr"],
                'string'
            ],
        ];
    }

    public function save()
    {
        $zaehldaten = new Zaehldaten();

        $zaehldaten->strecke = $this->strecke;
        $zaehldaten->linie = $this->linie;
        $zaehldaten->zugnummer = $this->zugnummer;
        $zaehldaten->zeitschicht = $this->zeitschicht;
        $zaehldaten->richtung = $this->richtung;
        $zaehldaten->hst_sort = $this->hst_sort;
        $zaehldaten->bhf_id = $this->bhf_id;
        $zaehldaten->bhf_name = $this->bhf_name;
        $zaehldaten->uhrzeit = $this->uhrzeit;
        $zaehldaten->zaehlung = $this->zaehlung;
        $zaehldaten->ein = $this->ein;
        $zaehldaten->aus = $this->aus;
        $zaehldaten->bes = $this->bes;
        $zaehldaten->bes1 = $this->bes1;
        $zaehldaten->bes2 = $this->bes2;
        $zaehldaten->ein_f = $this->ein_f;
        $zaehldaten->aus_f = $this->aus_f;
        $zaehldaten->c_bes_f = $this->c_bes_f;
        $zaehldaten->abschnitt_km = is_numeric(str_replace(",", ".", $this->abschnitt_km)) ? str_replace(",", ".", $this->abschnitt_km) : null;
        $zaehldaten->f_tag = is_numeric(str_replace(",", ".", $this->f_tag)) ? str_replace(",", ".", $this->f_tag) : null;
        $zaehldaten->f_jahr = $this->f_jahr;
        $zaehldaten->p_tag = $this->p_tag;
        $zaehldaten->r_tag = $this->r_tag;
        $zaehldaten->pkm_tag = is_numeric(str_replace(",", ".", $this->pkm_tag)) ? str_replace(",", ".", $this->pkm_tag) : null;
        $zaehldaten->rkm_tag = is_numeric(str_replace(",", ".", $this->rkm_tag)) ? str_replace(",", ".", $this->rkm_tag) : null;
        $zaehldaten->p_jahr = $this->p_jahr;
        $zaehldaten->r_jahr = $this->r_jahr;
        $zaehldaten->pkm_jahr = is_numeric(str_replace(",", ".", $this->pkm_jahr)) ? str_replace(",", ".", $this->pkm_jahr) : null;
        $zaehldaten->rkm_jahr = is_numeric(str_replace(",", ".", $this->rkm_jahr)) ? str_replace(",", ".", $this->rkm_jahr) : null;
        $zaehldaten->zaehldaten_file_id = $this->zaehldaten_file_id;

        return $zaehldaten->save();
    }
}