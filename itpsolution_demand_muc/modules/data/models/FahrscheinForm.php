<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class FahrscheinForm
 * @package app\modules\data\models
 */
class FahrscheinForm extends FormModel
{
    public $id;
    public $name;
    public $kategorie1;
    public $kategorie2;
    public $kategorie3;
    public $kategorie4;
    public $fahrschein_file_id;

    public function rules()
    {
        return [
            [
                ['id', 'name', 'kategorie1', 'kategorie2', 'kategorie3', 'kategorie4', 'fahrschein_file_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $fahrschein = Fahrschein::findOne($this->id);
        if (!$fahrschein) {
            $fahrschein = new Fahrschein();
        }

        $fahrschein->id = $this->id;
        $fahrschein->name = $this->name;
        $fahrschein->kategorie1 = $this->kategorie1;
        $fahrschein->kategorie2 = $this->kategorie2;
        $fahrschein->kategorie3 = $this->kategorie3;
        $fahrschein->kategorie4 = $this->kategorie4;
        $fahrschein->fahrschein_file_id = $this->fahrschein_file_id;

        return $fahrschein->save();
    }
}