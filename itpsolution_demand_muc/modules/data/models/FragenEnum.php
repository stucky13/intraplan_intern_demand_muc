<?php

namespace app\modules\questionnaire\models;


/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:45
 */

namespace app\modules\data\models;

use app\components\AbstractActiveRecord;

/**
 * @package app\modules\data\models
 * @property int $frage_id
 * @property int $enum_id
 * @property string $value
 **/
class FragenEnum extends AbstractActiveRecord
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getFrageId()
    {
        return $this->frage_id;
    }

    /**
     * @return int
     */
    public function getEnumId()
    {
        return $this->enum_id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

}