<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 25.01.2019
 * Time: 12:12
 */

namespace app\modules\data\models;

use app\components\AbstractActiveRecord;

/**
 * Class UmsatzType
 * @package app\modules\data\models
 *
 * @property int $value_type_id
 * @property string $name
 * @property string $shortname
 * @property string $description
 * @property string $unit
 * @property int $aufteilungsverfahren
 */
class ValueType extends AbstractActiveRecord
{
    public static function tableName()
    {
        return "{{value_type}}";
    }

    /**
     * @return int
     */
    public function getValueTypeId()
    {
        return $this->value_type_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return int
     */
    public function getAufteilungsverfahren()
    {
        return $this->aufteilungsverfahren;
    }

}