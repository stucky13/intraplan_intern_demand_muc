<?php

namespace app\modules\data\models;

use app\components\FormModel;
use app\components\SessionUtil;

/**
 * Class ErhebungsdatenForm
 * @package app\modules\data\models
 */
class ErhebungsdatenForm extends FormModel
{

    public $questionnaire_id;
    public $questionnaire_file;
    public $survey_id;
    public $survey_file;
    public $count_data_id;
    public $count_data_file;
    public $year;
    public $period;
    public $description;
    public $note;

    public function rules()
    {
        return [
            [
                ['questionnaire_id', 'survey_id', 'count_data_id', 'year', 'period', 'description', 'note'],
                'required'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'year' => 'Erhebungsjahr',
            'questionnaire_file' => 'Fragebogen Datei',
            'survey_file' => 'Befragungsdaten Datei',
            'count_data_file' => 'Zähldaten Datei',
            'period' => 'Periode',
            'description' => 'Beschreibung',
            'note' => 'Notiz'

        ];
    }

    public function save()
    {
        $erhebungsdaten = new Erhebungsdaten();

        $erhebungsdaten->questionnaire_id = $this->questionnaire_id;
        $erhebungsdaten->survey_id = $this->survey_id;
        $erhebungsdaten->count_data_id = $this->count_data_id;
        $erhebungsdaten->description = $this->description;
        $erhebungsdaten->year = $this->year;
        $erhebungsdaten->period = $this->period;
        $erhebungsdaten->note = $this->note;
        $erhebungsdaten->organisation_id = SessionUtil::getSessionOrganisationId();

        return $erhebungsdaten->save();
    }
}