<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 15:15
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Linien
 * @package app\modules\data\models
 *
 * @property int $linien_id
 * @property string $linien_name
 * @property int $linien_file_id
 */
class Linien extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'linien_id' => Module::t('data', 'route id'),
            'linien_name' => Module::t('data', 'route name'),
            'linien_file_id' => Module::t('data', 'route file id')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->linien_id;
    }

    /**
     * @return int
     */
    public function getLinienFileId()
    {
        return $this->linien_file_id;
    }

    /**
     * @return string
     */
    public function getLinienName()
    {
        return $this->linien_name;
    }

    /**
     * Returns all routeknots.
     *
     * @return ActiveQuery
     */
    public function getRouteKnots()
    {
        return $this->hasMany(LinienKnoten::className(), ['linien_id' => 'linien_id']);
    }

    /**
     * Returns all knots for this route without sorting.
     *
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getKnots()
    {
        return $this->hasMany(LinienKnoten::className(), ['linien_id' => 'linien_id'])
            ->viaTable('linien_knoten', ['nr' => 'nr']);
    }
}