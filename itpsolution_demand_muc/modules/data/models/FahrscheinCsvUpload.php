<?php

namespace app\modules\data\models;

use app\components\AbstractCsvUpload;
use app\components\Constants;
use app\components\CsvValidator;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class FahrscheinCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "fahrschein/";

    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $ticketCsvValidator */
    public $ticketCsvValidator;

    function rules()
    {
        return array_merge(parent::rules(), [
            [
                'csvFile', 'validateTicketContent'
            ],
            [
                'csvFile',
                'required'
            ]
        ]);
    }

    /**
     * Validate income report
     * @param $attribute
     * @throws \Exception
     */
    public function validateTicketContent($attribute)
    {
        $this->ticketCsvValidator = new CsvValidator($this, $this->ticketCsvRules(), Constants::CSV_HEADER_FAHRSCHEIN_FILE);
        $this->ticketCsvValidator->loadCsvFromModel($this, $attribute);
        $this->ticketCsvValidator->validateCsv();

        if (!empty($this->ticketCsvValidator->getCsvCritical())) {
            foreach ($this->ticketCsvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function ticketCsvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_CRITICAL
            ]
        ];
    }

    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            // Look for previous StreckenbandFile with same org
            /** @var FahrscheinFile $previousTicketFile */
            $previousTicketFile = LinienFile::find()->where(['organisation_id' => $this->organisation_id])
                ->orderBy(['version' => SORT_DESC])->one();

            $previousVersion = $previousTicketFile ? $previousTicketFile->getVersion() + 1 : 1;

            $ticketFile = new FahrscheinFile();
            $ticketFile->organisation_id = $this->organisation_id;
            $ticketFile->version = $previousVersion;
            $ticketFile->path = $this->uploadedPath;
            $ticketFile->name = $this->name;
            // Cut if last 2 digits of IP
            $ticketFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $ticketFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$ticketFile->save()) {
                throw new Exception("Could not save TicketFile to DB");
            }

            $ticketFileId = $ticketFile->getPrimaryKey();

            if (!empty($this->ticketCsvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->ticketCsvValidator->getCsvErrors()));
                $ticketFile->path_errors = $errorFile;
                $ticketFile->has_errors = true;
            } else {
                if ($previousTicketFile) {
                    $previousTicketFile->is_valid = false;
                    $previousTicketFile->save();
                }
                $ticketFile->is_valid = true;
            }
            $ticketFile->save();

            foreach ($this->ticketCsvValidator->getCsvErrors() as $message) {
                $ticketFileError = new FahrscheinFileError();
                $ticketFileError->fahrschein_file_id = $ticketFileId;
                $ticketFileError->error_type = CsvValidator::ERROR_NORMAL;
                $ticketFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $ticketFileError->detail = $message;
                if (!$ticketFileError->save()) {
                    throw new Exception("Could not save FahrscheinFileError to DB");
                }
            }

            foreach ($this->ticketCsvValidator->getCsvWarnings() as $message) {
                $ticketFileError = new FahrscheinFileError();
                $ticketFileError->fahrschein_file_id = $ticketFileId;
                $ticketFileError->error_type = CsvValidator::ERROR_WARNING;
                $ticketFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $ticketFileError->detail = $message;
                if (!$ticketFileError->save()) {
                    throw new Exception("Could not save FahrscheinFileError to DB");
                }
            }

            if (!$this->import($ticketFile)) {
                throw new Exception("Could not save Fahrscheine to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            //print_r($e);die();
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $ticketFile FahrscheinFile
     * @return bool
     * @throws Exception
     */
    private function import($ticketFile)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($ticketFile->getPath());
        $ticketFileId = $ticketFile->getFahrscheinFileId();
        $ticketFileUserCreated = $ticketFile->getUserCreated();
        /** @var Category[] $categories */
        $categories = Category::find()->all();

        try {
            foreach ($csvData as $index => $row) {
                $lineValues = array_combine(array_map('strtolower', Constants::CSV_HEADER_FAHRSCHEIN_FILE), $row);

                $ticketData = new FahrscheinForm();
                foreach ($lineValues as $key => $value) {
                    if (in_array($key, ['id', 'name'])) {
                        $ticketData->$key = $value;
                    } else {
                        $category = null;
                        foreach ($categories as $checkCategory) {
                            if ($checkCategory->getTitle() === $value) {
                                $category = $checkCategory;
                                break;
                            }
                        }
                        if (!$category) {
                            /** @var Category $category */
                            $category = new Category();
                            $category->title = $value;

                            if (!$category->save()) {
                                throw new Exception("Could not save categories to DB");
                            }
                            $categories[] = $category;
                        }
                        $ticketData->$key = $category->category_id;
                    }
                }

                $ticketData->fahrschein_file_id = $ticketFileId;

                if ($ticketData->validate()) {
                    $ticketData->save();
                } else {
                    foreach ($ticketData->errors as $field => $fieldErrors) {
                        foreach ($fieldErrors as $error) {
                            $fileError = new FahrscheinFileError();
                            $fileError->fahrschein_file_id = $ticketFileId;
                            $fileError->user_created = $ticketFileUserCreated;
                            $fileError->detail = "Zeile " . $index . ": Feld " . $field . ": " . $error;
                            $fileError->save();
                        }
                    }
                }

                unset($lineValues);
                unset($fileError);
                unset($ticketData);
                gc_collect_cycles();
            }
            $transaction->commit();
        } catch (Exception $e) {
            print_r($e);
            $transaction->rollBack();
        }

        return true;
    }
}