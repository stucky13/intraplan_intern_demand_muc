<?php

namespace app\modules\questionnaire\models;


/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:48
 */

namespace app\modules\data\models;

use yii\db\ActiveRecord;

/**
 * Class FrageFragebogen
 * @package app\modules\data\models
 * @property int $frage_id
 * @property int $fragebogen_id
 */
class FrageFragebogen extends ActiveRecord
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getFrageId()
    {
        return $this->frage_id;
    }

    /**
     * @return int
     */
    public function getFragebogenId()
    {
        return $this->fragebogen_id;
    }

}