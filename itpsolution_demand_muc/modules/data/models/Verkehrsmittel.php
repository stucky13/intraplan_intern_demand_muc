<?php

namespace app\modules\data\models;


use Yii;
use yii\db\ActiveRecord;

/**
 * Class Verkehrsmittel
 * @package app\modules\data\models
 *
 * @property int $nr
 * @property string $name
 */
class Verkehrsmittel extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nr' => Yii::t('app', 'nr'),
            'name' => Yii::t('app', 'name')
        ];
    }

    /**
     * @return int
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}