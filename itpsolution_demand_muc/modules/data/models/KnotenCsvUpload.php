<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 16.01.2019
 * Time: 14:27
 */

namespace app\modules\data\models;


use app\components\AbstractCsvUpload;
use app\components\Constants;
use app\components\CsvValidator;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class KnotenCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "knoten/";

    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $knotenCsvValidator */
    public $knotenCsvValidator;

    function rules()
    {
        return array_merge(parent::rules(), [
            [
                'csvFile', 'validateStationContent'
            ],
            [
                'csvFile',
                'required'
            ]
        ]);
    }

    /**
     * Validate income report
     * @param $attribute
     * @throws \Exception
     */
    public function validateStationContent($attribute)
    {
        $this->knotenCsvValidator = new CsvValidator($this, $this->knotenCsvRules(), Constants::CSV_HEADER_KNOTEN_FILE);
        $this->knotenCsvValidator->loadCsvFromModel($this, $attribute);
        $this->knotenCsvValidator->validateCsv();

        if (!empty($this->knotenCsvValidator->getCsvCritical())) {
            foreach ($this->knotenCsvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function knotenCsvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_CRITICAL
            ]
        ];
    }

    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            // Look for previous KnotenFile with same org
            /** @var KnotenFile $previousKnotenFile */
            $previousKnotenFile = KnotenFile::find()->where(['organisation_id' => $this->organisation_id])
                ->orderBy(['version' => SORT_DESC])->one();

            $previousVersion = 1;
            if ($previousKnotenFile) {
                $previousVersion = $previousKnotenFile->getVersion() + 1;
            }

            $knotenFile = new KnotenFile();
            $knotenFile->organisation_id = $this->organisation_id;
            $knotenFile->version = $previousVersion;
            $knotenFile->path = $this->uploadedPath;
            $knotenFile->name = $this->name;
            // Cut if last 2 digits of IP
            $knotenFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $knotenFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$knotenFile->save()) {
                throw new Exception("Could not save KnotenFile to DB");
            }

            $knotenFileId = $knotenFile->getPrimaryKey();

            if (!empty($this->knotenCsvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->knotenCsvValidator->getCsvErrors()));
                $knotenFile->path_errors = $errorFile;
                $knotenFile->has_errors = true;
            } else {
                if ($previousKnotenFile) {
                    $previousKnotenFile->is_valid = false;
                    $previousKnotenFile->save();
                }
                $knotenFile->is_valid = true;
            }
            $knotenFile->save();

            foreach ($this->knotenCsvValidator->getCsvErrors() as $message) {
                $knotenFileError = new KnotenFileError();
                $knotenFileError->knoten_file_id = $knotenFileId;
                $knotenFileError->error_type = CsvValidator::ERROR_NORMAL;
                $knotenFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $knotenFileError->detail = $message;
                if (!$knotenFileError->save()) {
                    throw new Exception("Could not save KnotenFileError to DB");
                }
            }

            foreach ($this->knotenCsvValidator->getCsvWarnings() as $message) {
                $knotenFileError = new KnotenFileError();
                $knotenFileError->knoten_file_id = $knotenFileId;
                $knotenFileError->error_type = CsvValidator::ERROR_WARNING;
                $knotenFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $knotenFileError->detail = $message;
                if (!$knotenFileError->save()) {
                    throw new Exception("Could not save KnotenFileError to DB");
                }
            }

            if (!$this->import($knotenFile)) {
                throw new Exception("Could not save Knoten to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            print_r($e);
            die();
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $knotenFile KnotenFile
     * @return bool
     * @throws Exception
     */
    private function import($knotenFile)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($knotenFile->getPath());
        $knotenFileId = $knotenFile->getKnotenFileId();
        $knotenFileUserCreated = $knotenFile->getUserCreated();

        try {
            foreach ($csvData as $rowIndex => $row) {
                $lineValues = array_combine(array_map('strtolower', Constants::CSV_HEADER_KNOTEN_FILE), $row);

                /** @var KnotenForm $knotenData */
                $knotenData = new KnotenForm();
                foreach ($lineValues as $key => $value) {
                    $knotenData->$key = $value;
                }
                $knotenData->knoten_file_id = $knotenFileId;

                if ($knotenData->validate()) {
                    $knotenData->save();
                } else {
                    foreach ($knotenData->errors as $field => $fieldErrors) {
                        foreach ($fieldErrors as $error) {
                            $fileError = new KnotenFileError();
                            $fileError->knoten_file_id = $knotenFileId;
                            $fileError->user_created = $knotenFileUserCreated;
                            $fileError->detail = "Zeile " . $rowIndex . ": Feld " . $field . ": " . $error;
                            $fileError->save();
                        }
                    }
                }

                unset($lineValues);
                unset($fileError);
                unset($knotenData);
                gc_collect_cycles();
            }
            $transaction->commit();
        } catch (Exception $e) {
            print_r($e);
            $transaction->rollBack();
        }

        return true;
    }
}