<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 22.02.2019
 * Time: 10:16
 */

namespace app\modules\account\models;

use yii\base\Model;

class TeilnetzSessionForm extends Model
{

    public $teilnetz;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                'teilnetz',
                'string'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'teilnetz' => 'Teilnetz'
        ];
    }

}