<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 21.01.2019
 * Time: 09:24
 */

use app\modules\account\Module;
use app\modules\account\widgets\DashboardPanel;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * @var ActiveQuery $org
 * @var ActiveQuery $surveyData
 */

$this->title = Module::t('account', 'survey data');
?>

<div class="col-lg-12">
    <div class="col-md-2"></div>
    <div class="col-lg-8">
        <?php DashboardPanel::begin() ?>
        <h3>Teilnetz <?= $org->getTitle() ?> - <?= $surveyData->getYear() ?>:</h3>
        <h3>Beschreibung:</h3>
        <ul>
            <?= $surveyData->getDescription() ?>
        </ul>
        <h3>Besonderheiten:</h3>
        <ul>
            <!-- TODO: liste von Besonderheiten -->
        </ul>
        <h3>Fragebogen:</h3>
        <table class="table panel-color">
            <thead>
            <tr>
                <th class="mediumButton gridview-header">Merkmal</th>
                <th class="mediumButton gridview-header">Fragestellung</th>
                <th class="mediumButton gridview-header">Filter</th>
            </tr>
            </thead>
            <tbody>
            <!-- TODO: Tablebody -->
            </tbody>
        </table>
        <?php DashboardPanel::end(); ?>
        <?= Html::a(Module::t('admin', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
    </div>
</div>

