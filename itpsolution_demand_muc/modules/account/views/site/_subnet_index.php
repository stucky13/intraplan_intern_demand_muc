<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 18.01.2019
 * Time: 12:09
 */

use app\modules\account\assets\AccountAsset;
use app\modules\account\Module;
use yii\db\ActiveQuery;

/**
 * @var ActiveQuery $orgs
 */
AccountAsset::register($this);
$this->title = Module::t('account', 'survey data');
?>
<div id="indexContent">

    <?php
    if ($orgs->count() == 0) : ?>
        <?= Module::t('account', 'no organisations'); ?>
    <?php else : ?>
        <h4>Teilnetzübersicht:</h4>
        <table>
            <?php
            foreach ($orgs->all() as $org): ?>
                <li><a style="font-size: 14pt; color:white; cursor: pointer"
                       onclick="setTeilnetz(<?= $org->getId() ?>)">
                        <?= $org->getTitle() ?>
                    </a></li>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
