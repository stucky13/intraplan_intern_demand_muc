<?php

use app\components\Constants;
use app\modules\account\assets\AccountAsset;
use app\modules\account\widgets\DashboardPanel;
use app\modules\account\widgets\NavigationWidget;
use app\modules\account\widgets\NewUsersWidget;
use app\modules\account\widgets\ValidateUsersWidget;
use yii\db\ActiveQuery;

/**
 * @var ActiveQuery $newUsers
 * @var ActiveQuery $oldUsers
 * @var ActiveQuery $newReports
 * @var ActiveQuery $eaPeriods
 */

AccountAsset::register($this);

$this->title = Yii::t('app', 'ITPidea');

?>


<?php DashboardPanel::begin() ?>
<div id="subnet_selection">
</div>
<div id="data_selection">
</div>
<?php DashboardPanel::end(); ?>

<div class="row">
    <div class="col-md-3"></div>
    <?php DashboardPanel::begin() ?>
    <?= NavigationWidget::widget() ?>
    <?php DashboardPanel::end(); ?>
    <div class="col-lg-3">
    </div>
    <div class="col-lg-12">
        <div class="col-md-2"></div>
        <?php if (Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_USER)): ?>
        <div class="col-lg-8">
            <?php DashboardPanel::begin(['title' => 'Neue Benutzer']) ?>
            <?= NewUsersWidget::widget(['users' => $newUsers]); ?>
            <?php DashboardPanel::end(); ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-12">
            <?php
            if (Yii::$app->getUser()->getIdentity()->isRepresentative()): ?>
            <div class="col-lg-12">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-8">
                    <?php DashboardPanel::begin(['title' => 'Inaktive Benutzer']) ?>
                    <?= ValidateUsersWidget::widget(['users' => $oldUsers]); ?>
                    <?php DashboardPanel::end(); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


