$(document).ready(function () {
    getSurveyDataIndex();
    getSurveyDataView();
});

function setTeilnetz(id) {
    $.ajax({
        type: 'POST',
        url: baseUrl + 'account/site/set-teilnetz-id-in-session',
        data: {
            'TeilnetzSessionForm[teilnetz]': id
        },
        success: function () {
            getSurveyDataView();
        }
    });
}

function getSurveyDataIndex() {
    $.ajax({
        type: 'GET',
        url: baseUrl + 'account/site/subnet-index',
        success: function (data) {
            $("#subnet_selection").html(data);
        }
    });
}

function getSurveyDataView() {
    $.ajax({
        type: 'GET',
        url: baseUrl + 'account/site/data-index',
        success: function (data) {
            $("#data_selection").html(data);
        }
    });
}