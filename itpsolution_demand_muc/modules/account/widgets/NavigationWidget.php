<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 14.02.2018
 * Time: 15:48
 */

namespace app\modules\account\widgets;

use yii\base\Widget;

/**
 * Class NavigationWidget
 * @package app\modules\account\widgets
 */
class NavigationWidget extends Widget
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_navigation_widget');
    }
}
