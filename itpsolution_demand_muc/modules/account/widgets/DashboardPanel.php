<?php

namespace app\modules\account\widgets;

use yii\bootstrap\Html;
use yii\bootstrap\Widget;

/**
 * Class DashboardPanel
 * @package app\modules\core\widgets
 */
class DashboardPanel extends Widget
{
    /** @var string the panel title */
    public $title;

    /** @var string the panel subtitle */
    public $subtitle;

    /**
     * @inheritDoc
     */
    public function init()
    {


        parent::init();
        ob_start();
        ob_implicit_flush(false);
    }

    /**
     * @inheritDoc
     */
    public function run()
    {
        $content = ob_get_clean();

        echo Html::beginTag("div", ['class' => 'panel panel-default panel-color', 'style' => 'display: block']);
        echo Html::beginTag("div", ['class' => 'panel-body']);
        if ($this->title != null) {
            echo Html::beginTag("div", ['class' => 'dashboard-panel-title']);
        }
        echo Html::beginTag('h2');
        echo $this->title;
        if ($this->subtitle) {
            echo Html::tag('small', $this->subtitle);
        }
        echo Html::endTag('h2');
        if ($this->title != null) {
            echo Html::endTag('div');
        }

        echo $content;
        echo Html::endTag('div');
        echo Html::endTag('div');
    }
}