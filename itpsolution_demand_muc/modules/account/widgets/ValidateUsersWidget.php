<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 27.02.2018
 * Time: 13:40
 */

namespace app\modules\account\widgets;

use yii\base\Widget;
use yii\db\ActiveQuery;

/**
 * Class ValidateUsersWidget
 * @package app\modules\account\widgets
 */
class ValidateUsersWidget extends Widget
{
    /**
     * @var ActiveQuery $users the new users that have to be activated
     */
    public $users;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('_validate_users_widget', [
            'users' => $this->users
        ]);
    }

}