<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 14.02.2018
 * Time: 16:22
 */

/**
 * @var $title string the title
 * @var $path string the path
 */

use yii\helpers\Url; ?>
<div class="col-lg-4 navigation-panel">
    <div><a href="<?= Url::to([$path]) ?>">
            <div class="thumbnail-color mediumButton" style="padding: 0">
                <div class="caption navigation-label">
                    <h4><?= $title ?></h4>
                </div>
            </div>
        </a>
    </div>
</div>
