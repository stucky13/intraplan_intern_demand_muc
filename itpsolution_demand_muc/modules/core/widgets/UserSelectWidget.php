<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 16:44
 */

namespace app\modules\core\widgets;

use app\modules\core\models\User;
use yii\base\Model;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/**
 * Class UserSelectWidget
 *
 * @package app\modules\core\widgets
 */
class UserSelectWidget extends Widget
{
    /**
     * @var ActiveForm the active form widget
     */
    public $form;

    /**
     * @var Model the associated model
     */
    public $model;

    /**
     * @var string the attribute name
     */
    public $attribute;

    /**
     * @var array the update route
     */
    public $updateRoute;

    /**
     * @var int the organisation Id
     */
    public $organisationId;

    /**
     * @var boolean indicates whether this widget is read only
     */
    public $readOnly = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_user_select', [
            'form' => $this->form,
            'model' => $this->model,
            'attribute' => $this->attribute,
            'updateUrl' => Url::to($this->updateRoute),
            'organisationId' => $this->model->organisationId,
            'initialText' => $this->getInitialText(),
            'readOnly' => $this->readOnly
        ]);
    }

    /**
     * Returns the initial text for the select
     */
    private function getInitialText()
    {
        $userId = $this->model->{$this->attribute};

        // Return an empty string if nothing is selected
        if (empty($userId) || !($user = User::findOne($userId))) {
            return "";
        }

        return $user->getFullName();
    }
}