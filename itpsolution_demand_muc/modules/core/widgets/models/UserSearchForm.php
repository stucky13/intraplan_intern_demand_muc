<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 16:56
 */

namespace app\modules\core\widgets\models;

use app\modules\core\models\User;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class UserSearchForm
 *
 * @package app\modules\core\widgets\models
 */
class UserSearchForm extends Model
{
    public $searchInput;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'searchInput',
                'required'
            ],
            [
                'searchInput',
                'string',
                'min' => 3
            ]
        ];
    }

    /**
     * @return ActiveQuery the query that returns all users that match the search input
     */
    public function search()
    {
        return User::find()
            ->where(['LIKE', 'first_name', $this->searchInput])
            ->orWhere(['LIKE', 'last_name', $this->searchInput])
            ->orWhere(['=', 'user_id', $this->searchInput])
            ->limit(20);
    }
}