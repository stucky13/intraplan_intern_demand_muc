<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.08.2019
 * Time: 11:11
 */

namespace app\modules\core\components;


use app\components\Constants;
use Yii;
use yii\db\ActiveQuery;

class BerechnungQuery extends ActiveQuery
{
    public function init()
    {
        if (is_a(Yii::$app, 'yii\web\Application') && Yii::$app->getUser()->can(Constants::ADMIN)) {
            $this->all();
        } else{
            $this->andWhere(['user_id' => Yii::$app->getUser()->getId()]);
        }
        parent::init();
    }
}