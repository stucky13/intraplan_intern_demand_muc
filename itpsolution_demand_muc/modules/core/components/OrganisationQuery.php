<?php

namespace app\modules\core\components;

use app\components\Constants;
use Yii;
use yii\db\ActiveQuery;

class OrganisationQuery extends ActiveQuery
{

    public function init()
    {
        $this->andWhere(['`organisation`.`is_deleted`' => 0]);
        if (is_a(Yii::$app, 'yii\web\Application') && Yii::$app->getUser()->can(Constants::ADMIN)) {
            $this->orWhere(['`organisation`.`is_deleted`' => 1]);
        }
        parent::init();
    }
}