<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 13.03.2018
 * Time: 11:17
 */

namespace app\modules\core\components;

use app\components\Constants;
use Yii;
use yii\db\ActiveQuery;

class OrganisationTypeQuery extends ActiveQuery
{
    public function init()
    {
        $this->andWhere(['`organisation_type`.`is_deleted`' => 0]);
        if (Yii::$app->getUser()->can(Constants::ADMIN)) {
            $this->orWhere(['`organisation_type`.`is_deleted`' => 1]);
        }
        parent::init();
    }
}