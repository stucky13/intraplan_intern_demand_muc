<?php

namespace app\modules\core\models;

use yii\db\ActiveRecord;

/**
 * Class AuthAssignment
 * @package app\modules\core\models
 *
 * @property string $item_name
 * @property int $user_id
 * @property int $data
 */
class AuthAssignment extends ActiveRecord
{
    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->item_name;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getData()
    {
        return $this->data;
    }
}