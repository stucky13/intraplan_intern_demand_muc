<?php

namespace app\modules\core\models;

use app\modules\core\Module;
use yii\db\ActiveRecord;

/**
 * Class UserOrganisation
 * @package app\modules\core\models
 *
 * @property $user_id int
 * @property $organisation_id int
 * @property $dt_created date
 * @property $dt_updated date
 */
class UserOrganisation extends ActiveRecord
{

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%user_organisation}}';
    }

    /**
     * @inheritDoc
     */
    public function getAttributeLabel($attribute)
    {
        return [
            'organisation_id' => Module::t('core', 'Organisation id'),
            'user_id' => Module::t('core', 'User id'),
        ];
    }

    /**
     * @return int
     */
    public function getOrganisationId()
    {
        return $this->organisation_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}