<?php

namespace app\modules\core\models;

use app\components\AbstractActiveRecord;
use app\modules\core\components\OrganisationTypeQuery;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class OrganisationType
 * @package app\modules\core\models
 *
 * @property int $organisation_type_id
 * @property string $dt_created
 * @property string $dt_updated
 * @property int $user_created
 * @property int $user_updated
 * @property string $title
 * @property string $short_title
 * @property Organisation[] $organisations
 * @property int $is_deleted
 */
class OrganisationType extends AbstractActiveRecord
{
    /**
     * @return OrganisationTypeQuery|ActiveQuery
     */
    public static function find()
    {
        return new OrganisationTypeQuery(get_called_class());
    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'organisation_type_id' => Yii::t('app', 'organisation type id'),
            'user_created' => Yii::t('app', 'user created'),
            'dt_created' => Yii::t('app', 'dt created'),
            'user_updated' => Yii::t('app', 'user updated'),
            'dt_updated' => Yii::t('app', 'dt updated'),
            'organisations' => Yii::t('app', 'organisations'),
            'title' => Yii::t('app', 'title'),
            'short_title' => Yii::t('app', 'short title'),
            'is_deleted' => Yii::t('app', 'is_deleted')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->organisation_type_id;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return $this->short_title;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return string[]
     */
    public function getPermissionTitles()
    {
        $permissions = [];
        foreach ($this->getPermissions()->all() as $permission) {
            $permissions[] = $permission->getItemName();
        }
        return $permissions;
    }

    /**
     * @return ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(OrganisationTypeAuthAssignment::className(), ['organisation_type_id' => 'organisation_type_id']);
    }

    public function getOrganisations()
    {
        return $this->hasMany(Organisation::className(), ['organisation_type_id' => 'organisation_type_id']);
    }
}