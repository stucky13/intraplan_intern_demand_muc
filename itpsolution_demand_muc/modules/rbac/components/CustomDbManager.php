<?php

namespace app\modules\rbac\components;


use app\components\SessionUtil;
use app\modules\rbac\models\CustomAssignment;
use yii\db\Query;
use yii\rbac\DbManager;

class CustomDbManager extends DbManager
{
    private $_checkAccessAssignments = [];

    /**
     * {@inheritdoc}
     */
    public function getAssignments($userId)
    {
        if ($this->isEmptyUserId($userId)) {
            return [];
        }

        $query = (new Query())
            ->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new CustomAssignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
                'data' => $row['data']
            ]);
        }

        return $assignments;
    }

    /**
     * Check whether $userId is empty.
     * @param mixed $userId
     * @return bool
     */
    public function isEmptyUserId($userId)
    {
        return !isset($userId) || $userId === '';
    }

    /**
     * {@inheritdoc}
     */
    public function assign($role, $userId, $data = null)
    {
        $assignment = new CustomAssignment([
            'userId' => $userId,
            'roleName' => $role,
            'createdAt' => time(),
            'data' => $data
        ]);

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'user_id' => $assignment->userId,
                'item_name' => $assignment->roleName,
                'created_at' => $assignment->createdAt,
                'data' => $data
            ])->execute();

        unset($this->_checkAccessAssignments[(string)$userId]);
        return $assignment;
    }

    /**
     * {@inheritdoc}
     */
    public function revoke($role, $userId, $data = null)
    {
        if ($this->isEmptyUserId($userId)) {
            return false;
        }

        unset($this->_checkAccessAssignments[(string)$userId]);
        return $this->db->createCommand()
                ->delete($this->assignmentTable, ['user_id' => (string)$userId, 'item_name' => $role->name, 'data' => $data])
                ->execute() > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function checkAccess($userId, $permissionName, $params = [])
    {
        if (isset($this->_checkAccessAssignments[(string)$userId])) {
            $assignments = $this->_checkAccessAssignments[(string)$userId];
        } else {
            if ($this->getAssignment($permissionName, $userId)) {
                return true;
            }
            $assignments = $this->getOrganisationAssignments($userId, SessionUtil::getSessionOrganisationId());
            $this->_checkAccessAssignments[(string)$userId] = $assignments;
        }

        if ($this->hasNoAssignments($assignments)) {
            return false;
        }

        $this->loadFromCache();
        if ($this->items !== null) {
            return $this->checkAccessFromCache($userId, $permissionName, $params, $assignments);
        }

        return $this->checkAccessRecursive($userId, $permissionName, $params, $assignments);
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignment($roleName, $userId, $data = null)
    {
        if ($this->isEmptyUserId($userId)) {
            return null;
        }

        //TODO: AdminCheck better
        $row = (new Query())->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId, 'item_name' => 'admin'])
            ->one($this->db);
        if ($row) {
            return true;
        }

        $row = (new Query())->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId, 'item_name' => $roleName, 'data' => $data])
            ->one($this->db);

        if ($row === false) {
            return null;
        }

        return new CustomAssignment([
            'userId' => $row['user_id'],
            'roleName' => $row['item_name'],
            'createdAt' => $row['created_at'],
            'data' => $row['data']

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrganisationAssignments($userId, $data)
    {
        if ($this->isEmptyUserId($userId)) {
            return [];
        }

        $query = (new Query())
            ->from($this->assignmentTable)
            ->where(['user_id' => (string)$userId, 'data' => $data]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['item_name']] = new CustomAssignment([
                'userId' => $row['user_id'],
                'roleName' => $row['item_name'],
                'createdAt' => $row['created_at'],
                'data' => $row['data']
            ]);
        }

        return $assignments;
    }
}