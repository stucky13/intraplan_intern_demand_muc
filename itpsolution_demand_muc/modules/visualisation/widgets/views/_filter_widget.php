<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 21.03.2019
 * Time: 10:25
 */

use app\modules\visualisation\assets\VisualisationAsset;
use app\modules\visualisation\Module;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * @var array $georef
 * @var array $filters
 * @var array $value
 * @var array $operations
 * @var array $sessionParameters
 * @var array $filterRows
 */

VisualisationAsset::register($this);
?>

<div class="col-md-12">
    <div class="col-md-6">
        <div class="col-md-12">
            <label class="control-label">Startfrage</label>
            <?= Select2::widget([
                'name' => 'startQuestion',
                'data' => $georef,
                'value' => !empty($sessionParameters) ? $sessionParameters['startQuestion'] : null,
                'options' => [
                    'placeholder' => Module::t('visualisation', 'choose start question'),
                    'class' => 'form-control form-control-color',
                    'id' => 'startQuestionSelect'
                ]]) ?>
        </div>
        <div class="col-md-12">
            <label class="control-label">Startstationen</label>
            <?= Select2::widget([
                'name' => 'startKnot',
                'value' => !empty($sessionParameters) ? $sessionParameters['startStations'] : null,
                'initValueText' => !empty($sessionParameters) ? $sessionParameters['startStationsText'] : null,
                'options' => [
                    'placeholder' => Module::t('visualisation', 'choose start knot/s'),
                    'class' => 'form-control form-control-color',
                    'id' => 'startKnotSelect'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                    'closeOnSelect' => false,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/visualisation/relation/get-knots']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q: params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function (category) { return category.text; }'),
                    'templateSelection' => new JsExpression('function (category) { return category.text; }'),
                ]
            ]) ?>
        </div>
        <div class="col-md-12">
            <label class="control-label">Endfrage</label>
            <?= Select2::widget([
                'name' => 'endQuestion',
                'data' => $georef,
                'value' => !empty($sessionParameters) ? $sessionParameters['endQuestion'] : null,
                'options' => [
                    'placeholder' => Module::t('visualisation', 'choose end question'),
                    'class' => 'form-control form-control-color',
                    'id' => 'endQuestionSelect'
                ]]) ?>
        </div>
        <div class="col-md-12">
            <label class="control-label">Endstationen</label>
            <?= Select2::widget([
                'name' => 'endKnot',
                'value' => !empty($sessionParameters) ? $sessionParameters['endStations'] : null,
                'initValueText' => !empty($sessionParameters) ? $sessionParameters['endStationsText'] : null,
                'options' => [
                    'placeholder' => Module::t('visualisation', 'choose end knot/s'),
                    'class' => 'form-control form-control-color',
                    'id' => 'endKnotSelect'
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                    'closeOnSelect' => false,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to(['/visualisation/relation/get-knots']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q: params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function (category) { return category.text; }'),
                    'templateSelection' => new JsExpression('function (category) { return category.text; }'),
                ]
            ]) ?>
        </div>
        <div class="col-md-12">
            <div class="col-md-4" style="padding-left: 0">
                <label class="control-label">Berechnungsart</label>
                <select class="form-control" id="operation">
                    <?php foreach ($operations as $key => $operation): ?>
                        <option value="<?= $key ?>" <?= !empty($sessionParameters) && $key === $sessionParameters['operation'] ? 'selected' : null ?>><?= $operation ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-8" style="padding-right: 0">
                <label class="control-label">Berechnungsfrage</label>
                <?= Select2::widget([
                    'name' => 'value',
                    'data' => $value,
                    'value' => !empty($sessionParameters) ? $sessionParameters['valueQuestion'] : null,
                    'options' => [
                        'placeholder' => Module::t('visualisation', 'choose value question'),
                        'class' => 'form-control form-control-color',
                        'id' => 'value-select'
                    ]]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12">
            <label class="control-label">Filterfragen</label>
            <?= Select2::widget([
                'name' => 'filter',
                'data' => $filters,
                'options' => [
                    'placeholder' => Module::t('visualisation', 'choose filter criteria'),
                    'class' => 'form-control form-control-color',
                    'id' => 'filter-select'
                ]]) ?>
        </div>
        <div class="col-md-12">
            <label class="control-label">gewählte Filter</label>
            <table id="filter-table" class="table table-bordered  margin-b-none panel-color ">
                <thead>
                <tr>
                    <th><?= Module::t('visualisation', 'question id') ?></th>
                    <th><?= Module::t('visualisation', 'question formulation') ?></th>
                    <th><?= Module::t('visualisation', 'input field') ?></th>
                    <th><?= Module::t('visualisation', 'options') ?></th>
                </tr>
                </thead>
                <tbody id="filter-body">
                <?php
                foreach ($filterRows as $filterRow) {
                    echo $filterRow;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-12">
    <?= Html::submitButton(Module::t('visualisation', 'calculate now'), ['id' => 'submit', 'class' => 'btn mediumButton pull-right']) ?>
</div>
