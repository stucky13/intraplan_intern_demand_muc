<?php

/**
 * Created by PhpStorm.
 * User: Michi
 * Date: 06.03.2018
 * Time: 14:19
 */

namespace app\modules\visualisation\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\modules\visualisation\models\Haltestelle;
use app\modules\visualisation\models\Knoten;
use app\modules\visualisation\models\Tarifgebiet;
use app\modules\visualisation\models\Tarifpunkt;
use Throwable;
use Yii;


/**
 * Class MapController
 *
 * @package app\modules\visualisation\components
 */
abstract class MapController extends BaseController
{
    /**
     * Generates AJAX response for variouse spot data.
     *
     * {lat: , lon: , nr: , name_30: , fillColor: , position: , parts: , knots: }
     *
     * .position: {lat, lon}
     *
     * .parts: [ {fillColor: , weight: , color: , fillOpacity: , points: }, ... ]
     *
     * .parts.points: [ {lat, lon}, ...]
     *
     * .knots: [ {lat: , lon: , nr: , name: , routes: }, ... ]
     *
     * .knotes.routes: [ {fillColor: , weight: , color: , fillOpacity: , points: }, ... ]
     *
     * .knotes.routes.points: [ {lat, lon}, ...]
     *
     * @param $id
     * @return string
     */
    public function actionGetSpotData($id)
    {
        /** @var Tarifpunkt $spot */
        $spot = Tarifpunkt::find()->where(['NR' => $id])->one();
        $knotData = [];
        /** @var Knoten $knot */
        foreach ($spot->getKnoten()->all() as $knot) {
            $knotData[] = [
                'lat' => $knot->getLat(),
                'lon' => $knot->getLon(),
                'nr' => $knot->getNr(),
                'name' => $knot->getName(),
                'routes' => $knot->getStreckenPunkteAsArray()
            ];
        }
        $data = [
            'lat' => $spot->getLat(),
            'lon' => $spot->getLon(),
            'nr' => $spot->getNr(),
            'name_30' => $spot->getName30(),
            'position' => [$spot->getLat(), $spot->getLon()],
            'parts' => $spot->getTarifpunktgrenzePunkteAsArray(),
            'fillColor' => $spot->getFuellfarbe(),
            'knots' => $knotData
        ];
        return json_encode($data);
    }

    /**
     * Generates AJAX response for variouse area data.
     *
     * area: { lat: ,lon: ,fillColor: ,parts: }
     *
     * spots: [{ lat: ,lon: , nr: , name_30:, position: , parts: ,fillColor: , knots: }, ...]
     *
     * area.parts: [ {fillColor: , weight: , color: , fillOpacity: ,points: }, ... ]
     *
     * area.parts.points: [ {lat, lon}, ...]
     *
     * spot.knots: [ {lat: , lon: , nr: , name: , routes: }, ... ]
     *
     * spot.knots.routes: [ {fillColor: , weight: , color: , fillOpacity: , points: }, ... ]
     *
     * spot.knots.routes.points: [ {lat, lon}, ...]
     *
     * @param int $id TarifpunktNr
     * @return string Json encoded data
     */
    public function actionGetAreaData($id)
    {
        $data = [];
        /** @var Tarifgebiet $area */
        $area = Tarifgebiet::find()->where(['NR' => $id])->one();

        $data['area'] = [
            'lat' => $area->getLat(),
            'lon' => $area->getLon(),
            'parts' => $area->getTarifgebietsgrenzePunkteAsArray(),
            'fillColor' => $area->getFuellfarbe(),
        ];

        $spotData = [];
        /** @var Tarifpunkt $spot */
        foreach ($area->getTarifpunkte()->all() as $spot) {
            $knotData = [];
            /** @var Knoten $knot */
            foreach ($spot->getKnoten()->all() as $knot) {
                $knotData[] = [
                    'lat' => $knot->getLat(),
                    'lon' => $knot->getLon(),
                    'nr' => $knot->getNr(),
                    'name' => $knot->getName(),
                    'routes' => $knot->getStreckenPunkteAsArray()
                ];
            }
            $spotData[] = [
                'lat' => $spot->getLat(),
                'lon' => $spot->getLon(),
                'nr' => $spot->getNr(),
                'name_30' => $spot->getName30(),
                'position' => [$spot->getLat(), $spot->getLon()],
                'parts' => $spot->getTarifpunktgrenzePunkteAsArray(),
                'fillColor' => $spot->getFuellfarbe(),
                'knots' => $knotData
            ];
        }
        $data['spots'] = $spotData;

        return json_encode($data);
    }

    /**
     * Generates AJAX response for area routes.
     * { knot [ {lat,lon}, ...], ... }
     *
     * @param $id
     * @return string
     */
    public function actionGetAreaRoutes($id)
    {
        $data = [];
        /** @var Tarifgebiet $area */
        $area = Tarifgebiet::find()->where(['NR' => $id])->one();
        foreach ($area->getTarifpunkte()->all() as $spot) {
            /** @var Knoten $knot */
            foreach ($spot->getKnoten()->all() as $knot) {
                $knotData[] = $knot->getStreckenPunkteAsArray();
            }
        }
        return json_encode($knotData);
    }


    /**
     * Generates AJAX response for spot routes.
     *
     * knots [ [lat,lon], ... ]
     *
     * @param $id
     * @return string
     */
    public function actionGetSpotRoutes($id)
    {
        $data = [];
        /** @var Tarifpunkt $spot */
        $spot = Tarifpunkt::find()->where(['NR' => $id])->one();
        /** @var Knoten $knot */
        foreach ($spot->getKnoten()->all() as $knot) {
            $data[] = $knot->getStreckenPunkteAsArray();
        }
        return json_encode($data);
    }

    public function actionGetRelationData($startId, $destinationId)
    {

        $start = Tarifpunkt::find()->where(['nr' => $startId])->one();
        $destination = Tarifpunkt::find()->where(['nr' => $destinationId])->one();
        $data[] = [
            'lat' => $start->getLat(),
            'lon' => $start->getLon(),
            'nr' => $startId,
            'name_30' => $start->getName30()
        ];

        $data[] = [
            'lat' => $destination->getLat(),
            'lon' => $destination->getLon(),
            'nr' => $destinationId,
            'name_30' => $destination->getName30()
        ];

        return json_encode($data);
    }

    /**
     * Generates AJAX response for Haltestelle.
     *
     * [ { NR: , LAT: , LON: , NAME: , REF: true }, ... ]
     * //REF exists only if stop is a reference stop!
     *
     * @param integer[] $ids
     * @return string - json encoded result
     * @throws Throwable
     */
    public function actionGetStops($ids)
    {
        $ids = json_decode($ids, true);

        $stops = Haltestelle::find()
            ->select(['NR', 'LAT', 'LON', 'NAME'])
            ->where(['NR_TARIFPUNKT' => $ids])
            ->asArray()
            ->all();

        // just necessary for refstop changing, then $ids has just one value
        foreach ($stops as $index => $stop) {
            $stops[$index]['MAYCHANGE'] = Yii::$app->getUser()->can(Constants::ADMIN) ||
                Yii::$app->getUser()->can(Constants::EDIT_REF_TYPE) ||
                Tarifpunkt::find()->where(['NR' => $ids])->one()->canUserChangeRefStop();
        }

        $spots = Tarifpunkt::findAll($ids);
        /** @var Tarifpunkt $spot */
        foreach ($spots as $spot) {
            $refStop = $spot->getRefHaltestelle()->one();
            if ($refStop) {
                for ($i = 0; $i < sizeof($stops); $i++) {
                    $stop = $stops[$i];
                    if ($stop['NR'] == $refStop->getNr()) {
                        $stop['REF'] = true;
                        $stops[$i] = $stop;
                        break;
                    }
                }
            }
        }

        return json_encode($stops);
    }
}