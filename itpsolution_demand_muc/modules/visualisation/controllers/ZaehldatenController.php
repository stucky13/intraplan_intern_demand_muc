<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 28.01.2019
 * Time: 12:56
 */

namespace app\modules\visualisation\controllers;

use app\components\BaseController;
use app\components\ExcelUtil;
use app\components\SessionUtil;
use app\modules\data\models\Erhebungsdaten;
use app\modules\data\models\Zaehldaten;
use Throwable;

class ZaehldatenController extends BaseController
{
    /**
     * @param $orgId
     * @param $download
     * @return string
     * @throws Throwable
     */


    public function actionIndex($download = false)
    {
        $orgId = SessionUtil::getSessionOrganisationId();

        $erhebungsdatenYears = Erhebungsdaten::find()->select('year')
            ->where(['organisation_id' => $orgId])
            ->orderBy('year', SORT_ASC)->all();

        $erhebungsdatenEinsteiger = Zaehldaten::getSumOfPassengers('ein', $orgId);
        $erhebungsdatenAussteiger = Zaehldaten::getSumOfPassengers('aus', $orgId);
        $erhebungsdatenPkmYear = Zaehldaten::getAvgOfPkm('pkm_jahr', $orgId);
        $erhebungsdatenRkmYear = Zaehldaten::getAvgOfPkm('rkm_jahr', $orgId);

        $years = [];
        $passengerIn = [];
        $passengerOut = [];
        $pkmYear = [];
        $rkmYear = [];

        foreach ($erhebungsdatenYears as $year) {
            $years[] = $year->getYear();
        }

        foreach ($erhebungsdatenEinsteiger as $einsteiger) {
            $passengerIn[] = $einsteiger->getEin();
        }

        foreach ($erhebungsdatenAussteiger as $aussteiger) {
            $passengerOut[] = $aussteiger->getAus();
        }

        foreach ($erhebungsdatenPkmYear as $pkm) {
            $pkmYear[] = $pkm->getPkmJahr();
        }

        foreach ($erhebungsdatenRkmYear as $rkm) {
            $rkmYear[] = $rkm->getRkmJahr();
        }

        $model = new ExcelUtil();

        if ($download) {

            $model->tablehead = ['Einsteiger', 'Aussteiger', 'PkmJahr', 'RkmJahr'];
            $model->title = [['Jahr', 'Einsteiger'], ['Jahr', 'Aussteiger'], ['Jahr', 'Pkm'], ['Jahr', 'Rkm']];
            for ($i = 0; $i < sizeof($years); $i++) {
                $yearEinsteiger[$i] = [$years[$i], $passengerIn[$i]];
                $yearAussteiger[$i] = [$years[$i], $passengerOut[$i]];
                $yearPkm[$i] = [$years[$i], $pkmYear[$i]];
                $yearRkm[$i] = [$years[$i], $rkmYear[$i]];
            }

            $model->tablecontent = [$yearEinsteiger, $yearAussteiger, $yearPkm, $yearRkm];

            $model->exportExcel('zaehldatenExcel');

        }

        return $this->render('index', [
            'years' => $years,
            'ein' => $passengerIn,
            'aus' => $passengerOut,
            'pkmYear' => $pkmYear,
            'rkmYear' => $rkmYear
        ]);
    }
}