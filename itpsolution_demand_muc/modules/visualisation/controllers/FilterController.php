<?php

/**
 * Created by PhpStorm.
 * User: Michi
 * Date: 06.03.2018
 * Time: 14:19
 */

namespace app\modules\visualisation\controllers;

use app\components\SessionUtil;
use app\modules\data\models\Antworten;
use app\modules\data\models\Fragen;
use app\modules\data\models\FragenEnum;
use app\modules\data\models\Knoten;
use app\modules\data\models\ValueType;
use DateTime;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;


/**
 * Class FilterController
 *
 * @package app\modules\visualisation\components
 */
class FilterController extends MapController
{

    protected function buildFilterValues()
    {
        $questionQuery = Fragen::find()->select(['frage_id', 'CONCAT(frage_id, \' - \', fragestellung) as fragestellung']);
        $georef = ArrayHelper::map($questionQuery->where(['is_georef' => true])->all(), 'frage_id', 'fragestellung');
        $filters = ArrayHelper::map($questionQuery->where(['is_filter' => true])->all(), 'frage_id', 'fragestellung');
        $value = ArrayHelper::map($questionQuery->where(['is_value' => true])->all(), 'frage_id', 'fragestellung');
        $operations = ['sum' => 'Summe', 'average' => 'Durchschnitt', 'min' => 'Min', 'max' => 'Max'];
        $sessionParameters = SessionUtil::getSessionMapVisualisation();

        // get tablerows to display the filters in the session
        $counter = 0;
        $filterRows = [];
        if (!empty($sessionParameters['filters'])) {
            foreach ($sessionParameters['filters'] as $key => $filter) {
                $counter++;
                $filterRows[] = $this->actionGetFilterValues($key, $counter, $filter);
            }
        }

        return [
            'streckeId' => 1,
            'georef' => $georef,
            'filters' => $filters,
            'value' => $value,
            'operations' => $operations,
            'sessionParameters' => $sessionParameters,
            'filterRows' => $filterRows,
            'counter' => $counter
        ];
    }

    /**
     * Gets the table rows for the filter table display.
     *
     * @param $id int the question id
     * @param $index int the counter
     * @param null $value the session value of the filter
     * @return string the html table row to display
     * @throws Exception Exception if no valid answer could be found
     */
    public function actionGetFilterValues($id, $index, $value = null)
    {
        /** @var Fragen $question */
        $question = Fragen::findOne($id);
        if (!$question) {
            throw new Exception('wrong id');
        }
        $questionFormulation = $question->getFragestellung();
        //preparing filter values for session stuff
        $valueString = $value ? "value='$value'" : '';

        // build string for filter in view
        $response = "<tr><td id='key'>$id</td><td id='question-formulation'>$questionFormulation</td>";
        switch ($question->getAntworttyp()) {
            case 'integer':
                $response .= "<td><input class='form-control' type='number' " . $valueString . "></td>";
                break;
            case 'boolean':
                $name = 'bool-' . $index;
                $response .= "<td>
                                 <input name=\"$name\" type='radio' value='true' " . ($value ? 'checked' : '') . "><label>Ja</label>
                                 <input name=\"$name\" type='radio' value='false' " . (!$value ? 'checked' : '') . "><label>Nein</label>
                              </td>";
                break;
            case 'decimal':
                $response .= "<td><input class='form-control' type='number' " . $valueString . "></td>";
                break;
            case 'date':
                $response .= "<td><input class='form-control' type='date' " . $valueString . "></td>";
                break;
            case 'string':
                $response .= "<td><input class='form-control' type='text' " . $valueString . "></td>";
                break;
            case 'reference':
                $references = $question->getReferenceArray();

                $dropdown = "<select class='form-control'>";
                foreach ($references as $key => $reference) {
                    $id = $reference['id'];
                    $name = $reference['name'];
                    $dropdown .= "<option value=\"$id\" " . ($value === $id ? 'selected' : '') . ">$name</option>";
                }
                $dropdown .= "</select>";

                $response .= "<td>$dropdown</td>";
                break;
            case 'user_defined':
                /** @var FragenEnum[] $questionEnums */
                $questionEnums = $question->getFragenEnums()->all();
                $dropdown = "<select class='form-control'>";
                foreach ($questionEnums as $key => $questionEnum) {
                    $enumValue = $questionEnum->getValue();
                    $dropdown .= "<option value=\"$enumValue\" " . ($value === $enumValue ? 'selected' : '') . ">$enumValue</option>";
                }
                $dropdown .= "</select>";

                $response .= "<td>$dropdown</td>";
                break;
        }
        $response .= "<td><span class='glyphicon glyphicon-remove' style='cursor: pointer' id=\"delete-$index\"></a></td></tr>";
        return $response;
    }

    /**
     * Calculates the values for the map visualisation display.
     *
     * @param $filters
     * @param $startId
     * @param $startStations
     * @param $endId
     * @param $endStations
     * @param $operation
     * @param $valueId
     * @return string
     */
    public function actionCalculateRelation($filters, $startId, $startStations, $endId, $endStations, $operation, $valueId)
    {
        $startStations = json_decode($startStations);
        $endStations = json_decode($endStations);
        $startResults = Antworten::find()->select('interview_id, fragebogen_id, value')->where(['frage_id' => $startId])->andWhere(['value' => $startStations])->asArray()->all();
        $endResults = Antworten::find()->select('interview_id, fragebogen_id, value')->where(['frage_id' => $endId])->andWhere(['value' => $endStations])->asArray()->all();
        $filters = json_decode($filters);

        // unset key value pairs with null as value
        foreach ($filters as $key => $value) {
            if (is_null($value) || $value == '')
                unset($filters[$key]);
        }

        if (!empty($filters)) {
            $filterResults = $this->getFilterResults($filters);
        }

        // change for stations ajax select2
        $startStationsText = [];
        /** @var \app\modules\data\models\Knoten $start */
        foreach (Knoten::find()->where(['nr' => $startStations])->all() as $start) {
            $startStationsText[] = $start->getNr() . ' - ' . $start->getName();
        }
        $endStationsText = [];
        /** @var Knoten $end */
        foreach (Knoten::find()->where(['nr' => $endStations])->all() as $end) {
            $endStationsText[] = $end->getNr() . ' - ' . $end->getName();
        }

        $sessionParameters = [
            'startQuestion' => $startId,
            'endQuestion' => $endId,
            'startStations' => $startStations,
            'startStationsText' => $startStationsText,
            'endStations' => $endStations,
            'endStationsText' => $endStationsText,
            'filters' => $filters,
            'operation' => $operation,
            'valueQuestion' => $valueId
        ];
        SessionUtil::setSessionMapVisualisation($sessionParameters);

        /** @var Fragen $valueQuestion */
        $valueQuestion = Fragen::find()->where(['frage_id' => $valueId])->one();
        /** @var ValueType $valueType */
        $valueType = $valueQuestion->getValueType()->one();

        $results = [];
        foreach ($startResults as $startResult) {
            foreach ($endResults as $endResult) {
                if (!empty($filterResults)) {
                    foreach ($filterResults as $filterResult) {
                        if ($startResult['interview_id'] === $filterResult['interview_id'] && $startResult['interview_id'] === $endResult['interview_id'] && $startResult['fragebogen_id'] === $endResult['fragebogen_id'] && $startResult['fragebogen_id'] === $filterResult['fragebogen_id']) {
                            $results[] = $this->getResults($startResult['interview_id'], $startResult['fragebogen_id'], $valueId, $startResult['value'], $endResult['value']);
                        }
                    }
                } else {
                    if ($startResult['interview_id'] === $endResult['interview_id'] && $startResult['fragebogen_id'] === $endResult['fragebogen_id']) {
                        $results[] = $this->getResults($startResult['interview_id'], $startResult['fragebogen_id'], $valueId, $startResult['value'], $endResult['value']);
                    }
                }
            }
        }

        $relations = [];
        foreach ($startStations as $startStation) {
            foreach ($endStations as $endStation) {
                $value = 0;
                $counter = 0;
                foreach ($results as $result) {
                    if ($result['startStation'] === $startStation && $result['endStation'] === $endStation) {
                        $correctValue = str_replace(',', '.', $result['value']);
                        switch ($operation) {
                            case 'min':
                                $value = $value > $correctValue || $value === 0 ? $correctValue : $value;
                                break;
                            case 'max':
                                $value = $value < $correctValue || $value === 0 ? $correctValue : $value;
                                break;
                            default:
                                $value += $correctValue;
                        }
                        $counter++;
                    }
                }
                if ($operation === 'average' && $counter > 0) {
                    $value = $value / $counter;
                }

                $relationData = [];
                $relationData['startStation'] = Knoten::find()->where(['nr' => $startStation])->asArray()->one();
                $relationData['endStation'] = Knoten::find()->where(['nr' => $endStation])->asArray()->one();
                $relationData['value'] = $value;
                $relationData['valueShortName'] = $valueType->getShortname();

                $relations[] = $relationData;
            }
        }

        /* Sort the array descending */
        array_multisort(array_column($relations, 'value'), SORT_DESC, $relations);

        return json_encode($relations);
    }



    /**
     * Fills one result array for the visualisation.
     *
     * @param $interviewId
     * @param $fragebogenId
     * @param $valueId
     * @param $startResult
     * @param $endResult
     * @return array
     */
    private function getResults($interviewId, $fragebogenId, $valueId, $startResult, $endResult)
    {
        /** @var Antworten $answer */
        $answer = Antworten::find()->where(['and', ['frage_id' => $valueId], ['interview_id' => $interviewId], ['fragebogen_id' => $fragebogenId]])->one();

        $data = [];
        $data['interview_id'] = $interviewId;
        $data['fragebogen_id'] = $fragebogenId;
        $data['startStation'] = $startResult;
        $data['endStation'] = $endResult;
        $data['value'] = $answer->getValue();

        return $data;
    }

    /**
     * Returns all valid InterviewIds for the filters
     *
     * @param $filters
     * @return array|ActiveRecord[]
     */
    private function getFilterResults($filters)
    {
        $antwortenQuery = Antworten::find()->select('interview_id, fragebogen_id');
        foreach ($filters as $key => $filter) {
            if (DateTime::createFromFormat('Y-m-d', $filter) !== FALSE) {
                $filter = date('d.m.Y', strtotime($filter));
            }

            $antwortenQuery->orWhere(['and', ['frage_id' => $key], ['value' => $filter]]);
        }

        return $antwortenQuery->groupBy(['interview_id', 'fragebogen_id'])->asArray()->all();
    }
}