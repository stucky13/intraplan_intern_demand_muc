<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 28.01.2019
 * Time: 09:20
 */


use app\modules\visualisation\assets\VisualisationAsset;
use app\modules\visualisation\Module;
use dosamigos\chartjs\ChartJs;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/**
 * @var int[] $years
 * @var int[] $ein
 * @var int[] $aus
 * @var double[] pkmYear
 * @var double[] rkmYear
 */
VisualisationAsset::register($this);
$this->title = Module::t('visualisation', 'count');
?>

<?php $form = ActiveForm::begin([
    'method' => 'get',
]) ?>

<div class="container" style="background-color: white">
    <ul class="nav nav-tabs">
        <li id="Einsteiger" class="active">
            <a onclick="openDiagram(event, 'diagrammEinsteiger')" style="color: black">Einsteiger</a>
        </li>
        <li id="Aussteiger">
            <a onclick="openDiagram(event, 'diagrammAussteiger')" style="color: black">Aussteiger</a>
        </li>
        <li id="pkmYear">
            <a onclick="openDiagram(event, 'diagrammPkm')" style="color: black">Pkm/Jahr</a>
        </li>
        <li id="rkmYear">
            <a onclick="openDiagram(event, 'diagrammRkm')" style="color: black">Rkm/Jahr</a>
        </li>
    </ul>


    <div id="diagrammEinsteiger" class="diagramm" style="display: block">
        <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'responsive' => true,
                'maintainAspectRatio' => false,
            ],
            'clientOptions' => [
                'animation' => 0,
                'legend' => [
                    'position' => "bottom",
                ],
            ],

            'data' => [
                'labels' => $years,
                'datasets' => [
                    [
                        'label' => "Einsteiger",
                        'lineTension' => 0,
                        'fontColor' => "0000FF",
                        'backgroundColor' => "rgba(179,181,198,0)",
                        'borderColor' => "#0000FF",
                        'pointBackgroundColor' => "#0000FF",
                        'pointBorderColor' => "#0000FF",
                        'pointHoverBackgroundColor' => "#0000FF",
                        'pointHoverBorderColor' => "#0000FF",
                        'data' => $ein
                    ]
                ]
            ]
        ]);
        ?>
    </div>

    <div id="diagrammAussteiger" class="diagramm" style="display: none">
        <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'responsive' => true,
                'maintainAspectRatio' => false,
            ],
            'clientOptions' => [
                'animation' => 0,
                'legend' => [
                    'position' => "bottom",
                ],
            ],

            'data' => [
                'labels' => $years,
                'datasets' => [
                    [
                        'label' => "Aussteiger",
                        'lineTension' => 0,
                        'fontColor' => "0000FF",
                        'backgroundColor' => "rgba(179,181,198,0)",
                        'borderColor' => "#0000FF",
                        'pointBackgroundColor' => "#0000FF",
                        'pointBorderColor' => "#0000FF",
                        'pointHoverBackgroundColor' => "#0000FF",
                        'pointHoverBorderColor' => "#0000FF",
                        'data' => $aus
                    ]
                ]
            ]
        ]);
        ?>
    </div>

    <div id="diagrammPkm" class="diagramm" style="display: none">
        <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'responsive' => true,
                'maintainAspectRatio' => false,
            ],
            'clientOptions' => [
                'animation' => 0,
                'legend' => [
                    'position' => "bottom",
                ],
            ],

            'data' => [
                'labels' => $years,
                'datasets' => [
                    [
                        'label' => "Durchschnittliche Pkm/Jahr",
                        'lineTension' => 0,
                        'fontColor' => "0000FF",
                        'backgroundColor' => "rgba(179,181,198,0)",
                        'borderColor' => "#0000FF",
                        'pointBackgroundColor' => "#0000FF",
                        'pointBorderColor' => "#0000FF",
                        'pointHoverBackgroundColor' => "#0000FF",
                        'pointHoverBorderColor' => "#0000FF",
                        'data' => $pkmYear
                    ]
                ]
            ]
        ]);
        ?>
    </div>

    <div id="diagrammRkm" class="diagramm" style="display: none">
        <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'responsive' => true,
                'maintainAspectRatio' => false,
            ],
            'clientOptions' => [
                'animation' => 0,
                'legend' => [
                    'position' => "bottom",
                ],
            ],

            'data' => [
                'labels' => $years,
                'datasets' => [
                    [
                        'label' => "Durchschnittliche Rkm/Jahr",
                        'lineTension' => 0,
                        'fontColor' => "0000FF",
                        'backgroundColor' => "rgba(179,181,198,0)",
                        'borderColor' => "#0000FF",
                        'pointBackgroundColor' => "#0000FF",
                        'pointBorderColor' => "#0000FF",
                        'pointHoverBackgroundColor' => "#0000FF",
                        'pointHoverBorderColor' => "#0000FF",
                        'data' => $rkmYear
                    ]
                ]
            ]
        ]);
        ?>
    </div>
</div>
<br>
<div class="col-md-12">
    <?= Html::a(Module::t('admin', 'back'), Yii::$app->getHomeUrl() . 'data/erhebungs-daten/view', ['class' => 'btn mediumButton pull-left']) ?>
    <?= Html::submitButton(Yii::t('app', 'download'), ['class' => 'btn mediumButton pull-right', 'name' => 'download', 'value' => 'true']) ?>
</div>

<?php ActiveForm::end(); ?>

<script>
    function openDiagram(evt, diagramName) {
        if (diagramName == "diagrammEinsteiger") {
            document.getElementById("Einsteiger").className = "active";
            document.getElementById(diagramName).style.display = "block";
            document.getElementById("Aussteiger").className = "";
            document.getElementById("diagrammAussteiger").style.display = "none";
            document.getElementById("pkmYear").className = "";
            document.getElementById("diagrammPkm").style.display = "none";
            document.getElementById("rkmYear").className = "";
            document.getElementById("diagrammRkm").style.display = "none";
        }
        if (diagramName == "diagrammAussteiger") {
            document.getElementById("Einsteiger").className = "";
            document.getElementById("diagrammEinsteiger").style.display = "none";
            document.getElementById("Aussteiger").className = "active";
            document.getElementById(diagramName).style.display = "block";
            document.getElementById("pkmYear").className = "";
            document.getElementById("diagrammPkm").style.display = "none";
            document.getElementById("rkmYear").className = "";
            document.getElementById("diagrammRkm").style.display = "none";
        }
        if (diagramName == "diagrammPkm") {
            document.getElementById("Aussteiger").className = "";
            document.getElementById("diagrammAussteiger").style.display = "none";
            document.getElementById("Einsteiger").className = "";
            document.getElementById("diagrammEinsteiger").style.display = "none";
            document.getElementById("pkmYear").className = "active";
            document.getElementById(diagramName).style.display = "block";
            document.getElementById("rkmYear").className = "";
            document.getElementById("diagrammRkm").style.display = "none";
        }
        if (diagramName == "diagrammRkm") {
            document.getElementById("Aussteiger").className = "";
            document.getElementById("diagrammAussteiger").style.display = "none";
            document.getElementById("Einsteiger").className = "";
            document.getElementById("diagrammEinsteiger").style.display = "none";
            document.getElementById("pkmYear").className = "";
            document.getElementById("diagrammPkm").style.display = "none";
            document.getElementById("rkmYear").className = "active";
            document.getElementById(diagramName).style.display = "block";
        }
    }
</script>
