<?php
/**
 * Created by PhpStorm.
 * User: Simon Hümmer
 * Date: 21.01.2019
 * Time: 11:51
 */


/**
 * @var array $data
 * @var array $stationsList
 * @var array $rowTotals
 * @var array $colTotals
 * @var array $matrixEntries
 * @var int $max
 */

use app\modules\visualisation\Module;

?>

<table class="matrix">
    <thead>
    <tr>
        <th class="center" colspan=<?= sizeof($data) + 4 ?>><?= Module::t('visualisation', 'matrix name') ?></th>
    </tr>
    </thead>
    <tr>
        <td></td>
        <td class="center" colspan=<?= sizeof($data) + 3 ?>><?= Module::t('visualisation', 'to') ?></td>
    </tr>
    <tr>
        <td rowspan= <?= sizeof($data) + 4 ?>>
            <div class="" style="display: inline-block"><?= Module::t('visualisation', 'from') ?></div>
        </td>
    </tr>
    <tr>
        <td><!-- Placeholder for vertical stop names --></td>
        <?php //Create rows
        foreach ($stationsList as $name): ?>
            <td>
                <div class="" style="display: inline-block"><?= $name ?></div>
            </td>
        <?php endforeach; ?>
        <td>
            <div class="" style="display: inline-block"><?= Module::t('visualisation', 'break out') ?></div>
        </td>
        <td>
            <div class="" style="display: inline-block"><?= Module::t('visualisation', 'total') ?></div>
        </td>
    </tr>
    <?php foreach ($stationsList as $vonNr => $vonName) : ?>
        <tr>
            <td><?= $vonName ?></td>
            <?php foreach ($stationsList as $zuNr => $zuName): ?>
                <?php if ($vonNr > $zuNr): ?>
                    <td style="background-color:rgba(100,150,100, <?= $matrixEntries[$zuNr][$vonNr] / $max ?>)"><?= $matrixEntries[$zuNr][$vonNr] ?></td>
                <?php elseif ($vonNr < $zuNr): ?>
                    <td style="background-color:rgba(100,150,100, <?= $matrixEntries[$vonNr][$zuNr] / $max ?>)"><?= $matrixEntries[$vonNr][$zuNr] ?></td>
                <?php else: ?>
                    <td></td>
                <?php endif; ?>
            <?php endforeach; ?>
            <td>TODO</td>
            <td><?= $rowTotals[$zuNr]; ?></td>
        </tr>
    <?php endforeach; ?>

    <tr>
        <td><?= Module::t('visualisation', 'break in') ?></td>
        <?php foreach ($colTotals as $total): ?>
            <td>TODO</td><?php endforeach; ?>
        <td>TODO</td>
        <td>TODO</td>
    </tr>
    <tr>
        <td><?= Module::t('visualisation', 'total') ?></td>
        <?php foreach ($colTotals as $total): ?>
            <td><?= $total ?></td><?php endforeach; ?>
        <td>TODO</td>
        <td><?= array_sum($rowTotals) ?></td>
    </tr>
</table>
