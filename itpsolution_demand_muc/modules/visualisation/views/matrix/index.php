<?php
/**
 * Created by PhpStorm.
 * User: Simon Hümmer
 * Date: 21.01.2019
 * Time: 11:51
 */


/**
 * @var int $streckeId
 * @var array $georef
 * @var array $filters
 * @var array $value
 * @var array $operations
 * @var array $sessionParameters
 * @var array $filterRows
 */

use app\modules\visualisation\assets\MatrixAsset;
use app\modules\visualisation\Module;
use app\modules\visualisation\widgets\FilterWidget;

MatrixAsset::register($this);

$this->title = "Matrix"
?>


<div class="panel panel-default panel-color">
    <div class="panel-title panel-heading panel-default panel-color">
        <a data-toggle="collapse"
           href="#collapse"><?= Module::t('visualisation', 'Filter') ?></a>
    </div>
    <div class="panel-body panel-color collapse in"
         id="collapse">
        <?= FilterWidget::widget([
            'georef' => $georef,
            'filters' => $filters,
            'value' => $value,
            'operations' => $operations,
            'sessionParameters' => $sessionParameters,
            'filterRows' => $filterRows
        ]); ?>
        <input type="hidden" id="counter" class="form-control" value="<?= $counter; ?>" style="display:none;">
    </div>
</div>

<div id="matrix_container">

</div>
