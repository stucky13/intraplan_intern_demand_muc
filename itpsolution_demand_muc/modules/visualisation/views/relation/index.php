<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 02.03.2018
 * Time: 15:25
 */

use app\modules\visualisation\assets\RelationAsset;
use app\modules\visualisation\Module;
use app\modules\visualisation\widgets\FilterWidget;
use yii\helpers\Html;

/**
 * @var int $streckeId
 * @var array $georef
 * @var array $filters
 * @var array $value
 * @var array $operations
 * @var array $sessionParameters
 * @var array $filterRows
 */

RelationAsset::register($this);

$this->title = "Relationsansicht";
?>

<div class="panel panel-default panel-color">
    <div class="panel-title panel-heading panel-default panel-color">
        <a data-toggle="collapse"
           href="#collapse"><?= Module::t('visualisation', 'Filter') ?></a>
    </div>
    <div class="panel-body panel-color collapse in"
         id="collapse">
        <?= FilterWidget::widget([
            'georef' => $georef,
            'filters' => $filters,
            'value' => $value,
            'operations' => $operations,
            'sessionParameters' => $sessionParameters,
            'filterRows' => $filterRows
        ]); ?>
        <input type="hidden" id="counter" class="form-control" value="<?= $counter; ?>" style="display:none;">
    </div>
</div>


<div class="col-md-4">
    <div id="scaling-slider">
    </div>
</div>
<?= Html::checkbox('show-thickness', [true], ['id' => 'show-thickness', 'label' => "Auslastung darstellen"]); ?>

<input type="hidden" id="strecke-id" class="form-control" value="<?= $streckeId; ?>" style="display:none;">


<div class="row">
    <div id="area-map" style="height: 400px; position: relative;"></div>
</div>