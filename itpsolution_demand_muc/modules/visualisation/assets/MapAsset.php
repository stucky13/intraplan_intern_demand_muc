<?php

namespace app\modules\visualisation\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class MapAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/visualisation/assets';

    public $js = [
        'images.js',
        'visualisation.js',
        'constants.js',
        'helper.js',
        'requests.js',
        'map.js'
    ];

    public $depends = [
        'yii\jui\JuiAsset',
        JqueryAsset::class,
        LeafLetAsset::class
    ];

}
