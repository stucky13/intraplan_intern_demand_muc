$(document).ready(function () {
    $('#submit').click(function () {
        let startStations = $('#startKnotSelect').val();
        let startQuestion = $('#startQuestionSelect').val();
        let endStations = $('#endKnotSelect').val();
        let endQuestion = $('#endQuestionSelect').val();
        let filters = [];
        let operation = $('#operation').val();
        let value = $('#value-select').val();
        $('#filter-body tr').each(function (i, row) {
                let $row = $(row), key = $row.find('#key')[0].innerHTML,
                    value = $row.find('input:checked').val() ? $row.find('input:checked').val() :
                        ($row.find('input').val() && !$row.find("input[type='radio']").val() ? $row.find('input').val() :
                            ($row.find("select").val() ? $row.find("select").val() : ''));

                if (key && value) {
                    filters[key] = value;
                }
            }
        );

        $("#collapse").collapse("hide");
        calculateMatrix(filters, startQuestion, startStations, endQuestion, endStations, operation, value);
    });

    $('.rotate').each(function () {
        this.parentNode.height = $(this).width();
    });
});

function calculateMatrix(filters, startId, startStations, endId, endStations, operation, valueId) {
    let data = calculateData(filters, startId, startStations, endId, endStations, operation, valueId);

    clearMatrixTable();
    buildMatrixTable(data);
}

function clearMatrixTable() {

}

function buildMatrixTable(data) {
    $.ajax({
        async: false,
        url: baseUrl + "visualisation/matrix/render-matrix",
        data: {
            data: data
        },
        success: function (data) {
            $("#matrix_container").html(data);
        }
    });
}