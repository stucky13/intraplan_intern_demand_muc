/**
 * With and height from the biggest icon displayed on the map.
 * NOT the biggest width and height on the file system!
 * @type {Object}
 */
const BIGGEST_ICON_SIZE = keepRatio({height: 30}, {width: 94, height: 160});

let stationIcon = L.icon(calculatingIconSettings(0.7, 30, 30));
stationIcon.options.iconUrl = "../../../web/images/station-icon.png";

let stopIcon = L.icon(calculatingIconSettings(0.5, 50, 48));
stopIcon.options.iconUrl = "../../../web/images/stop-icon.png";

let refStopIcon = L.icon(calculatingIconSettings(0.6, 50, 48));
refStopIcon.options.iconUrl = "../../../web/images/ref-stop-icon.png";

let spotIcon = L.icon(calculatingIconSettings(1, 94, 160));
spotIcon.options.iconUrl = "../../../web/images/spot-icon.png";

let spotHighlightIcon = L.icon(calculatingIconSettings(1.5, 94, 160));
spotHighlightIcon.options.iconUrl = "../../../web/images/spot-icon.png";

/**
 * @typedef {Object} Size
 * @property {number} [height]
 * @property {number} [width]
 */

/**
 * Calculates a new width and hight. If both width and hight are given on the newSize param then height will be ignored.
 * @param {Size} newSize - either width or height
 * @param {{width:number,height:number}} oldSize - both width and height
 * @returns {{width}}
 */
function keepRatio(newSize, oldSize) {
    function gcd(a, b) {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    let ratio = gcd(oldSize.width, oldSize.height);
    oldSize.width = oldSize.width / ratio;
    oldSize.height = oldSize.height / ratio;

    let fac = null;
    if (newSize.width === undefined)
        fac = newSize.height / oldSize.height;
    else
        fac = newSize.width / oldSize.width;

    newSize.width = oldSize.width * fac;
    newSize.height = oldSize.height * fac;

    return newSize;
}

/**
 * Calculating the new size for an icon with the given factor corresponding to the biggest icon to be displayed on the map.
 * @param factor
 * @param width - current icon width on file system
 * @param height - current icon height on file system
 * @returns {{iconSize: [number,number], iconAnchor: [number,number], popupAnchor: [number,number]}}
 */
function calculatingIconSettings(factor, width, height) {
    let toMaxfactor = null;
    if (BIGGEST_ICON_SIZE.width >= BIGGEST_ICON_SIZE.height)
        toMaxfactor = BIGGEST_ICON_SIZE.width / width;
    else
        toMaxfactor = BIGGEST_ICON_SIZE.height / height;
    width = width * toMaxfactor * factor;
    height = height * toMaxfactor * factor;
    return {
        iconSize: [width, height],
        iconAnchor: [width / 2, height],
        popupAnchor: [0, -height],
        defaultZoomLevel: 12
    }
}