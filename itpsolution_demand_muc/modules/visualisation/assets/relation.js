let MINIMUM_THICKNESS = 50;
let MAXIMUM_THICKNESS = 500;
let areaMap;
let strecke;
let zaehldaten;
let showThickness = true;
let scaling = 100;
let relationHolder;


$(document).ready(function () {
    $('#show-thickness').change(function () {
        areaMap.removeStrecke();
        showThickness = this.checked;
        if (showThickness && relationHolder) {
            drawRelations(relationHolder, areaMap, true, false);
        }
    });

    $("#scaling-slider").slider({
        range: "max",
        min: 50,
        max: 200,
        value: 125,
        slide: function (event, ui) {
            scaling = ui.value;
            areaMap.removeStrecke();
            if (relationHolder) {
                drawRelations(relationHolder, areaMap, true, false);
            }
        }
    });

    initializeMap();

    $('#submit').click(function () {
        let startStations = $('#startKnotSelect').val();
        let startQuestion = $('#startQuestionSelect').val();
        let endStations = $('#endKnotSelect').val();
        let endQuestion = $('#endQuestionSelect').val();
        let filters = [];
        let operation = $('#operation').val();
        let value = $('#value-select').val();
        $('#filter-body tr').each(function (i, row) {
                let $row = $(row), key = $row.find('#key')[0].innerHTML,
                    value = $row.find('input:checked').val() ? $row.find('input:checked').val() :
                        ($row.find('input').val() && !$row.find("input[type='radio']").val() ? $row.find('input').val() :
                            ($row.find("select").val() ? $row.find("select").val() : ''));

                if (key && value) {
                    filters[key] = value;
                }
            }
        );

        $("#collapse").collapse("hide");
        calculateRelation(filters, startQuestion, startStations, endQuestion, endStations, operation, value);
    });
});

function calculateRelation(filters, startId, startStations, endId, endStations, operation, valueId){
    var data = calculateData(filters, startId, startStations, endId, endStations, operation, valueId);

    areaMap.removeStrecke();
    areaMap.removeMarker();
    $('#show-thickness').prop('checked', true);
    drawRelations(JSON.parse(data), areaMap, true, true);
}

function initializeMap() {
    // coordinates for stuttgart
    let coordinates = [48.78232, 9.17702];
    areaMap = renderMap('area-map', coordinates);
}


function drawRelations(relations, areaMap, showThickness, adjustMap) {
    let vertices = [];
    let thickness;

    relationHolder = relations;

    for (let j = 0; j < relations.length; j++) {
        let startStationNr = relations[j]['startStation']['nr'];
        let startStationLaenge = relations[j]['startStation']['laenge'];
        let startStationBreite = relations[j]['startStation']['breite'];
        let startStationName = relations[j]['startStation']['name'];
        let endStationLaenge = relations[j]['endStation']['laenge'];
        let endStationBreite = relations[j]['endStation']['breite'];
        let endStationNr = relations[j]['endStation']['nr'];
        let endStationName = relations[j]['endStation']['name'];
        let value = relations[j]['value'];
        let valueShortName = relations[j]['valueShortName'];

        if (!showThickness) {
            thickness = (MAXIMUM_THICKNESS + MINIMUM_THICKNESS) * scaling / 100;
        } else {
            // array is sorted descending by value => first entry is the maximum value
            let maxValue = relations[0]['value'];
            thickness = (!((j + 1) === relations.length) && value > 0 ? (MAXIMUM_THICKNESS * value / maxValue) : MINIMUM_THICKNESS) * (scaling / 100);
        }

        areaMap.addMarker(MarkerType.STOP, startStationBreite, startStationLaenge, startStationNr, stopIcon, stopIcon, startStationName, Z_INDEX_SPOT);
        vertices.push([startStationBreite, startStationLaenge]);

        areaMap.addMarker(MarkerType.STOP, endStationBreite, endStationLaenge, endStationNr, stopIcon, stopIcon, endStationName, Z_INDEX_SPOT);
        vertices.push([endStationBreite, endStationLaenge]);

        areaMap.drawLineBetweenPoints(
            [[startStationBreite, startStationLaenge], [endStationBreite, endStationLaenge]],
            null, thickness / 50.0, null, null, startStationName + " - " + endStationName + "<br>" + Math.round(value) + " " + valueShortName
        );
    }
    if (adjustMap) {
        //resize map to first the first and last point in it.
        areaMap.fitMapToArea(vertices);
    }
}
