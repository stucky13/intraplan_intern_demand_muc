let Helper = {};

/**
 * Gets the value form a dom element
 * @memberOf {Helper}
 * @param {string} id - html dom id of an element
 * @returns {*}
 */
Helper.getValueFromId = function (id) {
    return $('#' + id).val();
};

/**
 * Collects all options values from a html select element and returns them as a simple array.
 * @memberOf {Helper}
 * @returns {number[]} the values as simple array e.g. [ number, number ]
 */
Helper.getSelectOptionsAsArray = function (selectId) {
    let options = $('#' + selectId + ' option');
    let ids = [];
    $.map(options, function (option) {
        ids.push(option.value);
    });
    return ids;
};

function decimalColorToHTMLcolor(number) {
    let intnumber = number - 0;
    let red, green, blue;
    let template = "#000000";
    red = (intnumber & 0x0000ff) << 16;
    green = intnumber & 0x00ff00;
    blue = (intnumber & 0xff0000) >>> 16;
    intnumber = red | green | blue;
    let HTMLcolor = intnumber.toString(16);
    HTMLcolor = template.substring(0, 7 - HTMLcolor.length) + HTMLcolor;
    return HTMLcolor;
}