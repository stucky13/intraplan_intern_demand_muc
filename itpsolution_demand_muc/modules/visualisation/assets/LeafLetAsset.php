<?php

namespace app\modules\visualisation\assets;

use yii\web\AssetBundle;

class LeafLetAsset extends AssetBundle
{
    public $sourcePath = '@npm/leaflet/dist';
    public $js = [
        'leaflet.js',
    ];
    public $css = [
        'leaflet.css',
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}