let poly = null;
let markerGroup = null;

function renderMap(mapId, mapLat, mapLon) {
    let map = new LeafletMap(mapId, mapLat, mapLon);
    map.setListView('spotform-spot_id');
    return map;
}

function getAndDrawStops(map, spotIds) {
    stopsRequest(spotIds, function (stops) {
        stops.forEach(function (stop) {
            let icon = stop.REF === true ? refStopIcon : stopIcon;
            let zIndex = stop.REF === true ? Z_INDEX_REF_STOP : Z_INDEX_STOP;
            currLayer = map.addMarker(MarkerType.STOP, stop.LAT, stop.LON, stop.NR, icon, icon, stop.NAME, zIndex, stop.MAYCHANGE);
        })
    });
}

