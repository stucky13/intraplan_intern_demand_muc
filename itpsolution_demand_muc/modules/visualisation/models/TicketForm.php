<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.03.2018
 * Time: 16:53
 */

namespace app\modules\visualisation\models;


use app\models\Model;
use app\modules\data\models\Fahrschein;

class TicketForm extends Model
{
    public $ticket_id;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                'ticket_id',
                'exist',
                'targetClass' => Fahrschein::className(),
                'targetAttribute' => 'id'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'ticket_id' => 'Fahrschein',

        ];
    }

    public function getTicketId()
    {
        return $this->ticket_id;
    }

}