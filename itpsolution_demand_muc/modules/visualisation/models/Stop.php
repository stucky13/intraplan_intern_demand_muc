<?php
/**
 * Created by PhpStorm.
 * User: Simon Hümmer
 * Date: 21.01.2019
 * Time: 12:03
 */

namespace app\modules\visualisation\models;

/**
 * Class Stop
 * @package app\modules\tariff\models
 *
 * @property int $nr;
 * @property string name;
 */
class Stop
{
    /**
     * @return int
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    public function getValueForOtherStop($otherStop)
    {
        return rand(0, 5);
    }

}