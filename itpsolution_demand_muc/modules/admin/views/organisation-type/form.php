<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 11:54
 */

/**
 * @var View $this
 * @var ActiveForm $form
 * @var OrganisationTypeForm $model
 * @var string[] $allPermissions
 * @var string[] $assignedPermissions
 * @var int $page
 */

use app\components\FormModel;
use app\modules\admin\models\OrganisationTypeForm;
use app\modules\admin\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$this->title = $model->getScenario() == FormModel::SCENARIO_DEFAULT ? Module::t('admin', 'create organisation type') : Module::t('admin', 'update organisation type');
?>

<?php $form = ActiveForm::begin([
    'id' => 'organisation-type-form',
    'options' => ['class' => 'form-vertical']
]); ?>
    <div class="col-md-6">
        <div class="panel panel-default panel-color">
            <div class="panel-heading panel-color">
                <h3 class="panel-title panel-color"><?= Module::t('admin', 'organisation type') ?></h3>
            </div>
            <div class="panel-body panel-color">
                <?= $form->field($model, 'organisationTypeId')->textInput(["class" => 'form-control form-control-color']) ?>

                <?= $form->field($model, 'title')->textInput(["class" => 'form-control form-control-color']) ?>

                <?= $form->field($model, 'shortTitle')->textInput(["class" => 'form-control form-control-color']) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default panel-color" id="permission">
            <div class="panel-heading panel-color">
                <h3 class="panel-title panel-color"><?= Module::t('admin', 'permissions') ?></h3>
            </div>
            <div class="panel-body panel-color">
                <?php foreach ($allPermissions
                               as $groupTitle => $arrayPermission): ?>
                    <div class="col-md-12" id="permission-container-<?= $groupTitle ?>">
                        <div class="panel panel-default panel-color">
                            <div class="panel-title panel-heading panel-default panel-color">
                                <a data-toggle="collapse"
                                   href="#collapse-<?= $groupTitle ?>"><?= Module::t('admin', $groupTitle); ?></a>
                            </div>
                            <div class="panel-body panel-color collapse" id="collapse-<?= $groupTitle ?>">
                                <?php
                                foreach ($arrayPermission as $permission) {
                                    $itemName = $permission->getPermission();
                                    echo $form->field($model, "permissions[$itemName]")->checkbox()->label($permission->getTitle());
                                }; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <?= Html::a(Module::t('admin', 'abort'), ["/admin/organisation-type/index?page=$page"], ['class' => 'btn mediumButton pull-left']) ?>

        <?= Html::submitButton(Module::t('admin', 'save'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page]) ?>
    </div>
<?php ActiveForm::end(); ?>