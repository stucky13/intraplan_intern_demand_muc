<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 12:00
 */

/**
 * @var View $this
 * @var ActiveForm $form
 * @var OrganisationType $organisationType
 */

use app\modules\admin\Module;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = $organisationType->getTitle();
?>
<div class="col-md-12">
    <div class="col-md-6">
        <?= DetailView::widget([
            'model' => $organisationType,
            'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
            'attributes' => [
                'organisation_type_id',
                'title',
                'short_title',
                'dt_created:datetime',
                'dt_updated:datetime',
                [
                    'label' => Yii::t('app', 'user created'),
                    'attribute' => 'user_created',
                    'value' => function (OrganisationType $organisationType) {
                        return User::findOne($organisationType->getUserCreated()) ? User::findOne($organisationType->getUserCreated())->getUsername() : null;
                    }
                ],
                [
                    'label' => Yii::t('app', 'user updated'),
                    'attribute' => 'user_updated',
                    'value' => function (OrganisationType $organisationType) {
                        return User::findOne($organisationType->getUserUpdated()) ? User::findOne($organisationType->getUserUpdated())->getUsername() : null;
                    }
                ],
            ],
        ]); ?>
    </div>
</div>

<?= Html::a(Module::t('admin', 'back'), Yii::$app->request->referrer, [
    'class' => 'btn mediumButton',
]); ?>
