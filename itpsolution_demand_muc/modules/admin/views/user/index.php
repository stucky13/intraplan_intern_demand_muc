<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 13:38
 */

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var String[] $allUsers
 * @var UserSearchForm $searchModel
 * @var int $page
 * @var User $users
 */

use app\components\Constants;
use app\components\SessionUtil;
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\UserSearchForm;
use app\modules\admin\Module;
use app\modules\core\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

AdminAsset::register($this);
$this->title = Module::t('admin', 'user');

$form = ActiveForm::begin([
    'id' => 'user-search',
    'options' => ['class' => 'form-vertical'],
]);
?>
<?php if (Yii::$app->getUser()->can(Constants::VIEW_USER) || Yii::$app->getUser()->can(Constants::VIEW_ALL_USERS)): ?>
    <div class="panel panel-default panel-color">
        <div class="panel-heading panel-color">
            <h4 class="panel-title panel-color">
                <?= Module::t('admin', 'search') ?>
            </h4>
        </div>
        <div class="panel-body panel-color">
            <div class="row" style="padding: 10px;">
                <div class="col-md-1">
                    <?= $form->field($searchModel, 'user_id')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($searchModel, 'username')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'first_name')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'last_name')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'email')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>
                <div class="col-md-1">
                    <?= $form->field($searchModel, 'company')->textInput(["class" => 'form-control form-control-color']); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($searchModel, 'user_created')->dropDownList($allUsers, ["class" => 'form-control form-control-color', 'prompt' => Module::t('admin', 'all')]) ?>
                </div>
            </div>
            <?= Html::submitButton(Module::t('admin', 'search'), [
                'class' => 'btn mediumButton pull-right'
            ]) ?>
        </div>
    </div>
<?php endif; ?>

<?php ActiveForm::end(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'user_id' => [
            'attribute' => 'user_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'username' => [
            'attribute' => 'username',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'first_name' => [
            'attribute' => 'first_name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'last_name' => [
            'attribute' => 'last_name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'email' => [
            'attribute' => 'email',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'company' => [
            'attribute' => 'company',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'is_active' => [
            'attribute' => 'is_active',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'format' => 'boolean'

        ],
        'is_new' => [
            'attribute' => 'is_new',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'format' => 'boolean'
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view} {update} {delete} {reactivate}',
            'buttons' => [
                'view' => function ($url) {
                    return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open', 'title' => Module::t('admin', 'open')]);
                },
                'update' => function ($url, $model) use ($page) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_USER) || $model->hasUserAsRepresentative(Yii::$app->getUser()->getId()) || Yii::$app->getUser()->getId() == $model->user_id) {
                        return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-pencil', 'title' => Module::t('admin', 'edit')]);
                    }
                    return "";
                },
                'delete' => function ($url, $model) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_USER) && ($model->is_deleted == 0)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-trash', 'title' => Module::t('admin', 'delete'), 'onclick' => 'return confirm("' . Module::t('admin', 'delete user') . '");']);
                    }
                    return "";
                },
                'reactivate' => function ($url, $model) {
                    if (Yii::$app->getUser()->can(Constants::ADMIN) && ($model->is_deleted == 1)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-check', 'title' => Module::t('admin', 'reactivate'), 'onclick' => 'return confirm("' . Module::t('admin', 'reactivate user') . '");']);
                    }
                    return "";
                }
            ]
        ]
    ],
]); ?>
<div class="clearfix">
    <?php if (Yii::$app->getUser()->can(Constants::EDIT_USER) || (SessionUtil::getSessionOrganisation() != null && SessionUtil::getSessionOrganisation()->getRepresentativeId() == Yii::$app->getUser()->getId())) {
        echo Html::a(Module::t('admin', 'create user'), ["create?page=$page"], [
            'class' => 'btn mediumButton pull-right'
        ]);
    }; ?>
    <?= Html::a(Module::t('admin', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>