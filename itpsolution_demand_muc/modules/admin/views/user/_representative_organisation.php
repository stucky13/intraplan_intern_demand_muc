<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 12:21
 */


use app\components\Constants;
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\RepUserOrganisationForm;
use app\modules\admin\Module;
use app\modules\core\models\Organisation;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\db\ActiveQuery;
use yii\jui\JuiAsset;
use yii\web\View;

/**
 * @var View $this
 * @var RepUserOrganisationForm[] $repOrganisations
 * @var string[] $assignableOrganisations
 * @var string[] $allPermissions
 * @var ActiveQuery $repAssignable
 */

AdminAsset::register($this);
JuiAsset::register($this);

?>
<div id="panel-option-values" class="panel panel-default panel-color">
    <div class="panel-heading panel-color">
        <h3 class="panel-title"><i class="fa fa-check-square-o"></i> <?= Yii::t('app', 'representative permissions') ?>
        </h3>
    </div>

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'rep_wrapper',
        'widgetBody' => '.rep-organisation-body',
        'widgetItem' => '.rep-organisation-item',
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.delete-item',
        'model' => $repOrganisations[0],
        'formId' => 'user-form',
        'formFields' => [
            'rep_id'
        ],
    ]); ?>

    <table class="table table-bordered table-striped margin-b-none panel-color">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'organisation') ?> </th>
            <th><?= Yii::t('app', 'permission') ?></th>
        </tr>
        </thead>
        <tbody class="rep-organisation-body panel-color">
        <?php foreach ($repOrganisations as $index => $repOrganisation):
            /** Organisation $org */
            $org = Organisation::find()->where(['organisation_id' => $repOrganisation->getOrganisationId()])->one();
            ?>
            <tr class="rep-organisation-item panel-color">
                <td>
                    <div style="display: none;">
                        <?= $form->field($repOrganisation, "[{$index}]user_id")->hiddenInput()->label(false); ?>
                        <?= $form->field($repOrganisation, "[{$index}]organisation_id")->hiddenInput()->label(false); ?>
                    </div>
                    <?= $org->getTitle(); ?>
                </td>
                <td class="vcenter">
                    <div class="panel-body panel-color">
                        <?php
                        $permissionGroupIds = [];
                        foreach ($org->getPermissions()->all() as $permission) {
                            $permissionGroupIds[$permission->getPermission()->one()->getPermissionGroupId()] = $permission->getPermission()->one()->getPermissionGroupId();
                        }
                        $permissions = $org->getPermissions()->all();
                        foreach ($permissionGroupIds as $permissionGroupId) {
                            if ($permissions): ?>
                                <div class="col-md-12" id="permission-container-<?= $permissionGroupId ?>">
                                    <div class="panel panel-default panel-color">
                                        <div class="panel-title panel-heading panel-default panel-color">
                                            <a data-toggle="collapse"
                                               href="#collapse<?= $permissionGroupId ?>"><?= Module::t('admin', Constants::PERMISSION_GROUP_TITLES[$permissionGroupId]); ?></a>
                                        </div>
                                        <div class="panel-body panel-color collapse"
                                             id="collapse<?= $permissionGroupId ?>">
                                            <?php foreach ($permissions as $permission) {
                                                if ($permission->getPermission()->one()->getPermissionGroupId() == $permissionGroupId) {
                                                    $itemName = $permission->getPermission()->one()->getPermission();
                                                    echo $form->field($repOrganisation, "[{$index}]permissions[$itemName]")->checkbox()->label($permission->getPermission()->one()->getTitle());
                                                }
                                            }; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif;
                        } ?>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php DynamicFormWidget::end(); ?>
</div>
