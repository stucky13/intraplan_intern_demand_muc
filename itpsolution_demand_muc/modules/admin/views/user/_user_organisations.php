<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 12:21
 */


use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\UserOrganisationForm;
use app\modules\admin\Module;
use app\modules\core\models\Permission;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\jui\JuiAsset;
use yii\web\View;

/**
 * @var View $this
 * @var UserOrganisationForm[] $userOrganisations
 * @var string[] $assignableOrganisations
 * @var boolean $editable
 * @var Permission[] $permissionArray
 */

AdminAsset::register($this);
JuiAsset::register($this);

?>
<div id="panel-option-values" class="panel panel-default panel-color">
    <div class="panel-heading panel-color">
        <h3 class="panel-title panel-color"><i
                    class="fa fa-check-square-o"></i> <?= Yii::t('app', 'user permissions') ?></h3>
    </div>

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'user_orga_wrapper',
        'widgetBody' => '.user-organisation-body',
        'widgetItem' => '.user-organisation-item',
        'min' => 1,
        'insertButton' => '.add-item',
        'deleteButton' => '.delete-item',
        'model' => $userOrganisations[0],
        'formId' => 'user-form',
        'formFields' => [
            'organisation_id'
        ],
    ]); ?>

    <table class="table table-bordered  margin-b-none panel-color tableColorwhite ">
        <thead>
        <tr>
            <th><?= Yii::t('app', 'organisation') ?></th>
            <th><?= Yii::t('app', 'permission') ?></th>
            <th><?= Yii::t('app', 'options') ?></th>
        </tr>
        </thead>
        <tbody class="user-organisation-body tableColorwhite">
        <?php
        /** @var UserOrganisationForm $organisation */
        foreach ($userOrganisations as $index => $organisation): ?>
            <?php $disableCheckbox = $organisation->areCheckboxesDisabled(); ?>
            <tr class="user-organisation-item tableColorwhite">
                <td>
                    <div style="display: none;">
                        <?= $form->field($organisation, "[{$index}]user_id")->hiddenInput()->label(false); ?>
                    </div>
                    <?= $form->field($organisation, "[{$index}]organisation_id")->dropDownList($assignableOrganisations, [
                        'class' => 'form-control user_org_dropdown form-control-color',
                        'onclick' => 'registerListeners()',
                        'prompt' => Yii::t('app', 'please choose'),
                        'disabled' => !$editable
                    ])->label(false); ?>
                </td>
                <td class="vcenter">
                    <div class="panel-body panel-color">
                        <?php
                        foreach ($permissionArray
                                 as $groupTitle => $arrayPermission) : ?>
                            <div class="col-md-12" id="permission-container-<?= $groupTitle ?>">
                                <div class="panel panel-default panel-color">
                                    <div class="panel-title panel-heading panel-default panel-color">
                                        <a data-toggle="collapse"
                                           href="#collapse-<?= $groupTitle . $index ?>"><?= Module::t('admin', $groupTitle); ?></a>
                                    </div>
                                    <div class="panel-body panel-color collapse"
                                         id="collapse-<?= $groupTitle . $index ?>">
                                        <?php foreach ($arrayPermission as $permission) {
                                            $itemName = $permission->getPermission();
                                            echo $form->field($organisation, "[{$index}]permissions[$itemName]")->checkbox()->label($permission->getTitle());
                                        }; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </td>
                <td class="text-left vcenter">
                    <?php if ($editable && !$disableCheckbox): ?>
                        <button type="button" class="delete-item btn btn-danger btn-xs"><i class="fa fa-minus"></i>
                        </button>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
        <?php if ($editable): ?>
            <tr>
                <td colspan="2"></td>
                <td class="text-right vcenter">
                    <button type="button" class="add-item btn mediumButton btn-sm"><span class="fa fa-plus"></span>
                        Neu
                    </button>
                </td>
            </tr>
        <?php endif; ?>
        </tfoot>
    </table>
    <?php DynamicFormWidget::end(); ?>
</div>
