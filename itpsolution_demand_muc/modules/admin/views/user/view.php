<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 13:40
 */

/**
 * @var View $this
 * @var ActiveForm $form
 * @var User $user
 * @var int $page
 */

use app\components\Constants;
use app\modules\admin\Module;
use app\modules\core\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;

$this->title = $user->getFirstName() . " " . $user->getLastName();
?>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="col-md-12">
                <?= DetailView::widget([
                    'model' => $user,
                    'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
                    'attributes' => [
                        'user_id',
                        'username',
                        'dt_created:datetime',
                        'dt_updated:datetime',
                        [
                            'label' => Yii::t('app', 'user created'),
                            'attribute' => 'user_created',
                            'value' => function (User $user) {
                                return User::findOne($user->getUserCreated()) ? User::findOne($user->getUserCreated())->getUsername() : null;
                            }
                        ],
                        [
                            'label' => Yii::t('app', 'user updated'),
                            'attribute' => 'user_updated',
                            'value' => function (User $user) {
                                return User::findOne($user->getUserUpdated()) ? User::findOne($user->getUserUpdated())->getUsername() : null;
                            }
                        ],
                        'first_name',
                        'last_name',

                    ],
                ]); ?>
            </div>
            <?php if(Yii::$app->getUser()->can(Constants::ADMIN)):?>
            <div><?= Html::a(Module::t("admin", "reset password"), ["/admin/user/resetPassword"], ['class' => 'btn mediumButton pull-right'])?></div>
            <?php endif; ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $user,
                'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
                'attributes' => [
                    'email',
                    'phone1',
                    'phone2',
                    'phone3',
                    'company',
                    'address1',
                    'address2',
                    'address3',
                    'address4',
                    'is_active:boolean',
                    'is_new:boolean',
                    [
                        'label' => Module::t('admin', 'organisation'),
                        'value' => function (User $user) {
                            return $user->getOrganisationsString();
                        }
                    ],
                ]
            ]); ?>
        </div>

    </div>

<?= Html::a(Module::t('admin', 'back'), ["/admin/user/index?page=$page"], ['class' => 'btn mediumButton pull-left']) ?>