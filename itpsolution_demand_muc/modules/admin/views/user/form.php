<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 13:43
 */

/**
 * @var View $this
 * @var ActiveForm $form
 * @var UserForm $model
 * @var UserOrganisationForm[] $userOrganisations
 * @var RepUserOrganisationForm[]|null $repOrganisations
 * @var string[] $assignableOrganisations
 * @var Permission[] $allPermissions
 * @var boolean $isAdmin
 * @var int $page
 * @var boolean $editable
 * @var Permission[]
 */

use app\components\Constants;
use app\components\FormModel;
use app\components\SessionUtil;
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\RepUserOrganisationForm;
use app\modules\admin\models\UserForm;
use app\modules\admin\models\UserOrganisationForm;
use app\modules\admin\Module;
use app\modules\core\models\Permission;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

AdminAsset::register($this);


$this->title = $model->getScenario() == FormModel::SCENARIO_DEFAULT ? Module::t('admin', 'create user') : Module::t('admin', 'update user');
?>

<?php $form = ActiveForm::begin([
    'id' => 'user-form',
    'options' => ['class' => 'form-vertical'],
]); ?>
    <div class="col-md-6">
        <div class="panel panel-default panel-color">
            <div class="panel-heading panel-color">
                <h3 class="panel-title panel-color"><?= Module::t('admin', 'user') ?></h3>
            </div>
            <div class="panel-body">
                <?= $form->field($model, 'username')->textInput(["class" => 'form-control form-control-color'], ['readonly' => $model->getScenario() == FormModel::SCENARIO_UPDATE]) ?>

                <?= $form->field($model, 'firstName')->textInput(["class" => 'form-control form-control-color']) ?>

                <?= $form->field($model, 'lastName')->textInput(["class" => 'form-control form-control-color']) ?>

                <?= $form->field($model, 'email')->textInput(["class" => 'form-control form-control-color'], ['readonly' => $model->getScenario() == FormModel::SCENARIO_UPDATE]) ?>

                <?php if ($model->getScenario() == FormModel::SCENARIO_UPDATE) {
                    echo Html::a(Module::t('admin', 'reset password'), ['user/reset-password', 'id' => $model->userId], ['class' => 'btn mediumButton pull-right']);
                } ?>

                <?php if (!(Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_USER))) {
                    $form->field($model, "isActive")->hiddenInput()->label(false);
                } ?>

                <?= $form->field($model, 'isActive')->checkbox([
                    'template' => "<div>{input} {label}</div>\n<div>{error} </div>",
                    'disabled' => !(Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_USER))
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default panel-color">
            <div class="panel-heading panel-color">
                <h3 class="panel-title panel-color"><?= Module::t('admin', 'address') ?></h3>
            </div>
            <div class="panel-body panel-color">
                <div class="col-md-6">
                    <?= $form->field($model, 'company')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'phone1')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'phone2')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'phone3')->textInput(["class" => 'form-control form-control-color']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'address1')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'address2')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'address3')->textInput(["class" => 'form-control form-control-color']) ?>

                    <?= $form->field($model, 'address4')->textInput(["class" => 'form-control form-control-color']) ?>
                </div>
            </div>
        </div>
    </div>

<?php if ((Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_USER)) && $repOrganisations && !$isAdmin): ?>
    <div class="col-md-12">
        <?= $this->render('_representative_organisation', [
            'form' => $form,
            'repOrganisations' => $repOrganisations,
            'assignableOrganisations' => $assignableOrganisations,
            'permissionArray' => $allPermissions
        ]) ?>
    </div>
<?php endif; ?>
<?php if ((Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_USER) || (SessionUtil::getSessionOrganisation() != null && SessionUtil::getSessionOrganisation()->getRepresentativeId() == Yii::$app->getUser()->getId())) && !$isAdmin): ?>
    <div class="col-md-12">
        <?= $this->render('_user_organisations', [
            'form' => $form,
            'userOrganisations' => $userOrganisations,
            'assignableOrganisations' => $assignableOrganisations,
            'editable' => $editable,
            'permissionArray' => $allPermissions
        ]) ?>
    </div>
<?php endif; ?>
    <div class="col-md-12">
        <div class="form-group">
            <?= Html::a(Module::t('admin', 'abort'), ["/admin/user/index?page=$page"], ['class' => 'btn mediumButton pull-left']) ?>

            <?= Html::submitButton(Module::t('admin', 'save'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>