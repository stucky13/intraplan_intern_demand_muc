<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 12.02.2018
 * Time: 11:41
 */

/**
 * @var View $this
 * @var ActiveForm $form
 * @var OrganisationType[] $organisationTypes
 * @var OrganisationGroup[] $organisationGroups
 * @var OrganisationForm $model
 * @var Permission[] $allPermissions
 * @var int $page
 * @var $allAssignable
 */

use app\components\FormModel;
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\OrganisationForm;
use app\modules\admin\Module;
use app\modules\core\models\OrganisationGroup;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\Permission;
use app\modules\core\widgets\UserSelectWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

AdminAsset::register($this);

$this->title = $model->getScenario() == FormModel::SCENARIO_DEFAULT ? Module::t('admin', 'create organisation') : Module::t('admin', 'update organisation');
?>

<?php $form = ActiveForm::begin([
    'id' => 'organisation-form',
    'options' => ['class' => 'form-vertical']
]); ?>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="panel panel-default panel-color">
                <div class="panel-heading panel-color">
                    <h3 class="panel-title panel-color"><?= Module::t('admin', 'organisation') ?></h3>
                </div>
                <div class="panel-body panel-color">

                    <div class="col-md-12">
                        <?= $form->field($model, 'title')->textInput(["class" => 'form-control form-control-color']) ?>

                        <?= $form->field($model, 'organisationId')->textInput(["class" => 'form-control form-control-color']) ?>

                        <?= $form->field($model, 'shortTitle')->textInput(["class" => 'form-control form-control-color']) ?>

                        <?= $form->field($model, 'organisationTypeId')->dropDownList($organisationTypes, ["class" => 'form-control form-control-color', 'prompt' => 'Bitte auswählen']) ?>
                    </div>
                    <div class="form control form-control-color">
                        <?php
                        $organisationId = $model->organisationId;
                        echo UserSelectWidget::widget([
                            'form' => $form,
                            'model' => $model,
                            'attribute' => 'representativeId',
                            'updateRoute' => ["/core/user/query"]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default panel-color">
                <div class="panel-heading panel-color">
                    <h3 class="panel-title panel-color"><?= Module::t('admin', 'address') ?></h3>
                </div>
                <div class="panel-body panel-color">
                    <div class="col-md-8">
                        <?= $form->field($model, 'street')->textInput(["class" => 'form-control form-control-color']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'houseNumber')->textInput(["class" => 'form-control form-control-color']) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'zip')->textInput(["class" => 'form-control form-control-color']) ?>
                    </div>
                    <div class="col-md-8">
                        <?= $form->field($model, 'city')->textInput(["class" => 'form-control form-control-color']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default panel-color" id="permission">
            <div class="panel-heading panel-color">
                <h3 class="panel-title panel-color"><?= Module::t('admin', 'permissions') ?></h3>
            </div>
            <div class="panel-body panel-color">
                <?php foreach ($allPermissions
                               as $groupTitle => $arrayPermission): ?>
                    <div class="col-md-12" id="permission-container-<?= $groupTitle ?>">
                        <div class="panel panel-default panel-color">
                            <div class="panel-title panel-heading panel-default panel-color">
                                <a data-toggle="collapse"
                                   href="#collapse-<?= $groupTitle ?>"><?= Module::t('admin', $groupTitle); ?></a>
                            </div>
                            <div class="panel-body panel-color collapse" id="collapse-<?= $groupTitle ?>">
                                <?php
                                foreach ($arrayPermission as $permission) {
                                    $itemName = $permission->getPermission();
                                    echo $form->field($model, "permissions[$itemName]")->checkbox()->label($permission->getTitle());
                                }; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <?= Html::a(Module::t('admin', 'abort'), ["/admin/organisation/index?page=$page"], ['class' => 'btn mediumButton pull-left']) ?>

        <?= Html::submitButton(Module::t('admin', 'save'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page]) ?>
    </div>
<?php ActiveForm::end(); ?>