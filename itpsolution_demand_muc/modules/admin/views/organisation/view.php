<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 12.02.2018
 * Time: 11:42
 */

use app\modules\admin\Module;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationGroup;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var int $page
 */
$this->title = $organisation->getTitle();
?>
<div class="col-md-12">
    <div class="col-md-6">
        <?= DetailView::widget([
            'model' => $organisation,
            'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
            'attributes' => [
                'organisation_id' => [
                    'attribute' => 'organisation_id',
                    'headerOptions' => ['class' => 'mediumButton gridview-header']
                ],
                [
                    'label' => Module::t('admin', 'organisation type'),
                    'attribute' => 'organisation_type_id',
                    'headerOptions' => ['class' => 'mediumButton gridview-header'],
                    'value' => function (Organisation $organisation) {
                        return OrganisationType::findOne($organisation->getOrganisationTypeId()) ? OrganisationType::findOne($organisation->getOrganisationTypeId())->getTitle() : null;
                    }
                ],
                'dt_created:datetime',
                'dt_updated:datetime',
                [
                    'label' => Yii::t('app', 'user created'),
                    'attribute' => 'user_created',
                    'value' => function (Organisation $organisation) {
                        return User::findOne($organisation->getUserCreated()) ? User::findOne($organisation->getUserCreated())->getUsername() : null;
                    }
                ],
                [
                    'label' => Yii::t('app', 'user updated'),
                    'attribute' => 'user_updated',
                    'value' => function (Organisation $organisation) {
                        return User::findOne($organisation->getUserUpdated()) ? User::findOne($organisation->getUserUpdated())->getUsername() : null;
                    }
                ],
                [
                    'label' => Module::t('admin', 'representative'),
                    'attribute' => 'representative_id',
                    'value' => function (Organisation $organisation) {
                        return User::findOne($organisation->getRepresentativeId()) ? User::findOne($organisation->getRepresentativeId())->getUsername() : null;
                    }
                ],
                'title',
                'short_title',


            ],
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= DetailView::widget([
            'model' => $organisation,
            'options' => ['class' => 'table table-bordered detail-view panel-color  bg-color'],
            'attributes' => [
                'street',
                'house_number',
                'zip',
                'city'
            ]
        ]); ?>
    </div>
</div>


<?= Html::a(Module::t('admin', 'back'), ["/admin/organisation/index?page=$page"], ['class' => 'btn mediumButton pull-left']) ?>
