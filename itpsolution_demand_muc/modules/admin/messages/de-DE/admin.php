<?php

return [
    /** organisation type */
    'organisation type' => 'Organisationstypen',
    'create organisation type' => 'Organisationstyp anlegen',
    'update organisation type' => 'Organisationstyp bearbeiten',
    'organisation type successfully created' => 'Organisationstyp konnte erfolgreich angelegt werden',
    'organisation type %s successfully updated' => 'Organisationstyp %s konnte erfolgreich aktualisiert werden',
    'organisation type %s successfully deleted' => 'Organisationstyp %s erfolgreich gelöscht',
    "organisation type %s couldn't be deleted" => 'Organisationstyp %s konnte nicht gelöscht werden',
    'organisation type %s successfully reactivated' => 'Organisationstyp %s konnte erfolgreich reaktiviert werden',
    "organisation type %s couldn't be reactivated" => 'Organisationstyp %s konnte nicht reaktiviert werden',
    'organisation type {title} currently used by {count} organisations' => 'Organisationstyp {title} wird aktuell in {count} Organisationen verwendet',
    "Invalid organisation type id " => "Ungültige Organisationstyp Id ",
    'Failed to find organisation type with id ' => 'Organisationstyp mit der Id  konnte nicht gefunden werden',

    /** organisation */
    'organisation' => 'Organisation',
    'create organisation' => 'Organisation anlegen',
    'update organisation' => 'Organisation bearbeiten',
    'organisation successfully created' => 'Organisation konnte erfolgreich angelegt werden',
    'organisation %s successfully updated' => 'Organisation %s konnte erfolgreich aktualisiert werden',
    'organisation %s successfully deleted' => 'Organisation %s erfolgreich gelöscht',
    "organisation %s couldn't be deleted" => 'Organisation %s konnte nicht gelöscht werden',
    'organisation %s successfully reactivated' => 'Organisation %s konnte erfolgreich reaktiviert werden',
    "organisation %s couldn't be reactivated" => 'Organisation %s konnte nicht reaktiviert werden',
    "Invalid organisation id " => "Ungültige Organisations Id ",
    'Failed to find organisation with id ' => 'Organisation mit der Id  konnte nicht gefunden werden',
    'representative' => 'Bevollmächtigter',
    'tms user' => 'TMS-Nachrichtenempfänger',
    'eav user' => 'EAV-Nachrichtenempfänger',
    'committees user' => 'Gremien-Nachrichtenempfänger',
    'tms' => 'TMS',
    'eav' => 'EAV',
    'committees' => 'Gremium',
    'search' => 'Suche',
    'addresses' => 'Adressen',
    'address' => 'Adresse',
    'message receiver' => 'Nachrichtenempfänger',


    /** organisation group */
    'organisation group' => 'Organisationsgruppe',
    'create organisation group' => 'Organisationgruppe anlegen',
    'update organisation group' => 'Organisationgruppe bearbeiten',
    'organisation group successfully created' => 'Organisationgruppe konnte erfolgreich erstellt werden',
    'organisation group %s successfully updated' => 'Organisationgruppe %s konnte erfolgreich aktualisiert werden',
    'organisation group %s successfully deleted' => 'Organisationgruppe %s erfolgreich gelöscht',
    'organisation group %s could not be deleted' => 'Organisationgruppe %s konnte nicht gelöscht werden',
    'organisation group %s successfully reactivated' => 'Organisationsgruppe %s konnte erfolgreich reaktiviert werden',
    "organisation group %s couldn't be reactivated" => 'Organisationsgruppe %s konnte nicht reaktiviert werden',
    'Invalid organisation group id ' => 'Ungültige Organisationgruppen-ID ',
    'Failed to find organisation group with id ' => 'Organisationgruppe mit der Id  konnte nicht gefunden werden',

    /** allgemein */
    'title' => 'Bezeichnung',
    'short title' => 'Abkürzung',
    'back' => 'Zurück',
    'abort' => 'Abbrechen',
    'save' => 'Speichern',

    /** user */
    'user' => 'Benutzer',
    'username is already taken' => 'Der Benutzername ist bereits vergeben',
    'If you want to set a new password, please fill these fields.' => 'Falls Sie ein neues Passwort setzen möchten, füllen Sie bitte folgende Felder aus.',
    'create user' => 'Benutzer anlegen',
    'update user' => 'Benutzer bearbeiten',
    'user successfully created' => 'Nutzer konnte erfolgreich erstellt werden',
    'user %s successfully updated' => 'Nutzer %s konnte erfolgreich aktualisiert werden',
    'user %s successfully deleted' => 'Nutzer %s erfolgreich gelöscht',
    "user %s couldn't be deleted" => 'Nutzer %s konnte nicht gelöscht werden',
    'user %s successfully reactivated' => 'Nutzer %s konnte erfolgreich reaktiviert werden',
    "user %s couldn't be reactivated" => 'Nutzer %s konnte nicht reaktiviert werden',
    "Invalid user id " => "Ungültige Nutzer Id ",
    'Failed to find user with id ' => 'User mit der Id  konnte nicht gefunden werden',
    'is_deleted' => 'Wurde gelöscht',

    /** options */
    'options' => 'Optionen',
    'delete organisation' => "Möchten Sie die Organisation wirklich löschen?",
    'delete organisation group' => "Möchten Sie die Organisationgruppe wirklich löschen?",
    'delete organisation type' => "Möchten Sie den Organisationtypen wirklich löschen?",
    'delete user' => "Möchten Sie den Benutzer wirklich löschen?",
    'reactivate user' => 'Möchten Sie den Benutzer wirklich aktivieren?',
    'reactivate organisation' => "Möchten Sie die Organisation wirklich aktivieren?",
    'reactivate organisation group' => "Möchten Sie die Organisationgruppe wirklich aktivieren?",
    'reactivate organisation type' => "Möchten Sie den Organisationtypen wirklich aktivieren?",
    'open' => 'Öffnen',
    'edit' => 'Bearbeiten',
    'delete' => 'Löschen',
    'reactivate' => 'Reaktivieren',

    /** Allgemein */
    'permissions' => 'Rechte',
    'all' => 'Alle',
    'Your BW account was created' => 'Ihr bwTarifportal Benutzerkonto wurde erstellt.',
    'Your BW account password was reset' => 'Ihr bwTarifportal Passwort wurde zurückgesetzt.',
    'reset password' => 'Passwort zurücksetzen',
    'synch all' => "Alle Nutzer in Outlook synchronisieren",

    /** Permissions */
    'admin_permission' => 'Admin Rechte',
    'edit_permission' => 'Bearbeitungsrechte',
    'view_permission' => 'Rechte zum Sehen von Ansichten',
    'manage_permission' => 'Management',
    'upload_download' => 'Herunterladen/Hochladen'
];