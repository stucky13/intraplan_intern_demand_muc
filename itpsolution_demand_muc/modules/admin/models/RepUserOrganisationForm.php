<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 11:29
 */

namespace app\modules\admin\models;

use app\models\Model;
use app\modules\core\models\Organisation;
use app\modules\core\models\User;
use app\modules\rbac\components\CustomDbManager;


/**
 * Class RepUserOrganisationForm
 * @package app\modules\admin\models
 */
class RepUserOrganisationForm extends Model
{

    public $organisation_id;
    public $user_id;
    public $permissions;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                'organisation_id',
                'exist',
                'targetClass' => Organisation::className(),
                'targetAttribute' => 'organisation_id'
            ],
            [
                'user_id',
                'exist',
                'targetClass' => User::className(),
                'targetAttribute' => 'user_id'
            ],
            [
                //TODO: Validierung obs das Recht überhaupt gibt
                'permissions',
                'each',
                'rule' => [
                    'boolean'
                ]
            ]
        ];
    }


    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'organisation_id' => 'Organisation',
            'permissions' => 'Rechte'
        ];
    }

    /**
     * @return mixed
     */
    public function getOrganisationId()
    {
        return $this->organisation_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $dbManager = new CustomDbManager();

        foreach ($this->permissions as $permission => $value) {
            if ($value) {
                $dbManager->assign($permission, $this->user_id, $this->organisation_id);
            }
        }
        return true;
    }
}