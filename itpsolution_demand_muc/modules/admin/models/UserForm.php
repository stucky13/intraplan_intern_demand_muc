<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 12:52
 */

namespace app\modules\admin\models;

use app\components\Constants;
use app\components\FormModel;
use app\modules\admin\Module;
use app\modules\core\models\AuthAssignment;
use app\modules\core\models\User;
use app\modules\core\models\UserOrganisation;
use app\modules\rbac\components\CustomDbManager;
use Throwable;
use Yii;
use yii\base\Exception;
use yii\db\StaleObjectException;

class  UserForm extends FormModel
{
    public $userId;
    public $username;
    public $password;
    public $passwordRepeat;
    public $firstName;
    public $lastName;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $company;
    public $address1;
    public $address2;
    public $address3;
    public $address4;
    public $isActive = false;
    public $organisations;
    public $repOrganisations;


    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes(),
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                ['username', 'firstName', 'lastName', 'email'],
                'required',
            ],
            [
                ['password', 'passwordRepeat'],
                'string',
                'min' => 6,
            ],
            [
                ['password', 'passwordRepeat'],
                'required',
                'on' => static::SCENARIO_DEFAULT
            ],
            [
                'username',
                'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'username',
                'message' => Module::t('admin', 'username is already taken'),
                'on' => static::SCENARIO_DEFAULT
            ],
            [
                'isActive',
                'boolean',
            ],
            [
                'email',
                'email',
            ],
            [
                'passwordRepeat',
                'compare',
                'compareAttribute' => 'password'
            ],
            [
                ['password', 'passwordRepeat'],
                'validatePassword',
            ]
        ];
    }


    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'username'),
            'password' => Yii::t('app', 'password'),
            'passwordRepeat' => Yii::t('app', 'passwordRepeat'),
            'firstName' => Yii::t('app', 'first name'),
            'lastName' => Yii::t('app', 'last name'),
            'email' => Yii::t('app', 'email'),
            'phone1' => Yii::t('app', 'phone') . ' 1',
            'phone2' => Yii::t('app', 'phone') . ' 2',
            'phone3' => Yii::t('app', 'phone') . ' 3',
            'company' => Yii::t('app', 'company'),
            'address1' => Yii::t('app', 'address') . ' 1',
            'address2' => Yii::t('app', 'address') . ' 2',
            'address3' => Yii::t('app', 'address') . ' 3',
            'address4' => Yii::t('app', 'address') . ' 4',
            'isActive' => Yii::t('app', 'is active')
        ];
    }

    /**
     * Validates if the given password contains at least 1 special char, at least 1 number and at least 6 chars
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        $validatePassword = preg_match('/^.*(?=.{6,})(?=.*[!$%&=?*-:;.,+~@_])(?=.*[0-9])(?=.*[a-z]).*$/', $this->password);

        if (!$validatePassword) {
            $this->addError($attribute, Yii::t('app', 'The password needs to have..'));
        }
    }


    /**
     * Creates a new user, or updates an existing one.
     *
     * @return boolean true, if the user was saved successfully
     * @throws \Exception
     * @throws Throwable
     * @throws Exception
     * @throws StaleObjectException
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $user = new User();
            $user->is_new = 1;
            $user->user_created = Yii::$app->user->getId();
        } else {
            $user = User::findOne($this->id);
            $user->user_updated = Yii::$app->user->getId();
        }

        $user->username = $this->username;
        $user->first_name = $this->firstName;
        $user->last_name = $this->lastName;
        $user->email = $this->email;
        $user->phone1 = $this->phone1;
        $user->phone2 = $this->phone2;
        $user->phone3 = $this->phone3;
        $user->company = $this->company;
        $user->address1 = $this->address1;
        $user->address2 = $this->address2;
        $user->address3 = $this->address3;
        $user->address4 = $this->address4;
        $user->is_active = $this->isActive;

        if ($this->getScenario() == static::SCENARIO_DEFAULT || !empty($this->password)) {
            $user->setPassword($this->password);
            $user->is_password_change_required = 1;
        }

        if (!$user->save()) {
            return false;
        }
        $this->userId = Yii::$app->db->getLastInsertID();
        // Saved User is admin, dont delete/set any organisations or permissions
        if (AuthAssignment::find()->where(['item_name' => 'admin', 'user_id' => $user->getId()])->one()) {
            return true;
        };

        // User edits himself without Organisation assignments
        if ($user->getId() == Yii::$app->user->getId() &&
            (
                !Yii::$app->getUser()->can(Constants::ADMIN) &&
                !Yii::$app->getUser()->can(Constants::EDIT_USER)
            )) {
            return true;
        }

        $oldUserOrganisations = UserOrganisation::find()->where(['user_id' => $user->getId()])->all();
        foreach ($oldUserOrganisations as $oldUserOrganisation) {
            $oldUserOrganisation->delete();
        }

        $dbManager = new CustomDbManager();
        $dbManager->revokeAll($user->getId());

        if (sizeof($this->organisations) > 0) {
            $savedOrganisations = [];
            /** @var UserOrganisationForm $organisation */
            foreach ($this->organisations as $organisation) {
                $organisation->user_id = $user->getId();
                if (!in_array($organisation->getOrganisationId(), $savedOrganisations) && $organisation->organisation_id != null) {
                    $savedOrganisations[] = $organisation->getOrganisationId();
                    $organisation->save();
                }
            }
        }
        if ($this->repOrganisations && sizeof($this->repOrganisations) > 0) {
            /** @var RepUserOrganisationForm $repOrganisation */
            foreach ($this->repOrganisations as $repOrganisation) {
                $repOrganisation->user_id = $user->getId();
                if ($repOrganisation->organisation_id != null) {
                    $repOrganisation->save();
                }
            }
        }

        return true;
    }
}