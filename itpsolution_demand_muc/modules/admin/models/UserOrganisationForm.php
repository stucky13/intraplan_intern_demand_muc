<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 11:29
 */

namespace app\modules\admin\models;

use app\components\Constants;
use app\models\Model;
use app\modules\core\models\Organisation;
use app\modules\core\models\User;
use app\modules\core\models\UserOrganisation;
use app\modules\rbac\components\CustomDbManager;
use Yii;


/**
 * Class UserOrganisationForm
 * @package app\modules\admin\models
 */
class UserOrganisationForm extends Model
{
    public $organisation_id;
    public $user_id;
    public $permissions;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                'organisation_id',
                'required'
            ],
            [
                'organisation_id',
                'exist',
                'targetClass' => Organisation::className(),
                'targetAttribute' => 'organisation_id'
            ],
            [
                'user_id',
                'exist',
                'targetClass' => User::className(),
                'targetAttribute' => 'user_id'
            ],
            [
                //TODO: Validierung obs das Recht überhaupt gibt
                'permissions',
                'each',
                'rule' => [
                    'boolean'
                ]
            ]
        ];
    }


    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'organisation_id' => 'Organisation',
            'permissions' => 'Rechte'
        ];
    }

    public function getOrganisationPermissions()
    {
        return $this->getOrganisation() ? $this->getOrganisation()->getPermissions()->all() : [];
    }

    public function getOrganisation()
    {
        return Organisation::find()->where(['organisation_id' => $this->getOrganisationId()])->one();
    }

    /**
     * @return mixed
     */
    public function getOrganisationId()
    {
        return $this->organisation_id;
    }

    public function getOrganisationAssignablePermissions()
    {
        return $this->getOrganisation() ? $this->getOrganisation()->getRepresentative()->getPermissionTitlesByOrganisationId($this->organisation_id) : [];
    }

    public function areCheckboxesDisabled()
    {
        if ($this->getOrganisation()) {
            return $this->getOrganisation()->getRepresentativeId() != Yii::$app->getUser()->getId() && !Yii::$app->getUser()->can(Constants::EDIT_USER);
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    public function save()
    {
        $userOrganisation = new UserOrganisation();
        $userOrganisation->user_id = $this->user_id;
        $userOrganisation->organisation_id = $this->organisation_id;
        $userOrganisation->dt_created = date("Y-m-d H:i:s");
        $userOrganisation->user_created = Yii::$app->getUser()->getId();
        if (!$userOrganisation->save() || !$this->saveAuthAssignments($this->permissions)) {
            return false;
        };
        return true;
    }

    private function saveAuthAssignments($permissions)
    {
        $dbManager = new CustomDbManager();

        foreach ($permissions as $permission => $value) {
            if ($value) {
                $dbManager->assign($permission, $this->user_id, $this->organisation_id);
            }

        }
        return true;
    }
}