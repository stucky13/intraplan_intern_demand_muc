<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 14:21
 */

namespace app\modules\admin\models;


use yii\base\Model;

class OrganisationSessionForm extends Model
{
    public $organisation;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [
                'organisation',
                'string'
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'organisation' => 'Organisation'
        ];
    }
}