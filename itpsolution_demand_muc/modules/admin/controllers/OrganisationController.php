<?php

namespace app\modules\admin\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\components\FormModel;
use app\modules\admin\models\OrganisationForm;
use app\modules\admin\Module;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\Permission;
use app\modules\core\models\User;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;


/**
 * Class OrganisationController
 *
 * @package app\modules\admin\controllers
 */
class OrganisationController extends BaseController
{
    /**
     * Displays the overview.
     *
     * @return string the content
     */
    public function actionIndex($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_ORGANISATION)) {
            return $this->goHome();
        }
        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Organisation::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Displays the detail view for the organisation.
     *
     * @param int $id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation id
     */
    public function actionView($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_ORGANISATION)) {
            return $this->goHome();
        }
        $organisation = Organisation::findOne($id);
        if ($organisation == null) {
            throw new BadRequestHttpException(Module::t('admin', "Invalid organisation id ") . $id);
        }

        return $this->render('view', [
            'organisation' => $organisation,
            'page' => $page
        ]);
    }

    /**
     * Displays the organisation form.
     *
     * @return string the content
     * @throws \Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionCreate($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION)) {
            return $this->goHome();
        }
        $model = new OrganisationForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('admin', 'organisation successfully created'));
            return $this->redirect(["index?page=$page"]);
        }

        $organisationTypes = [];
        /** @var OrganisationType $organisationType */
        foreach (OrganisationType::find()->all() as $organisationType) {
            $organisationTypes[$organisationType->getId()] = $organisationType->getTitle();
        }


        $users = [];
        /** @var User $user */
        foreach (User::find()->all() as $user) {
            $users[$user->getId()] = $user->getUsername();
        }

        $permissionArray = [];
        foreach (Constants::PERMISSION_GROUP_TITLES as $groupId => $title) {
            $permissionArray[$title] = Permission::find()->where(["permission_group_id" => $groupId])->andWhere(['is_assignable' => 1])->all();
        }

        return $this->render("form", [
            'model' => $model,
            'organisationTypes' => $organisationTypes,
            'users' => $users,
            'allPermissions' => $permissionArray,
            'page' => $page
        ]);
    }

    /**
     * Displays the organisation edit form.
     *
     * @param int $id the organisation id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation id
     * @throws \Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionUpdate($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION)) {
            return $this->goHome();
        }
        /** @var Organisation $organisation */
        $organisation = Organisation::findOne($id);
        if ($organisation == null) {
            throw new BadRequestHttpException(Module::t('admin', "Failed to find organisation with id ") . $id);
        }

        $model = new OrganisationForm(FormModel::SCENARIO_UPDATE, $id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('admin', 'organisation %s successfully updated'), $model->title);
            return $this->redirect(["index?page=$page"]);
        } else {
            $model->organisationId = $organisation->getId();
            $model->title = $organisation->getTitle();
            $model->shortTitle = $organisation->getShortTitle();
            $model->organisationTypeId = $organisation->getOrganisationTypeId();
            $model->representativeId = $organisation->getRepresentativeId();
            $model->street = $organisation->getStreet();
            $model->houseNumber = $organisation->getHouseNumber();
            $model->zip = $organisation->getZip();
            $model->city = $organisation->getCity();
            $model->permissions = array_fill_keys($organisation->getPermissionTitles(), 1);
        }

        $organisationTypes = ArrayHelper::map(OrganisationType::find()->all(), 'organisation_type_id', 'title');

        $allPermissions = [];
        foreach ($organisation->getOrganisationType()->getPermissionTitles() as $orgaPermission) {
            $allPermissions[] = Permission::find()->where(['permission' => $orgaPermission])->one();
        }

        $permissionArray = [];
        /** @var Permission $permission */
        foreach ($allPermissions as $permission) {
            $permissionArray[$permission->getPermissionGroupId()][] = $permission;
        }

        foreach (Constants::PERMISSION_GROUP_TITLES as $group_id => $title) {
            if (isset($permissionArray[$group_id])) {
                $permissionArray[$title] = $permissionArray[$group_id];
                unset($permissionArray[$group_id]);
            }
        }

        return $this->render("form", [
            'model' => $model,
            'organisationTypes' => $organisationTypes,
            'allPermissions' => $permissionArray,
            'page' => $page,
        ]);
    }

    /**
     * Deletes the specified organisation.
     *
     * @param int $id the organisation id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var Organisation $organisation */
        $organisation = Organisation::findOne($id);
        if ($organisation == null) {
            throw new BadRequestHttpException("Failed to find organisation with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $organisation->is_deleted = 1;
            $organisation->save();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "organisation %s successfully deleted"), $organisation->getTitle());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "organisation %s couldn't be deleted"), $organisation->getTitle());
        }

        return $this->redirect(['index']);
    }

    public function actionReactivate($id)
    {
        /** @var Organisation $organisation */
        $organisation = Organisation::findOne($id);
        if ($organisation == null) {
            throw new BadRequestHttpException("Failed to find organisation with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $organisation->is_deleted = 0;
            $organisation->save();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "organisation %s successfully reactivated"), $organisation->getTitle());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "organisation %s couldn't be reactivated"), $organisation->getTitle());
        }
        return $this->redirect(['index']);
    }


    /**
     * @param $id int TypeId
     * @return string json
     */
    public function actionGetAssignablePermissionsByType($id)
    {
        /** @var OrganisationType $organisationType */
        $organisationType = OrganisationType::find()->where(['organisation_type_id' => $id])->one();
        if (!$organisationType) {
            return json_encode([]);
        }
        return json_encode($organisationType->getPermissionTitles());
    }

    /**
     * @param $orgId
     * @return string
     * @throws \Exception
     * @throws Throwable
     */
    public function actionGetAssignablePermissionsByUserAndOrg($orgId)
    {
        /** @var User $user */
        $user = Yii::$app->getUser();

        if ($user->can(Constants::ADMIN) || $user->can(Constants::EDIT_USER)) {
            return json_encode(Organisation::find()->where(['organisation_id' => $orgId])->one()->getPermissionTitles());
        } else {
            $user = User::find()->where(['user_id' => $user->getId()])->one();
            return json_encode($user->getPermissionTitlesByOrganisationId($orgId));
        }
    }
}