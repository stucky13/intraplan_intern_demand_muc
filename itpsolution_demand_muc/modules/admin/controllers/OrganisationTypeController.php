<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 11:31
 */

namespace app\modules\admin\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\components\FormModel;
use app\modules\admin\models\OrganisationTypeForm;
use app\modules\admin\Module;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\Permission;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\BadRequestHttpException;


/**
 * Class OrganisationTypeController
 *
 * @package app\modules\admin\controllers
 */
class OrganisationTypeController extends BaseController
{
    /**
     * Displays the overview.
     *
     * @return string the content
     */
    public function actionIndex($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_ORGANISATION_TYPE)) {
            return $this->goHome();
        }
        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => OrganisationType::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Displays the detail view for the specified organisation type.
     *
     * @param int $id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation type id
     */
    public function actionView($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_ORGANISATION_TYPE)) {
            return $this->goHome();
        }
        $organisationType = OrganisationType::findOne($id);
        if ($organisationType == null) {
            throw new BadRequestHttpException(Module::t('admin', "Invalid organisation type id ") . $id);
        }

        return $this->render('view', [
            'organisationType' => $organisationType,
            'page' => $page
        ]);
    }

    /**
     * Displays the organisation type form.
     *
     * @return string the content
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionCreate($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION_TYPE)) {
            return $this->goHome();
        }
        $model = new OrganisationTypeForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('admin', 'organisation type successfully created'));
            return $this->redirect(["index?page=$page"]);
        }

        $permission = [];
        foreach (Constants::PERMISSION_GROUP_TITLES as $groupId => $title) {
            $permission[$title] = Permission::find()->where(["permission_group_id" => $groupId])->andWhere(['is_assignable' => 1])->all();
        }

        return $this->render("form", [
            'model' => $model,
            'allPermissions' => $permission,
            'page' => $page
        ]);
    }

    /**
     * Displays the organisation type edit form.
     *
     * @param int $id the organisation type id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation type id
     * @throws \Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionUpdate($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION_TYPE)) {
            return $this->goHome();
        }
        /** @var OrganisationType $organisationType */
        $organisationType = OrganisationType::findOne($id);
        if ($organisationType == null) {
            throw new BadRequestHttpException(Module::t('admin', "Failed to find organisation type with id ") . $id);
        }

        $model = new OrganisationTypeForm(FormModel::SCENARIO_UPDATE, $id);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('admin', 'organisation type %s successfully updated'), $model->title);
            return $this->redirect(["index?page=$page"]);
        } else {
            $model->organisationTypeId = $organisationType->getId();
            $model->title = $organisationType->getTitle();
            $model->shortTitle = $organisationType->getShortTitle();
            $model->permissions = array_fill_keys($organisationType->getPermissionTitles(), 1);
        }
        /** @var Permission $permission */
        $permission = [];
        foreach (Constants::PERMISSION_GROUP_TITLES as $groupId => $title) {
            $permission[$title] = Permission::find()->where(["permission_group_id" => $groupId])->andWhere(['is_assignable' => 1])->all();
        }

        return $this->render("form", [
            'model' => $model,
            'allPermissions' => $permission,
            'page' => $page
        ]);
    }

    /**
     * Deletes the specified organisation type.
     *
     * @param int $id the organisation type id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid organisation type id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var OrganisationType $organisationType */
        $organisationType = OrganisationType::findOne($id);
        if ($organisationType == null) {
            throw new BadRequestHttpException("Failed to find organisation type with id " . $id);
        }

        $organisationCount = $organisationType->getOrganisations()->count();
        if ($organisationCount > 0) {
            Alert::addError(Module::t("admin", "organisation type {title} currently used by {count} organisations"), [
                'title' => $organisationType->getTitle(),
                'count' => $organisationCount
            ]);
            return $this->redirect(['index']);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $organisationType->is_deleted = 1;
            $organisationType->save();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "organisation type %s successfully deleted"), $organisationType->getTitle());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "organisation type %s couldn't be deleted"), $organisationType->getTitle());
        }

        return $this->redirect(['index']);
    }

    public function actionReactivate($id)
    {
        /** @var OrganisationType $organisationType */
        $organisationType = OrganisationType::findOne($id);
        if ($organisationType == null) {
            throw new BadRequestHttpException("Failed to find organisation type with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $organisationType->is_deleted = 0;
            $organisationType->save();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "organisation type %s successfully reactivated"), $organisationType->getTitle());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "organisation type %s couldn't be reactivated"), $organisationType->getTitle());
        }
        return $this->redirect(['index']);
    }
}