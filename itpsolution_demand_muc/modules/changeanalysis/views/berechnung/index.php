<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.08.2019
 * Time: 09:47
 */
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\components\Constants;
use app\modules\changeanalysis\Module;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = Module::t('changeanalysis', 'Übersicht der Berechnungsvorgänge ');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color',
    ],
    'columns' => [
        'Berechnung ID' => [
            'label' => Module::t('changeanalysis', 'Berechnung ID'),
            'attribute' => 'berechnung_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'Bezeichnung' => [
            'attribute' => 'bezeichnung',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'User ID' => [
            'label' => Module::t('changeanalysis', 'User ID'),
            'attribute' => 'user_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
        ],
        'Startzeitpunkt' => [
            'attribute' => 'startzeitpunkt',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'Status' => [
            'attribute' => 'status',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],

        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view} ',
            'buttons' => [
                'view' => function ($url) {
                    return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open', 'title' => Module::t('changeanalysis', 'Details anschauen')]);
                },
//                'update' => function($url) use ($page) {
//                    if (Yii::$app->getUser()->can(Constants::ADMIN) || Yii::$app->getUser()->can(Constants::EDIT_CHANGE_ANALYSIS)){
//                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-pencil', 'title' => Module::t('changeanalysis', 'update')]);
//                    }
//                },
            ]
        ]
    ]
]) ?>

<div class="clearfix">
    <?php if (Yii::$app->getUser()->can(Constants::EDIT_CHANGE_ANALYSIS)):?>
    <?= Html::a(Module::t('changeanalysis', "neue Berechnung durchführen"), ["create?page=$page"], [
        'class' => 'btn mediumButton pull-right'
    ]); ?>
    <?php endif; ?>
    <?= Html::a(Module::t('admin', 'Zurück'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>