<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.08.2019
 * Time: 09:50
 */
namespace app\modules\changeanalysis;
use Yii;

class Module extends \yii\base\Module
{
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/changeanalysis/' . $category, $message, $params, $language);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->defaultRoute = 'changeanalysis/index';
        $this->registerTranslations();
    }

    /**
     * Registers the module translations
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/changeanalysis/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/changeanalysis/messages',
            'fileMap' => [
                'modules/admin/admin' => 'changeanalysis.php'
            ],
        ];
    }
}