<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 06.08.2019
 * Time: 16:46
 */

namespace app\modules\changeanalysis\models;


use app\components\FormModel;
use app\modules\changeanalysis\Module;
use app\modules\core\models\User;
use DateTime;
use Yii;
use yii\base\Exception;
use yii\db\StaleObjectException;

class BerechnungForm extends FormModel
{
    public $berechnungId;
    public $userId;
    public $bezeichnung;
    public $status;
    public $anmerkung;
    public $startzeitpunkt;
    public $details;
    public $muc_fahrzeit_funktion;
    public $muc_fahrzeit_hbf_ist;
    public $muc_fahrzeit_hbf_neu;
    public $muc_fahrzeit_no_ist;
    public $muc_fahrzeit_no_neu;
    public $muc_fahrzeit_so_ist;
    public $muc_fahrzeit_so_neu;
    public $muc_fahrzeit_w_ist;
    public $muc_fahrzeit_w_neu;
    public $muc_takt_funktion;
    public $muc_takt_hbf_ist;
    public $muc_takt_hbf_neu;
    public $muc_takt_no_ist;
    public $muc_takt_no_neu;
    public $muc_takt_so_ist;
    public $muc_takt_so_neu;
    public $muc_takt_w_ist;
    public $muc_takt_w_neu;
    public $muc_direktanbindung_funktion;
    public $muc_direktanbindung_hbf_ist;
    public $muc_direktanbindung_hbf_neu;
    public $muc_direktanbindung_no_ist;
    public $muc_direktanbindung_no_neu;
    public $muc_direktanbindung_so_ist;
    public $muc_direktanbindung_so_neu;
    public $muc_direktanbindung_w_ist;
    public $muc_direktanbindung_w_neu;
    public $muc_hbf_produkt_funktion;
    public $muc_hbf_produkt_janein;
    public $muc_hbf_produkt_preisaufschlag;
    public $muc_str_fahrzeit_funktion;
    public $muc_str_fahrzeit_a_ist;
    public $muc_str_fahrzeit_a_neu;
    public $muc_fra_fahrzeit_funktion;
    public $muc_fra_fahrzeit_achse_s_ist;
    public $muc_fra_fahrzeit_achse_s_neu;
    public $muc_fra_fahrzeit_achse_n_ist;
    public $muc_fra_fahrzeit_achse_n_neu;
    public $muc_vie_fahrzeit_funktion;
    public $muc_vie_fahrzeit_sal_ist;
    public $muc_vie_fahrzeit_sal_neu;
    public $muc_miv_fahrzeit_funktion;
    public $muc_miv_fahrzeit_a9_ist;
    public $muc_miv_fahrzeit_a9_neu;
    public $muc_miv_fahrzeit_ad_neu;
    public $muc_miv_fahrzeit_ad_ist;
    public $muc_miv_fahrzeit_ak_s_ist;
    public $muc_miv_fahrzeit_ak_s_neu;
    public $muc_miv_fahrzeit_ak_w_ist;
    public $muc_miv_fahrzeit_ak_w_neu;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes()
        ];
    }

    public function rules()
    {
        return [
            [
                ['bezeichnung'],
                'required'
            ],
            [
                ['muc_fahrzeit_funktion'/*=>'required_without_all: muc_takt_funktion, muc_direktanbindung_funktion, muc_hbf_produkt_funktion, muc_hbf_produkt_funktion, muc_str_fahrzeit_funktion, muc_fra_fahrzeit_funktion, muc_vie_fahrzeit_funktion, muc_miv_fahrzeit_funktion']*/
                ,'muc_takt_funktion', 'muc_direktanbindung_funktion', 'muc_hbf_produkt_funktion', 'muc_hbf_produkt_janein', 'muc_str_fahrzeit_funktion', 'muc_fra_fahrzeit_funktion', 'muc_vie_fahrzeit_funktion', 'muc_miv_fahrzeit_funktion'],
                'boolean'
            ],
            [
                ['muc_fahrzeit_hbf_ist', 'muc_fahrzeit_hbf_neu', 'muc_fahrzeit_no_ist', 'muc_fahrzeit_no_neu', 'muc_fahrzeit_so_ist', 'muc_fahrzeit_so_neu', 'muc_fahrzeit_w_ist', 'muc_fahrzeit_w_neu', 'muc_takt_hbf_ist', 'muc_takt_hbf_neu',
                    'muc_takt_no_ist', 'muc_takt_no_neu', 'muc_takt_so_ist', 'muc_takt_so_neu', 'muc_takt_w_ist', 'muc_takt_w_neu', 'muc_direktanbindung_hbf_ist', 'muc_direktanbindung_hbf_neu', 'muc_direktanbindung_no_ist', 'muc_direktanbindung_no_neu',
                    'muc_direktanbindung_so_ist', 'muc_direktanbindung_so_neu', 'muc_direktanbindung_w_ist', 'muc_direktanbindung_w_neu', 'muc_str_fahrzeit_a_ist', 'muc_str_fahrzeit_a_neu', 'muc_fra_fahrzeit_achse_s_ist', 'muc_fra_fahrzeit_achse_s_neu',
                    'muc_fra_fahrzeit_achse_n_ist', 'muc_fra_fahrzeit_achse_n_neu', 'muc_vie_fahrzeit_sal_ist', 'muc_vie_fahrzeit_sal_neu', 'muc_miv_fahrzeit_a9_ist', 'muc_miv_fahrzeit_a9_neu', 'muc_miv_fahrzeit_ad_ist', 'muc_miv_fahrzeit_ad_neu',
                    'muc_miv_fahrzeit_ak_s_ist', 'muc_miv_fahrzeit_ak_s_neu', 'muc_miv_fahrzeit_ak_w_ist', 'muc_miv_fahrzeit_ak_w_neu', 'muc_hbf_produkt_preisaufschlag'],
                'validateGrenzwerte'
            ],
        ];
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'berechnungId' => Module::t('changeanalysis', 'berechnung id'),
            'userId' => Yii::t('app', 'user id'),
            'bezeichnung' => Module::t('changeanalysis', 'Bezeichnung'),
            'status' => Module::t('changeanalysis', 'status'),
            'anmerkung' => Module::t('changeanalysis', 'Anmerkung'),
            'details' => Module::t('changeanalysis', 'details'),
            'muc_fahrzeit_funktion' => Module::t('changeanalysis', 'muc_fahrzeit_funktion'),
            'muc_fahrzeit_hbf_ist' => Module::t('changeanalysis', 'muc_fahrzeit_hbf_ist'),
            'muc_fahrzeit_hbf_neu' => Module::t('changeanalysis', 'muc_fahrzeit_hbf_neu'),
            'muc_fahrzeit_no_ist' => Module::t('changeanalysis', 'muc_fahrzeit_no_ist'),
            'muc_fahrzeit_no_neu' => Module::t('changeanalysis', 'muc_fahrzeit_no_neu'),
            'muc_fahrzeit_so_ist' => Module::t('changeanalysis', 'muc_fahrzeit_so_ist'),
            'muc_fahrzeit_so_neu' => Module::t('changeanalysis', 'muc_fahrzeit_so_neu'),
            'muc_fahrzeit_w_ist' => Module::t('changeanalysis', 'muc_fahrzeit_w_ist'),
            'muc_fahrzeit_w_neu' => Module::t('changeanalysis', 'muc_fahrzeit_w_neu'),
            'muc_takt_funktion' => Module::t('changeanalysis', 'muc_takt_funktion'),
            'muc_takt_hbf_ist' => Module::t('changeanalysis', 'muc_takt_hbf_ist'),
            'muc_takt_hbf_neu' => Module::t('changeanalysis', 'muc_takt_hbf_neu'),
            'muc_takt_no_ist' => Module::t('changeanalysis', 'muc_takt_no_ist'),
            'muc_takt_no_neu' => Module::t('changeanalysis', 'muc_takt_no_neu'),
            'muc_takt_so_ist' => Module::t('changeanalysis', 'muc_takt_so_ist'),
            'muc_takt_so_neu' => Module::t('changeanalysis', 'muc_takt_so_neu'),
            'muc_takt_w_ist' => Module::t('changeanalysis', 'muc_takt_w_ist'),
            'muc_takt_w_neu' => Module::t('changeanalysis', 'muc_takt_w_neu'),
            'muc_direktanbindung_funktion' => Module::t('changeanalysis', 'muc_direktanbindung_funktion'),
            'muc_direktanbindung_hbf_ist' => Module::t('changeanalysis', 'mucDirektanbindunfHbfIst'),
            'muc_direktanbindung_hbf_neu' => Module::t('changeanalysis', 'muc_direktanbindung_hbf_neu'),
            'muc_direktanbindung_no_ist' => Module::t('changeanalysis', 'muc_direktanbindung_no_ist'),
            'muc_direktanbindung_no_neu' => Module::t('changeanalysis', 'muc_direktanbindung_no_neu'),
            'muc_direktanbindung_so_ist' => Module::t('changeanalysis', 'muc_direktanbindung_so_ist'),
            'muc_direktanbindung_so_neu' => Module::t('changeanalysis', 'muc_direktanbindung_so_neu'),
            'muc_direktanbindung_w_ist' => Module::t('changeanalysis', 'muc_direktanbindung_w_ist'),
            'muc_direktanbindung_w_neu' => Module::t('changeanalysis', 'muc_direktanbindung_w_neu'),
            'muc_hbf_produkt_funktion' => Module::t('changeanalysis', 'muc_hbf_produkt_funktion'),
            'muc_hbf_produkt_janein' => Module::t('changeanalysis', 'muc_hbf_produkt_janein'),
            'muc_hbf_produkt_preisaufschlag' => Module::t('changeanalysis', 'muc_hbf_produkt_preisaufschlag'),
            'muc_str_fahrzeit_funktion' => Module::t('changeanalysis', 'muc_str_fahrzeit_funktion'),
            'muc_str_fahrzeit_a_ist' => Module::t('changeanalysis', 'muc_str_fahrzeit_a_ist'),
            'muc_str_fahrzeit_a_neu' => Module::t('changeanalysis', 'muc_str_fahrzeit_a_neu'),
            'muc_fra_fahrzeit_funktion' => Module::t('changeanalysis', 'muc_fra_fahrzeit_funktion'),
            'muc_fra_fahrzeit_achse_s_ist' => Module::t('changeanalysis', 'muc_fra_fahrzeit_achse_s_ist'),
            'muc_fra_fahrzeit_achse_s_neu' => Module::t('changeanalysis', 'muc_fra_fahrzeit_achse_s_neu'),
            'muc_fra_fahrzeit_achse_n_ist' => Module::t('changeanalysis', 'muc_fra_fahrzeit_achse_n_ist'),
            'muc_fra_fahrzeit_achse_n_neu' => Module::t('changeanalysis', 'muc_fra_fahrzeit_achse_n_neu'),
            'muc_vie_fahrzeit_funktion' => Module::t('changeanalysis', 'muc_vie_fahrzeit_funktion'),
            'muc_vie_fahrzeit_sal_ist' => Module::t('changeanalysis', 'muc_vie_fahrzeit_sal_ist'),
            'muc_vie_fahrzeit_sal_neu' => Module::t('changeanalysis', 'muc_vie_fahrzeit_sal_neu'),
            'muc_miv_fahrzeit_funktion' => Module::t('changeanalysis', 'muc_miv_fahrzeit_funktion'),
            'muc_miv_fahrzeit_a9_ist' => Module::t('changeanalysis', 'muc_miv_fahrzeit_a9_ist'),
            'muc_miv_fahrzeit_a9_neu' => Module::t('changeanalysis', 'muc_miv_fahrzeit_a9_neu'),
            'muc_miv_fahrzeit_ad_neu' => Module::t('changeanalysis', 'muc_miv_fahrzeit_ad_neu'),
            'muc_miv_fahrzeit_ak_s_ist' => Module::t('changeanalysis', 'muc_miv_fahrzeit_ak_w_ist'),
            'muc_miv_fahrzeit_ak_s_neu' => Module::t('changeanalysis', 'muc_miv_fahrzeit_ak_s_neu'),
            'muc_miv_fahrzeit_ak_w_ist' => Module::t('changeanalysis', 'muc_miv_fahrzeit_ak_w_ist'),
            'muc_miv_fahrzeit_ak_w_neu' => Module::t('changeanalysis', 'muc_miv_fahrzeit_ak_w_neu')
        ];
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     */


    public function validateGrenzwerte($attribute, $params)
    {
        $bezeichnung = substr($attribute, 4, -3);
        $bezeichnung = str_replace('direktanbindung', 'direktverbindung', $bezeichnung);


        /** @var Grenzwerte $grenzwert */
        $grenzwert = Grenzwerte::find()->where(['LIKE', 'bezeichnung', $bezeichnung])->one();

        if ($this->$attribute) {
            if ($this->$attribute > $grenzwert->getMax() or $this->$attribute < $grenzwert->getMin()) {
                $this->addError($attribute, "Die Werte der " . Module::t('changeanalysis', $attribute) . " müssen innerhalb der Grenzwerte " . $grenzwert->getMin() . " und " . $grenzwert->getMax() . " liegen");
                return false;
            }
        }

        return true;
    }


    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $berechnung = new Berechnung();
            $berechnung->user_id = Yii::$app->user->getId();
        } else {
            $berechnung = Berechnung::findOne($this->id);
        }
        $berechnung->berechnung_id = $this->berechnungId;
        $berechnung->bezeichnung = $this->bezeichnung;
        $berechnung->anmerkung = $this->anmerkung;
        $berechnung->muc_fahrzeit_funktion = $this->muc_fahrzeit_funktion;
        $berechnung->muc_fahrzeit_hbf_ist = $this->muc_fahrzeit_hbf_ist;
        $berechnung->muc_fahrzeit_hbf_neu = $this->muc_fahrzeit_hbf_neu;
        $berechnung->muc_fahrzeit_no_ist = $this->muc_fahrzeit_no_ist;
        $berechnung->muc_fahrzeit_no_neu = $this->muc_fahrzeit_no_neu;
        $berechnung->muc_fahrzeit_so_ist = $this->muc_fahrzeit_so_ist;
        $berechnung->muc_fahrzeit_so_neu = $this->muc_fahrzeit_so_neu;
        $berechnung->muc_fahrzeit_w_ist = $this->muc_fahrzeit_w_ist;
        $berechnung->muc_fahrzeit_w_neu = $this->muc_fahrzeit_w_neu;
        $berechnung->muc_takt_funktion = $this->muc_takt_funktion;
        $berechnung->muc_takt_hbf_ist = $this->muc_takt_hbf_ist;
        $berechnung->muc_takt_hbf_neu = $this->muc_takt_hbf_neu;
        $berechnung->muc_takt_no_ist = $this->muc_takt_no_ist;
        $berechnung->muc_takt_no_neu = $this->muc_takt_no_neu;
        $berechnung->muc_takt_so_ist = $this->muc_takt_so_ist;
        $berechnung->muc_takt_w_ist = $this->muc_takt_w_ist;
        $berechnung->muc_takt_w_neu = $this->muc_takt_w_neu;
        $berechnung->muc_direktanbindung_funktion = $this->muc_direktanbindung_funktion;
        $berechnung->muc_direktanbindung_hbf_ist = $this->muc_direktanbindung_hbf_ist;
        $berechnung->muc_direktanbindung_hbf_neu = $this->muc_direktanbindung_hbf_neu;
        $berechnung->muc_direktanbindung_no_ist = $this->muc_direktanbindung_no_ist;
        $berechnung->muc_direktanbindung_no_neu = $this->muc_direktanbindung_no_neu;
        $berechnung->muc_direktanbindung_so_ist = $this->muc_direktanbindung_so_ist;
        $berechnung->muc_direktanbindung_so_neu = $this->muc_direktanbindung_so_neu;
        $berechnung->muc_direktanbindung_w_ist = $this->muc_direktanbindung_w_ist;
        $berechnung->muc_direktanbindung_w_neu = $this->muc_direktanbindung_w_neu;
        $berechnung->muc_hbf_produkt_funktion = $this->muc_hbf_produkt_funktion;
        $berechnung->muc_hbf_produkt_janein = $this->muc_hbf_produkt_janein;
        $berechnung->muc_hbf_produkt_preisaufschlag = $this->muc_hbf_produkt_preisaufschlag;
        $berechnung->muc_str_fahrzeit_funktion = $this->muc_str_fahrzeit_funktion;
        $berechnung->muc_str_fahrzeit_a_ist = $this->muc_str_fahrzeit_a_ist;
        $berechnung->muc_str_fahrzeit_a_neu = $this->muc_str_fahrzeit_a_neu;
        $berechnung->muc_fra_fahrzeit_funktion = $this->muc_fra_fahrzeit_funktion;
        $berechnung->muc_fra_fahrzeit_achse_s_ist = $this->muc_fra_fahrzeit_achse_s_ist;
        $berechnung->muc_fra_fahrzeit_achse_n_ist = $this->muc_fra_fahrzeit_achse_n_ist;
        $berechnung->muc_fra_fahrzeit_achse_n_neu = $this->muc_fra_fahrzeit_achse_n_neu;
        $berechnung->muc_vie_fahrzeit_funktion = $this->muc_vie_fahrzeit_funktion;
        $berechnung->muc_vie_fahrzeit_sal_ist = $this->muc_vie_fahrzeit_sal_ist;
        $berechnung->muc_vie_fahrzeit_sal_neu = $this->muc_vie_fahrzeit_sal_neu;
        $berechnung->muc_miv_fahrzeit_funktion = $this->muc_miv_fahrzeit_funktion;
        $berechnung->muc_miv_fahrzeit_a9_ist = $this->muc_miv_fahrzeit_a9_ist;
        $berechnung->muc_miv_fahrzeit_a9_neu = $this->muc_miv_fahrzeit_a9_neu;
        $berechnung->muc_miv_fahrzeit_ad_ist = $this->muc_miv_fahrzeit_ad_ist;
        $berechnung->muc_miv_fahrzeit_ad_neu = $this->muc_miv_fahrzeit_ad_neu;
        $berechnung->muc_miv_fahrzeit_ak_s_ist = $this->muc_miv_fahrzeit_ak_s_ist;
        $berechnung->muc_miv_fahrzeit_ak_s_neu = $this->muc_miv_fahrzeit_ak_s_neu;
        $berechnung->muc_miv_fahrzeit_ak_w_ist = $this->muc_miv_fahrzeit_ak_w_ist;
        $berechnung->muc_miv_fahrzeit_ak_w_neu = $this->muc_miv_fahrzeit_ak_w_neu;

        if (!$berechnung->save()) {
            return false;
        }

        return true;
    }
}