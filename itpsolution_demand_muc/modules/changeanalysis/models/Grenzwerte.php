<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 06.08.2019
 * Time: 15:43
 */

namespace app\modules\changeanalysis\models;
use app\components\AbstractActiveRecord;

/**
 * Class Grenzwerte
 * @property int $nr
 * @property string $bezeichnung
 * @property int $max
 * @property int $min
 * @property string $anmerkung
 */
class Grenzwerte extends AbstractActiveRecord
{
    /**
     * @return int
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @return string
     */
    public function getAnmerkung()
    {
        return $this->anmerkung;
    }

}