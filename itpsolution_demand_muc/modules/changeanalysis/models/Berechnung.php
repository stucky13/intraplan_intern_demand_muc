<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 06.08.2019
 * Time: 15:43
 */
namespace app\modules\changeanalysis\models;
use app\components\AbstractActiveRecord;
use app\modules\core\components\BerechnungQuery;
use yii\db\ActiveRecord;

/**
 * Class Berechnung
 * @property int $berechnung_id
 * @property int $user_id
 * @property string $bezeichnung
 * @property int $status
 * @property string $anmerkung
 * @property string $startzeitpunkt
 * @property string $details
 * @property int $muc_fahrzeit_funktion
 * @property int $muc_fahrzeit_hbf_ist
 * @property int $muc_fahrzeit_hbf_neu
 * @property int $muc_fahrzeit_no_ist
 * @property int $muc_fahrzeit_no_neu
 * @property int $muc_fahrzeit_so_ist
 * @property int $muc_fahrzeit_so_neu
 * @property int $muc_fahrzeit_w_ist
 * @property int $muc_fahrzeit_w_neu
 * @property int $muc_takt_funktion
 * @property int $muc_takt_hbf_ist
 * @property int $muc_takt_hbf_neu
 * @property int $muc_takt_no_ist
 * @property int $muc_takt_no_neu
 * @property int $muc_takt_so_ist
 * @property int $muc_takt_so_neu
 * @property int $muc_takt_w_ist
 * @property int $muc_takt_w_neu
 * @property int $muc_direktanbindung_funktion
 * @property int $muc_direktanbindung_hbf_ist
 * @property int $muc_direktanbindung_hbf_neu
 * @property int $muc_direktanbindung_no_ist
 * @property int $muc_direktanbindung_no_neu
 * @property int $muc_direktanbindung_so_ist
 * @property int $muc_direktanbindung_so_neu
 * @property int $muc_direktanbindung_w_ist
 * @property int $muc_direktanbindung_w_neu
 * @property int $muc_hbf_produkt_funktion
 * @property int $muc_hbf_produkt_janein
 * @property int $muc_hbf_produkt_preisaufschlag
 * @property int $muc_str_fahrzeit_funktion
 * @property int $muc_str_fahrzeit_a_ist
 * @property int $muc_str_fahrzeit_a_neu
 * @property int $muc_fra_fahrzeit_funktion
 * @property int $muc_fra_fahrzeit_achse_s_ist
 * @property int $muc_fra_fahrzeit_achse_s_neu
 * @property int $muc_fra_fahrzeit_achse_n_ist
 * @property int $muc_fra_fahrzeit_achse_n_neu
 * @property int $muc_vie_fahrzeit_funktion
 * @property int $muc_vie_fahrzeit_sal_ist
 * @property int $muc_vie_fahrzeit_sal_neu
 * @property int $muc_miv_fahrzeit_funktion
 * @property int $muc_miv_fahrzeit_a9_ist
 * @property int $muc_miv_fahrzeit_a9_neu
 * @property int $muc_miv_fahrzeit_ad_ist
 * @property int $muc_miv_fahrzeit_ad_neu
 * @property int $muc_miv_fahrzeit_ak_s_ist
 * @property int $muc_miv_fahrzeit_ak_s_neu
 * @property int $muc_miv_fahrzeit_ak_w_ist
 * @property int $muc_miv_fahrzeit_ak_w_neu
 */
class Berechnung extends ActiveRecord
{
    /**
     * @param bool $useQuery
     * @return BerechnungQuery|\yii\db\ActiveQuery
     */
    public static function find($useQuery = true){
        if ($useQuery){
            return new BerechnungQuery(get_called_class());
        } else{
            return parent::find();
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->berechnung_id;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getBezeichnung()
    {
        return $this->bezeichnung;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getAnmerkung()
    {
        return $this->anmerkung;
    }

    /**
     * @return string
     */
    public function getStartzeitpunkt()
    {
        return $this->startzeitpunkt;
    }

    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitFunktion()
    {
        return $this->muc_fahrzeit_funktion;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitHbfIst()
    {
        return $this->muc_fahrzeit_hbf_ist;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitHbfNeu()
    {
        return $this->muc_fahrzeit_hbf_neu;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitNoIst()
    {
        return $this->muc_fahrzeit_no_ist;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitNoNeu()
    {
        return $this->muc_fahrzeit_no_neu;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitSoIst()
    {
        return $this->muc_fahrzeit_so_ist;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitSoNeu()
    {
        return $this->muc_fahrzeit_so_neu;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitWIst()
    {
        return $this->muc_fahrzeit_w_ist;
    }

    /**
     * @return int
     */
    public function getMucFahrzeitWNeu()
    {
        return $this->muc_fahrzeit_w_neu;
    }

    /**
     * @return int
     */
    public function getMucTaktFunktion()
    {
        return $this->muc_takt_funktion;
    }

    /**
     * @return int
     */
    public function getMucTaktHbfIst()
    {
        return $this->muc_takt_hbf_ist;
    }

    /**
     * @return int
     */
    public function getMucTaktHbfNeu()
    {
        return $this->muc_takt_hbf_neu;
    }

    /**
     * @return int
     */
    public function getMucTaktNoIst()
    {
        return $this->muc_takt_no_ist;
    }

    /**
     * @return int
     */
    public function getMucTaktNoNeu()
    {
        return $this->muc_takt_no_neu;
    }

    /**
     * @return int
     */
    public function getMucTaktSoIst()
    {
        return $this->muc_takt_so_ist;
    }

    /**
     * @return int
     */
    public function getMucTaktSoNeu()
    {
        return $this->muc_takt_so_neu;
    }

    /**
     * @return int
     */
    public function getMucTaktWIst()
    {
        return $this->muc_takt_w_ist;
    }

    /**
     * @return int
     */
    public function getMucTaktWNeu()
    {
        return $this->muc_takt_w_neu;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungFunktion()
    {
        return $this->muc_direktanbindung_funktion;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungHbfIst()
    {
        return $this->muc_direktanbindung_hbf_ist;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungHbfNeu()
    {
        return $this->muc_direktanbindung_hbf_neu;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungNoIst()
    {
        return $this->muc_direktanbindung_no_ist;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungNoNeu()
    {
        return $this->muc_direktanbindung_no_neu;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungSoIst()
    {
        return $this->muc_direktanbindung_so_ist;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungSoNeu()
    {
        return $this->muc_direktanbindung_so_neu;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungWIst()
    {
        return $this->muc_direktanbindung_w_ist;
    }

    /**
     * @return int
     */
    public function getMucDirektanbindungWNeu()
    {
        return $this->muc_direktanbindung_w_neu;
    }

    /**
     * @return int
     */
    public function getMucHbfProduktFunktion()
    {
        return $this->muc_hbf_produkt_funktion;
    }

    /**
     * @return int
     */
    public function getMucHbfProduktJanein()
    {
        return $this->muc_hbf_produkt_janein;
    }

    /**
     * @return int
     */
    public function getMucHbfProduktPreisaufschlag()
    {
        return $this->muc_hbf_produkt_preisaufschlag;
    }

    /**
     * @return int
     */
    public function getMucStrFahrzeitFunktion()
    {
        return $this->muc_str_fahrzeit_funktion;
    }

    /**
     * @return int
     */
    public function getMucStrFahrzeitAIst()
    {
        return $this->muc_str_fahrzeit_a_ist;
    }

    /**
     * @return int
     */
    public function getMucStrFahrzeitANeu()
    {
        return $this->muc_str_fahrzeit_a_neu;
    }

    /**
     * @return int
     */
    public function getMucFraFahrzeitFunktion()
    {
        return $this->muc_fra_fahrzeit_funktion;
    }

    /**
     * @return int
     */
    public function getMucFraFahrzeitAchseSIst()
    {
        return $this->muc_fra_fahrzeit_achse_s_ist;
    }

    /**
     * @return int
     */
    public function getMucFraFahrzeitAchseSNeu()
    {
        return $this->muc_fra_fahrzeit_achse_s_neu;
    }

    /**
     * @return int
     */
    public function getMucFraFahrzeitAchseNIst()
    {
        return $this->muc_fra_fahrzeit_achse_n_ist;
    }

    /**
     * @return int
     */
    public function getMucFraFahrzeitAchseNNeu()
    {
        return $this->muc_fra_fahrzeit_achse_n_neu;
    }

    /**
     * @return int
     */
    public function getMucVieFahrzeitFunktion()
    {
        return $this->muc_vie_fahrzeit_funktion;
    }

    /**
     * @return int
     */
    public function getMucVieFahrzeitSalIst()
    {
        return $this->muc_vie_fahrzeit_sal_ist;
    }

    /**
     * @return int
     */
    public function getMucVieFahrzeitSalNeu()
    {
        return $this->muc_vie_fahrzeit_sal_neu;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitFunktion()
    {
        return $this->muc_miv_fahrzeit_funktion;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitA9Ist()
    {
        return $this->muc_miv_fahrzeit_a9_ist;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitA9Neu()
    {
        return $this->muc_miv_fahrzeit_a9_neu;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAdIst()
    {
        return $this->muc_miv_fahrzeit_ad_ist;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAdNeu()
    {
        return $this->muc_miv_fahrzeit_ad_neu;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAkSIst()
    {
        return $this->muc_miv_fahrzeit_ak_s_ist;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAkSNeu()
    {
        return $this->muc_miv_fahrzeit_ak_s_neu;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAkWIst()
    {
        return $this->muc_miv_fahrzeit_ak_w_ist;
    }

    /**
     * @return int
     */
    public function getMucMivFahrzeitAkWNeu()
    {
        return $this->muc_miv_fahrzeit_ak_w_neu;
    }
}